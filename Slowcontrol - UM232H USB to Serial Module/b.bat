echo off
cls
del *.o *.exe
gcc -c sample-static.c -o sample-static.o
gcc -o sample-static.exe sample-static.o -L. -lMPSSE
sample-static

pause

gcc sample-dynamic.c -o sample-dynamic.exe
sample-dynamic
