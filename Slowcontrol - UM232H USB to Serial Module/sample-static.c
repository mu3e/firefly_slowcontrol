/*!
 * \file sample-static.c
 *
 * \author FTDI
 * \date 20110512
 *
 * Copyright � 2011 Future Technology Devices International Limited
 * Company Confidential
 *
 * Project: libMPSSE
 * Module:  I2C Sample Application - Interfacing 24LC02B I2C EEPROM
 *
 * Rivision History:
 * 0.1 - initial version
 * 0.2 - 20110801 - Changed LatencyTimer to 255
 * 				  Attempt to open channel only if available
 *				  Added & modified macros
 *                Change in APIs I2C_GetChannelInfo & OpenChannel to start indexing from 0
 */

#include<stdio.h>
#include<stdlib.h>
#ifdef _WIN32
#include<windows.h>
#endif
#include "libMPSSE_i2c.h"
#include "ftd2xx.h"
#include <unistd.h>
#include <math.h> 


#define APP_CHECK_STATUS(exp) {if(exp!=FT_OK){printf("%s:%d:%s(): status(0x%x) != FT_OK\n",__FILE__, __LINE__, __FUNCTION__,exp);}else{;}};
#define CHECK_NULL(exp){if(exp==NULL){printf("%s:%d:%s():  NULL expression encountered \n",__FILE__, __LINE__, __FUNCTION__);exit(1);}else{;}};

#define I2C_DEVICE_ADDRESS		0x50 //0x57
#define I2C_DEVICE_BUFFER_SIZE		256
#define I2C_WRITE_COMPLETION_RETRY		10
#define START_ADDRESS 	0x00
#define END_ADDRESS		0x00

#define RETRY_COUNT				10
#define CHANNEL_TO_OPEN			0	/*0 for first available channel, 1 for next... */

uint32 channels;
FT_HANDLE ftHandle;
ChannelConfig channelConf;
FT_STATUS status;
uint8 buffer[I2C_DEVICE_BUFFER_SIZE];

uint32 write_byte(uint8 slaveAddress, uint8 registerAddress, uint8 data)
{
	uint32 bytesToTransfer = 0;
	uint32 bytesTransfered;
	bool writeComplete=0;
	uint32 retry=0;
	
	bytesToTransfer=0;
	bytesTransfered=0;
	buffer[bytesToTransfer++]=registerAddress; /*Byte addressed inside EEPROM's memory*/
	buffer[bytesToTransfer++]=data;
	status = I2C_DeviceWrite(ftHandle, slaveAddress, bytesToTransfer, buffer, &bytesTransfered, I2C_TRANSFER_OPTIONS_START_BIT|I2C_TRANSFER_OPTIONS_STOP_BIT);
	APP_CHECK_STATUS(status);

	while((writeComplete==0) && (retry<I2C_WRITE_COMPLETION_RETRY))
	{
		bytesToTransfer=0;
		bytesTransfered=0;
		buffer[bytesToTransfer++]=registerAddress; /*Byte addressed inside EEPROM's memory*/
		status = I2C_DeviceWrite(ftHandle, slaveAddress, bytesToTransfer, buffer, &bytesTransfered, I2C_TRANSFER_OPTIONS_START_BIT|I2C_TRANSFER_OPTIONS_BREAK_ON_NACK);
		
		if(bytesToTransfer==bytesTransfered)
		{
			writeComplete=1;
			//printf("... Write done\n");
		}

		retry++;
	}
	return 0;
}

FT_STATUS read_byte(uint8 slaveAddress, uint8 registerAddress, uint8 *data)
{
	FT_STATUS status;
	uint32 bytesToTransfer = 0;
	uint32 bytesTransfered;
	
	bytesToTransfer=0;
	bytesTransfered=0;
	buffer[bytesToTransfer++]=registerAddress; /*Byte addressed inside EEPROM's memory*/
	status = I2C_DeviceWrite(ftHandle, slaveAddress, bytesToTransfer, buffer, &bytesTransfered, I2C_TRANSFER_OPTIONS_START_BIT);
	bytesToTransfer=1;
	bytesTransfered=0;
	status |= I2C_DeviceRead(ftHandle, slaveAddress, bytesToTransfer, buffer, &bytesTransfered, I2C_TRANSFER_OPTIONS_START_BIT);
	*data = buffer[0];
	return status;
}

void power_setting(uint8 power, uint8 emphasis)
{
	uint8 address = 127;
	uint8 address2 = 237;
	uint8 data;
	uint8 data2;
	FT_STATUS status;
	int i, j;

	write_byte(I2C_DEVICE_ADDRESS, address, 0x03);
	address = 238;
	status = read_byte(I2C_DEVICE_ADDRESS, address, &data);
	for (j = 0; ((j<RETRY_COUNT) && (FT_OK != status)); j++)
	{
		printf("read error... retrying \n");
		status = read_byte(I2C_DEVICE_ADDRESS, address, &data);
	}
	printf("old power setting in address %d data read=%x\n", address, data);

	write_byte(I2C_DEVICE_ADDRESS, address, power);

	status = read_byte(I2C_DEVICE_ADDRESS, address, &data);
	for (j = 0; ((j<RETRY_COUNT) && (FT_OK != status)); j++)
	{
		printf("read error... retrying \n");
		status = read_byte(I2C_DEVICE_ADDRESS, address, &data);
	}
	printf("new power setting in address %d data read=%x\n", address, data);


	address = 236;
	status = read_byte(I2C_DEVICE_ADDRESS, address, &data);
	status = read_byte(I2C_DEVICE_ADDRESS, address, &data2);
	printf("old emphasis setting in address %d data read=%x%x\n", address, data, data2);

	write_byte(I2C_DEVICE_ADDRESS, address, emphasis);

	status = read_byte(I2C_DEVICE_ADDRESS, address, &data);
	status = read_byte(I2C_DEVICE_ADDRESS, address, &data2);
	printf("new emphasis setting in address %d data read=%x%x\n", address, data, data2);

	address = 127;
	write_byte(I2C_DEVICE_ADDRESS, address, 0x00);
}

void statistic_read(uint8 address,int length , uint8 option, uint8 disable)
{
	uint16 data = 0;
	uint8 data1 = 0;
	uint8 data2 = 0;
	FT_STATUS status;
	int count = 0;
	double mylist[length];
	double mean = 0.0;
	double var = 0.0;
	double sd = 0.0;
	for (count = 0; count < length; count++)
	{
		
		status = read_byte(I2C_DEVICE_ADDRESS, address, &data1);
		data = data1;
		if (option == 2)
		{
			address += 1;
			status = read_byte(I2C_DEVICE_ADDRESS, address, &data2);
			data = data << 8;
			data += data2;
			address = address - 1;
		}
		mean += (double)data;
		mylist[count] = (double)data;
		printf("data1: %x, data2: %x, data: %x, %d, sum: %lf\n", data1, data2, data, data, mean);

		if (disable == 1)
		{
			int add = 127;
			uint8 mask = 0x03;
			write_byte(I2C_DEVICE_ADDRESS, address, mask);
			add = 241;
			mask = 0b11110000;
			write_byte(I2C_DEVICE_ADDRESS, address, mask);
			Sleep(100);
			mask = 0b00000000;
			write_byte(I2C_DEVICE_ADDRESS, address, mask);
			add = 127;
			mask = 0x00;
			write_byte(I2C_DEVICE_ADDRESS, address, mask);
		}


		Sleep(500);
	}

	printf("sum = %lf\n", mean);


	mean = mean / length;

	for (count = 0; count < length; count++)
	{
		var += pow(mylist[count] - mean, 2);
	}
	var /= (length - 1);

	sd = sqrt(var);

	printf("mean value in register %d is %lf, stdDev = %lf\n",address, mean, sd);
}

int main()
{
	printf("Testausgabe\n");
	FT_STATUS status;
	FT_DEVICE_LIST_INFO_NODE devList;
	uint8 address;
	uint8 data;
	int i,j;
#ifdef _MSC_VER
	Init_libMPSSE();
#endif
	channelConf.ClockRate = I2C_CLOCK_STANDARD_MODE;/*i.e. 400000 KHz*/
	channelConf.LatencyTimer= 255;
	//channelConf.Options = I2C_DISABLE_3PHASE_CLOCKING;

	status = I2C_GetNumChannels(&channels);
	APP_CHECK_STATUS(status);
	printf("Number of available I2C channels = %d\n",channels);

	if(channels>0)
	{
		for(i=0;i<channels;i++)
		{
			status = I2C_GetChannelInfo(i,&devList);
			APP_CHECK_STATUS(status);
			printf("Information on channel number %d:\n",i);
			/*print the dev info*/
			printf("		Flags=0x%x\n",devList.Flags); 
			printf("		Type=0x%x\n",devList.Type); 
			printf("		ID=0x%x\n",devList.ID); 
			printf("		LocId=0x%x\n",devList.LocId); 
			printf("		SerialNumber=%s\n",devList.SerialNumber); 
			printf("		Description=%s\n",devList.Description); 
			printf("		ftHandle=0x%x\n",devList.ftHandle);/*always 0 unless open*/
		}
		
		status = I2C_OpenChannel(CHANNEL_TO_OPEN,&ftHandle);/*Open the first available channel*/
		APP_CHECK_STATUS(status);
		printf("\nhandle=%d status=%d\n",ftHandle,status);
		status = I2C_InitChannel(ftHandle,&channelConf);
		
		//power_setting(0xF0, 0x00);
		statistic_read(38, 500, 2, 1);


		address = 127;
		status = read_byte(I2C_DEVICE_ADDRESS, address, &data);
		for(j=0; ((j<RETRY_COUNT) && (FT_OK !=status)); j++)
			{
				printf("read error... retrying%x \n",FT_OK);
				status = read_byte(I2C_DEVICE_ADDRESS,address, &data);
			}
		printf("address %d data read=%x\n",address,data);
/*		for(address=START_ADDRESS;address<END_ADDRESS;address++)
		{
			printf("writing byte at address = %d   ",address);
			write_byte(I2C_DEVICE_ADDRESS,address,1);
		}

		for(address=START_ADDRESS;address<END_ADDRESS;address++)
		{
			status = read_byte(I2C_DEVICE_ADDRESS,address, &data);
			for(j=0; ((j<RETRY_COUNT) && (FT_OK !=status)); j++)
			{
				printf("read error... retrying \n");
				status = read_byte(I2C_DEVICE_ADDRESS,address, &data);
			}
			printf("address %d data read=%x\n",address,data);
		}
*/		status = I2C_CloseChannel(ftHandle);
	}
	
#ifdef _MSC_VER
	Cleanup_libMPSSE();
#endif
	
	return 0;
}
