
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

entity firmware_test is
    port (
		clk : in std_logic;
		reset_n : in std_logic;
		LED_n : out std_logic_vector (7 downto 0);
		test_sig : in std_logic_vector (7 downto 0)
);
end entity firmware_test;

architecture structural of firmware_test is
	signal count : std_logic_vector (31 downto 0);
	signal LED_sig_n : std_logic;


begin
	LED_n(0) <= LED_sig_n;
	LED_n(1) <= test_sig(1);
	LED_n(2) <= test_sig(2);
	LED_n(3) <= test_sig(3);
	LED_n(4) <= test_sig(4);
	LED_n(5) <= test_sig(5);
	LED_n(6) <= test_sig(6);
	LED_n(7) <= test_sig(7);
	
	process (clk, reset_n)
	begin
		if (reset_n = '0') then
			count <= (others => '0');
			LED_sig_n <= '0';	
		elsif (rising_edge(clk)) then
			count <= count + 1;
			if (count = 50000000) then
				count <= (others => '0');
				LED_sig_n <= not LED_sig_n;
			end if;
		end if;
	end process;
end architecture structural;