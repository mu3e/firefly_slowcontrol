# This example script demonstrates the way to obtain optimum analog
# settings of the transceiver by sweeping across different analog
# values. This is achieved by reconfiguring the transceiver at
# runtime and controlling the data pattern generator and checker
# to obtain bit error rate of the link across different settings.
# This script uses transceiver_debug_link, transcevier_channel_tx
# and transcevier_channel_rx services.

source link_test_lib.tcl

####################################################
# Customize the test variables
####################################################

# Device name to match the correct device in case of multiple devices
set device_die_name "5SGTMC7K2"

# Relationship between transceiver channel RX and TX:
#   index 0 - Transceiver debug link service name: Loopback_Link_<$xcvr_instance_name>_address_<number>
#   index 1 - Transceiver channel TX service name: TX_<$xcvr_instance_name>_address_<number>
#   index 2 - Transceiver channel RX service name: RX_<$xcvr_instance_name>_address_<number>
set logical_link_pairs {{ "Loopback_Link_xcvr_custom_phy_0_address_0" "TX_xcvr_custom_phy_0_address_0" "RX_xcvr_custom_phy_0_address_0" 0} \
						{ "Loopback_Link_xcvr_custom_phy_0_address_1" "TX_xcvr_custom_phy_0_address_1" "RX_xcvr_custom_phy_0_address_1" 1} \
						{ "Loopback_Link_xcvr_custom_phy_0_address_2" "TX_xcvr_custom_phy_0_address_2" "RX_xcvr_custom_phy_0_address_2" 2} \
						{ "Loopback_Link_xcvr_custom_phy_0_address_3" "TX_xcvr_custom_phy_0_address_3" "RX_xcvr_custom_phy_0_address_3" 3} }

set xcvr_instance_name "xcvr"

# Generator preamble mode: 0 to disable, 1 to enable
set enable_preamble 0

# Generator preamble word, not used if preamble is disabled
set preamble_word 0x0

# Transceiver RX word aligner: 0 to disable, 1 to enable
set enable_word_aligner 0

# Set Transceiver's PRBS pattern for IP : Avalon-ST Data Pattern Generator
# Data pattern type: prbs7, prbs15, prbs23, prbs31
set data_pattern "prbs23"

# Transmitter and Receiver side analog value setting, by default based on design's Custom PHY block (link_test_sopc_sys.xcvr)
# SET user_define_analog_value to 1 if you would like to change manually Rx side analog setting
# Transceiver reconfig analog values
# VOD Control               (vodctrl)      - Valid Value = {0 1 2 3 4 5 6 7}
# Pre-emphasis 1st post-tap (preemph1t)    - Valid Value = {0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31}
# Pre-emphasis Pre post-tap (preemph0t)    - Valid Value = {-15 -14 -13 -12 -11 -10 -9 -7 -6 -5 -4 -3 -2 -1 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15}
# Pre-emphasis 2nd post-tap (preemph2t)    - Valid Value = {-15 -14 -13 -12 -11 -10 -9 -7 -6 -5 -4 -3 -2 -1 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15}
# DC Gain                   (dc_gain)      - Valid Value = {0 1 2 3 4}
# Equalizer Control         (eq_ctrl_list) - Valid Value = {0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15}
set vodctrl_list   {50 63}
set preemph1t_list {2}
set preemph0t_list {1}
set preemph2t_list {3}
set dcgain_list    {1 2}
set eqctrl_list    {1 3}

# Setup link run length
set max_run_time_in_seconds 2

# Use the current path as the QII project path
set design_filepath .


####################################################
# Initialize the design and device environment
####################################################

puts "\n\n--- Initialization ---\n\n"


# Setup design
design_load $design_filepath

# Assumes single design entry
if { [llength [get_service_paths design]] != 1 } {
	error "Incorrect number of designs available: expect only one, but found [llength [get_service_paths designs]]"
} else {
	set design_path [lindex [get_service_paths design] 0]
}

# Setup device
foreach device_name [get_service_paths device] {
	if { [regexp .*$device_die_name.* $device_name] } {
		set device_path $device_name
		break
	}
}

# Link design to the device
puts "Design: Path = $design_path"
puts "Device: Path = $device_path"
design_link $design_path $device_path


####################################################
# Transceiver reconfig analog sweep
####################################################

# Using internal loopback
enable_internal_loopback

# Loop every transceiver link
foreach logical_link_pair $logical_link_pairs {

	set debug_link         [lindex $logical_link_pair 0]
	set tx_logical_channel [lindex $logical_link_pair 1]
	set rx_logical_channel [lindex $logical_link_pair 2]
	set link_pair_count    [lindex $logical_link_pair 3]
	
        # this is to reset all PMA value for each channel 
        set ber_lowest 1
        set dcgain_lowest 0
        set eqctrl_lowest 0
        set vodctrl_lowest 0
        set preemph1t_lowest 0
        set preemph0t_lowest 0
        set preemph2t_lowest 0

	puts "\n\n--- Transceiver reconfig analog settings sweep for $debug_link between $tx_logical_channel and $rx_logical_channel ---\n\n"

	# Obtain the services path
	set transceiver_debug_link_path [lindex [get_service_paths "transceiver_debug_link"] $link_pair_count]
	set transceiver_channel_tx_path [lindex [get_service_paths "transceiver_channel_tx"] $link_pair_count]
	set transceiver_channel_rx_path [lindex [get_service_paths "transceiver_channel_rx"] $link_pair_count]

	# Open services and setup components

	open_service transceiver_debug_link $transceiver_debug_link_path
	open_service transceiver_channel_tx $transceiver_channel_tx_path
	open_service transceiver_channel_rx $transceiver_channel_rx_path

        # set PRBS Data Pattern
	transceiver_debug_link_set_pattern $transceiver_debug_link_path $data_pattern
	transceiver_debug_link_config_print $transceiver_debug_link_path

	if { $enable_preamble == 1 } {
		transceiver_channel_tx_enable_preamble $transceiver_channel_tx_path
		transceiver_channel_tx_set_preamble_word $transceiver_channel_tx_path $preamble_word
	} else {
		transceiver_channel_tx_disable_preamble $transceiver_channel_tx_path
	}
	transceiver_channel_tx_config_print $transceiver_channel_tx_path

	transceiver_channel_rx_set_word_aligner_enabled $transceiver_channel_rx_path $enable_word_aligner
	transceiver_channel_rx_config_print $transceiver_channel_rx_path

	puts "Running test with the following settings:"
	puts "  Data pattern generator    : [transceiver_channel_tx_get_pattern $transceiver_channel_tx_path]"
	puts "  Data pattern checker      : [transceiver_channel_rx_get_pattern $transceiver_channel_rx_path]"
	puts "  VOD control               : $vodctrl_list"
	puts "  Pre-emphasis 1st post-tap : $preemph1t_list"
	puts "  Pre-emphasis pre-tap      : $preemph0t_list"
	puts "  Pre-emphasis 2nd post-tap : $preemph2t_list"
	puts "  DC gain                   : $dcgain_list"
	puts "  Equalization control      : $eqctrl_list"
	puts ""

	puts [format "%7s %9s %9s %9s %7s %12s %14s %12s %14s" "  VOD  " "Preemph1t" "Preemph0t" "Preemph2t" "DC gain" "Equalization" "  Total bits  " " Error bits " "Bit error rate"]
	puts [format "%7s %9s %9s %9s %7s %12s %14s %12s %14s" "=======" "=========" "=========" "=========" "=======" "============" "==============" "============" "=============="]

	# Loop every analog settings
	foreach vodctrl $vodctrl_list {
		foreach preemph1t $preemph1t_list {
			foreach preemph0t $preemph0t_list {
				foreach preemph2t $preemph2t_list {
					foreach dcgain $dcgain_list {
						foreach eqctrl $eqctrl_list {

							transceiver_channel_rx_analog_value_set $transceiver_channel_rx_path $dcgain $eqctrl
							transceiver_channel_tx_analog_value_set $transceiver_channel_tx_path $vodctrl $preemph1t $preemph0t $preemph2t

							# Reset counters, start generator and checker
								transceiver_debug_link_start_running $transceiver_debug_link_path

							set start_time [clock seconds]
							set bit_error_rate 0
							set locked 1
							after [expr $max_run_time_in_seconds * 1000]
							if { [transceiver_channel_rx_is_locked $transceiver_channel_rx_path] } {
								# Snapshot the counters
								set counters [transceiver_channel_rx_get_data $transceiver_channel_rx_path]
								set total_bits [lindex $counters 0]
								set error_bits [lindex $counters 1]
								set bit_error_rate [lindex $counters 2]
								puts [format "%7s %9s %9s %9s %7s %12s %14s %12s %14s" \
									"$vodctrl" \
									"$preemph1t" \
									"$preemph0t" \
									"$preemph2t" \
									"$dcgain" \
									"$eqctrl" \
									$total_bits $error_bits $bit_error_rate]

		                                              		if { $ber_lowest > $bit_error_rate } {
		                                        		   #puts " Found lower BER : $bit_error_rate Original : $ber_lowest"
		                                                           set ber_lowest $bit_error_rate

		                                                           set vodctrl_lowest $vodctrl
		                                                           set preemph1t_lowest $preemph1t
		                                                           set preemph0t_lowest $preemph0t
		                                                           set preemph2t_lowest $preemph2t
		                                                           set dcgain_lowest $dcgain
		                                                           set eqctrl_lowest $eqctrl
		                                        		}
							} else {
								puts [format "%7s %9s %9s %9s %7s %12s %14s %12s %14s" \
									"$vodctrl" \
									"$preemph1t" \
									"$preemph0t" \
									"$preemph2t" \
									"$dcgain" \
									"$eqctrl" \
									"\[Not locked\]" "\[Not locked\]" "\[Not locked\]"]
								set locked 0
							}

							# Stop generator and checker
							transceiver_channel_rx_stop_checking $transceiver_channel_rx_path
							transceiver_channel_tx_stop_generation $transceiver_channel_tx_path
						}
					}
				}
			}
		}
	}

             puts ""
             puts " =========================================================================================="
             puts "  Found lowest BER : $ber_lowest with below analog settings"
      	     puts "  VOD control               : $vodctrl_lowest"
      	     puts "  Pre-emphasis 1st post-tap : $preemph1t_lowest"
      	     puts "  Pre-emphasis pre-tap      : $preemph0t_lowest"
      	     puts "  Pre-emphasis 2nd post-tap : $preemph2t_lowest"
      	     puts "  DC gain                   : $dcgain_lowest"
      	     puts "  Equalization control      : $eqctrl_lowest"
             puts ""
	puts "Test stopped after sweeping through each combination of analog values for $max_run_time_in_seconds seconds\n"

	# Close services
	close_service transceiver_debug_link $transceiver_debug_link_path
	close_service transceiver_channel_rx $transceiver_channel_rx_path
	close_service transceiver_channel_tx $transceiver_channel_tx_path

	puts "\n\n--- End of transceiver reconfig analog settings sweep for $debug_link between $tx_logical_channel and $rx_logical_channel ---\n\n"

}
