#!/bin/bash

read -p "name: " var
cp  gx_link_test_example.sof flash/gx_link_test_example_$var.sof
sof2flash --offset=0xC20000 --pfl --optionbit=0x180000 --programmingmode=PS --input=flash/gx_link_test_example_$var.sof  --output=flash/gx_link_test_example_$var.flash
echo $var

