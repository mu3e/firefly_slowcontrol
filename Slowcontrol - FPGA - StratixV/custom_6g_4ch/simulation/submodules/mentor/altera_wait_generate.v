// Copyright (C) 1991-2016 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, the Altera Quartus Prime License Agreement, the Altera
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 16.0
// ALTERA_TIMESTAMP:Thu Apr 28 07:02:11 PDT 2016
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
koKVjKiQLlWxLlPVw8gl61VWDiQ7nRhEOlELdp9rvXm4NS+iIVCuyLl2DFFnyDjm
RJC6EowohMy7J9Q5rFL7kY21ny7DSVTLGA0TIOf2WjvZ3UwcCdMsbouV/nIJ+uaG
G5AlK5KVa4vcG8wv+HHYzBWp3dAo9KGvYcMLCErArss=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2176)
5Vwfz+D7f0WNqzymAgbhVQjK75FSrCRNGqq8ttadfb7O4aq5yc7Mrhz2Vz/5i1uo
353eh6hwGn5uYLUdSPSc8oJ7Wg4dFTP5KSEe9QYwyRNbxUiXmBzUOgv+jxA7vsQO
jMzvFpmp1ZY0SkimMwkuragMocqIopMPKMSdqQ0hOhCsaTIBfzyet5nWIestL9qH
1sRhNfG0LPzLzNXNNy5SI4V4t12RuGIAAFKXTKJj/8lJo5RI2Nxd5YG5MNBll2OW
Uftd53CVnVKQzwDlD6zF3NpqE5Fbmsqxb2CvVoxoSsDQXkX0GRa0ZE3PJ4w2tmh9
IABr5rgtTiq1LRagRntUNIKaZ5pRMxcizauaiWgiEsZU94VX5eKiexSCKK/PCqC5
1BSftWzyylvxfOnIYx+FHbhfw2gdTvUickZeG/3Q7D1O8vELJ8vyZfGoWMlQ0R7o
rDCZ2g+VdtzOpHqNNyN5MxoXfFPeJaygSlWMhkKxHTMaCpCQp1El5BSC5cN/QwWv
a2aGRm2eJj4NL1h0cR5WhwWOeyjol1frs402FAyj2cXXDvhE2k/1iWL/29+5bO4P
h5RyKMMk9E05HfLVc5Qr3wANjlndJil304LCQd9o7EEuJFYttl7SyTeCGfqU27ZD
nHyJ1CFd7W21X0otWasASVG2Z7R9PFb9IviBjsk9xx8qp8Hi8JuiXWalYcPgFyft
vA0uk8SpVWbSq5eQgH2ghXgoAk2TY9E7oYVuw+M4//imD0vO45RqXkc7WFLlZ7fF
ZLgaPcLPRuker54FjWQvr1PgrhiWadWPm6obnzhUk/3sGyk3fu5uuXevwly5UvoY
Zp+r2WvquhPaBTMsG2ubAPnNYF7JWjG90/IWKMXnVVVRwc8CMc/o2tlJSNU7ckV1
PjIYsbSSWfS5105nTdM9Hha6N7wR2wihO/cbVd+irx2/XbXw9058xfZR2BPLjvFv
ViPwOGGsDqBKQnjQngdqkhVOisge5vHt1MBKTyz0zOT4JDXbhPfuXueL7YK6ZvyL
BojdIC/oLhwrnM56+HdBHOTFW7s0BcCJ7HOy0mDDVnN2NZsftp8/vohcfcZ9sCZw
98NpwFZsD7bLuuMKdcE0D2XREEbDVuERJXboGIgQYOUza+04bTKz08uvzIjtDJgV
sNsCHJXsja0aT1jNT7jRlOoYcyyTx0qJGtDRjL9Y/Mxri+BVphiKlrp0b9hQ9z+q
Z6rE3n6k4rZ5sf3O/HGA3V8fuKo7P8hXvgMBvVDWU4PXxW7aS/oJHRpNpXCunjj7
TlPZRyF/pQ2OjltGgZLOltzLIwCrbnEIN2ljBULASB95wEWY1TYWqBScdy1+7Vf8
jiZcQLPh9sFfbtQVTxdlAhbo+tubT4IA1f0lvm+RZSGLhEaHPVvjaHD8CaX4/QT8
bVykI1KZseiN2UL7yKvJoAAVCAXhEcnkL/Qw7RiYMKyqWItblwK4p30hWD7WvgAB
Wp5xyxI+TIXB7GLNRJ94XNz8eVcDWjjBHf7J/GNWbQTK03aCXZYYaDwi60XXO0Zw
WpLjaJ50lWhUuIApAs/rpGe7/T9VTv1IbbS9PfIN4I+B7GtTew/oj1UT4v1G0Cpc
w/H24BEr1calyb32z61FgYed+p1qHyAcl1ErAon0M2PcErNrucTchtPEuxlBXcN0
Rhp471UMAUjdryMzOwaywAkI4So2yrssJGFk0G9pPCAAdphNx5/Trk5m7xonTqSq
WBV3hf348SrmtnD5VD9RLOLzMiDXTUEuYhXTMjXrxJKeD442fA+ClkSxEe/EV1yC
2KS1UR4HY5hqjMDrDwfbKvOIzF+VUSynkLWyylkrMSgm7T8cJa5JX8W7UYHK5qHs
27QSMPhCBreNb+T2RPACnNb3t2tdK1o7BU8y5QhAiz7thbfMax8JGxBejD/X1Yj4
eeSwtKmr2aKr1wP5G2YgX6g0+GI8R1MEzgItWqz6RmC45S57llJzd0+bUknwN6sE
w7Q57gThuRa8NeYce1OgQZHv9v1nMzLPdctWkrfQ8PJk+CiTOSs27nTXOrKt8Y8Z
z2pqU728CYm8W4kM1y5/369TuCHxvdYO48MLzrEuryotV435/Cbulo2XAqXGD4RL
/HLqQjOT8AOJ3azZITpBYHWoh7/v+TGpzsQX481dLfckKR64KBKOiVsznPCtZ2AH
gXPfTpzTbKlJ7m85nLGhvjHhxV2ci0CKrpkjUtFeu07wt7ABgGdwsElLHWEu2jxT
v7d9cNvPCo6zcoP+ISVxsqwc7d85jL/1weW2vp7jzhpbCizPwGN59us2dnfip3n3
zJXNq1T4dIith/1menKn2uuLX88AaQlIr2Y8ooJqQe7PfxBfs5PAHz5tOGTaXZLl
KQGsoEL4IJzFG2mv4BeKAP9ZdjEmtCHjSxH0UaqCg6c92mKDXHHhQ8mHSWgtFt5z
lrlTbH+6Oa2SOH2Z/xQm/Dg1Z/lU7mjvdsoR36E/4lQmzJxOmrJN+to6JN7ml9pr
GCbdMWDKynzq4AfTXwg5LMoirfyf39TAF5RaZwq+mJ+gokn9Rb6K9diW4wkAx5iG
+vYunRBSn0jJrEFCu+uzfvU6HWkpFHS2lKd+C8TXUG0Msot4DuT8ropO8KwlNVOM
aibG5utFuI+HOtr6y3ADFxgxJ+z9XBph+5kwysQCEO4dMg4ZwsdLqfNOiUXMgymr
8DiqzjwjSWLO8+0ods8XEKzRsX6+kxw025D4KdMBgzDjhuexZJ7yyJT9L1mOd92O
xCw3JhZTyFeQC5CPm4xonGOh1iZMCCLYGNu/v2EYmTs5lDyHps4jSr1iOQxWDUb8
fZPhOJg27SBvIEN0oZeNXiqGokPGw2LwmxLKTQfeBlvrYX+/2YPLXk+JXGVM20JK
XmwoLyGi9/dicuYTWlv1DA==
`pragma protect end_protected
