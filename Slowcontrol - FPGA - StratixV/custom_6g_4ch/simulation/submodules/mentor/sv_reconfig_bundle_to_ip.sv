// Copyright (C) 1991-2016 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, the Altera Quartus Prime License Agreement, the Altera
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 16.0
// ALTERA_TIMESTAMP:Thu Apr 28 07:02:15 PDT 2016
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
rHBGmOART3+d2w5Awmd4vtoLF5+Q2uL8EAZtd/9QfRa3s4GQoTXYSlFU9KdOERCb
Zjvl8CNhATgGNwgr7nzn9I50M7e/W1wfgiaooZ8PAx6ruUabr9rWi/rs/EU8q5a1
+Gdp44bqh7o7NTYVtbQwGJA5nIPfXywqGqAOJWTNb9I=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2288)
OpalZYwmgRZ3QkHi+P3JQqQ9Apf+9mRarv38Yjd/L40ez+fzIXuRJe7s9HuirnGD
u17cNdRB3AijPgsK4XQhNNxyWFtgU3DZQi2IyrREAsPG0j5W8XX7vjAQoWmULgtV
+4Ey7iK3w96smBMlplLZQwXY+DDH+bodmFbFFUVhAOYabvP7e0UEm6Qmpi+2R1g+
qyygi1bJv5xpPZ/vqL09C1E59tOCl32QBalsREhzP2LrWoFlbzCKTLdp0lzfyBv1
ne1COwKcNa362Jdc1DYFRN9MHxOYnvJdY3CwHx6VU7czvsPAz+MAGfbWRXpe06ch
152cE4RRFBCvjQeSZpHiVVlxGUPZ/mZqSSrBE7bNfR0G96pDKIk6PStacPHEOsqT
85p0bioUnqhwtNvippfPTEIf6iOPMxRVzYXTI3oG5HHdN49V40SwIA4RolGV6nmO
uEwYPvtCArTOPnJb+cJ4D1zFkcsnQdHhJ4z/4RjJtRkYAE/F1atcwPIwoCPr/fLU
i2W3kMaXnmpm3EUdvXsDcyFIGpoHEUEUMYiCBgoGLvZ5FMMCIpff/7x+BkHYE5Cp
1IHhaPDAB4F9CBIK6PPWjjA7brYtDqMmmiG+HS+MfyyA8eIZhsYQgYEOwQdoyZgf
ah0eSTXtBUaX7QMW330CMKrOI/HOzFJSVSLk6tZcq+yAxfzS62LWScDwbI4daqh6
A8Fn7SvlckN48WzW/55ZjfwqEK5I9AReFaG4rxDRCWZGcuDE89oHd3DJt2wUt77T
UHCTCbCjhf83oXelXTBZccj89T9BPU4Xc2LMiaGTD1I2q8CuLdUTALkteGVRDKph
Zljb8ZzvbtdjzP17SyTfAC/TgslCi85zS3wj5XpxwOgYB2xaSkmjdzmk/QOlbpAw
CVNST4pW8S8Z1Jxto2uTGphbgQE320MPFvSuzcWicoRu8KldvOgj3xL3p7QIHkum
Wz+fJAzk/9/Oq2ExvolGeD7FDFXuSg2G2eRQk7LXv9YwlH+urus4EotvfL2yYWEl
RmFssaj0yKf0lYJrxRWBxCsca2COw7KACoJpWViFztbJJw1zdsNiaVZNfwsGpehx
o0NCMb4kPmAHZh1aJX2UIng1qKVaol8o2BT6gMgcOyONQBwag7E9815ssOyfyBfw
C4nVUbvy0abuM1dScY2c8po2jPCv38PFlGwi759FjHumXqjEu6BPwoXbncI7aHVx
omQIrOVvSSxq08BQ5Duygsf2oiu+uLDEbWXRgp7Um+CDUU8LQ3XXtgmI7QAxFM5m
fYeUwq/tQkSkJ7RZrUE6WPIDWpPIeqTCrobvxljKVAOoq6umWGIpDVMIRF177OR1
Zvkr1Xo82j6LeMjfXorXZgOeCgN3XFZnnmVhTP9b2ibloK2AplePUe7kBVz6tv4x
w/71m3BEpYqMM+lC+Io/PpappjQS9HDspKel30EFNy2PUFLZctEl5kvb1fsHC8Zr
vkwTo6pD/eRPWuwGcTkrSZhZ5k1ecqyGNUAGmIRrfz5xqvsMo8jpNavp0NGnmIf0
WoYYAreNaFyn6HAUAfrywxUBIRkPC6pFi6xIlotl57FlWTSrQNiUFCH9RIUWolKm
jO5/132/eCdW/lbRdaS+9ihRYW1VOSVXgLTVuCoT97JHfR4NJYCibSly+cesaEOE
A/5AtxJJhWC0aU8f9PMDf2qt7XKsVEElh5OdKE5bNLacwsAFNRrLLkn3vPzPZT7B
LttzG2Vk3eJ515f3FYQsag9uXARCpNvzrVCKvk6O/jETg/smunFXVKDNbfthWupG
wbIwRF4JGyJVRgbji0ONs2FoN5Vd59RpYQkPRoUG20A7ctSrS1VOY0OoK+lKrX3x
GWFohzOw8xsGB79zeM1kXa3jJWrRiD+3opwgoNhp/uNEtTgYHZr6PxNsI4jIm+Cg
7bsy3sXtYs2JJSRZuolVqq44J4j3C8AffAn0CehSaxe/3KYoT9AdKa/n4VWinRgA
g5+o3x9rTt98hH6eLKxDU2bqrezuj7L+VDv7IJ0OpbMOjYepsfa4uM2gOk7GSFoJ
4M7qLFcbUdQ5z30lvpoFOksfEYC7HDzKP1utaJLiGPS0f5M8Py4iYcUoTZL1Fkd0
3ddRjX8qBeCrPT25oiTL8qeNMTfiqGazzO8SNnRnvUxkdbT1rMbo0juSSw4W/kDK
T34mDrehFodO1S09PM90w34Gw6/8QcOcaP/R1sIvLMxYaugTQ+4ik1xtXZFk+PIN
TabWGCVPRSF57oj429PDhdm0wNTU0/9nk20hOMQn8BgHofP9BZJ+YvqxvkZ/meA6
MWSkOl7lmT2oSnZc4VJlnsQqeVplpQAykw+D3CB1rw5dHsHmM77mLuD1Z0caJysH
XinbNKo6gYOIk6F7jJBlaCE2z5Lb/kqmMBh9ddsGLGvsy2cUfxv+R0iQTlKXY7lF
ugXz+cycm51JVRGEvRfdvOq5lc+TYnTaaYT28qvp8v/NnWlNuOdQZ3A+2NP5FFQJ
gEEUi1fdguAXMNoqvjU+0jlDOwxlxhRR7jSf0hJAXn5HIDsAz9kfN8b7p5v+D3vg
4mowSrWkR171/k01rbEEOsN/u8/Jd7C8Sq728sa1CX9UFXpgu3plfm9S2Y99N5sT
VlFGOXTl0ACAW+IHZI+9I0NobMSCni+YUozsJw1s2VLqH2IzCVGlB+ac7zGPKNY7
fsoMGGEwvP1pLXGfDYpMkV7sq4cUHG5sktHxq3vGiXfKXX93+vJxHui0ArRW/C/c
D8lsSBN+eGo9m5pAqW3p3AYW9DRQwl9ld+rkOeLaq57r1lWMTxQVdajoglmFGLuE
fa5j0G7g6miwbxmmGg9PGquejsadv4aaj/grJkKzS4zjtpxKrVepFCa7gljjnK2a
JZIuFlWPnwr7f83nXIp77qh29rB/g2k7xetmCBxp5ExARRpbdQY65OiM6ZmurioG
GltQwQaJoBgggKfe710bhCau8LNuau8fsqlnp1WBkT7kS4dkaG4A1vEcl9qWNSxd
YyX5/MBYlXhszG5Vhs3xMa6YlTg1zhA6aRmrSy22kps=
`pragma protect end_protected
