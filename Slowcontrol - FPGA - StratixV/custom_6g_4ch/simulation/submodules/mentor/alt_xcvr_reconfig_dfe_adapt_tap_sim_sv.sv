// Copyright (C) 1991-2016 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, the Altera Quartus Prime License Agreement, the Altera
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 16.0
// ALTERA_TIMESTAMP:Thu Apr 28 07:05:39 PDT 2016
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
Ie9ynJg/PULi2gXp+ZwCQOt2534fWmBJ7L1jEMAeDVwmh+NytxBF49vmy7zI1iSM
dDoCMKzBIe+CGPtniiHD/wPQ+Lk4GvpSGgr6H29GKLo/VTHH/mK1gtDiwcB5M22a
mvkpTaeHYTMpc9WkoAQHwessAnA2gwp8aJ4tw79MJ9w=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2736)
ehJGm9b7TQj9h+Dr8KvOIIBh4F/unnq1n2HO97R6QWexrEcdqpdyw8TGWzyE24FD
q3Oww1YnwowaKXFYV5rGxygpOdY5rbKQANn1PgzK/FTRKrQDV8xAPBnWlvHuCK9h
qJlYLv1wGIL3z1uydTC1ixMHHt2w0HcNCaX8R0VFDTCEyUtq4+EjM//AZdw/kFON
Tsb33C75yy+w3qzPAv+j5syJ5xlo6olyGsqD3R2IYBzxxRutE65B3brzZhbdGjo3
QlsyjUhb9ARtovjB6oSN7rPgvDMAg0xcrJBkQb6fZrhSDiLjkIPsmQt/ko110uJf
Ak7ijPuLVTrlOREnb/Xr3CweQcRnJ7FzZP/K9KuBLOxkIzEdVOsnFiSrOU9JK/No
o1xb/m7xC7R5JclX7Vft6pVf5mr3laKtRkwkaEvUxXxQOXHgR8RoIpouUKMjeppj
O8dRPXOpSG838Ilz7rZS2udNn1uDA6DxFbqcGFB28KHycHP8m6TDUUwO6s9+ya+u
Mvau4BBd65GHachM9/Lw8ANAoUXgvdt5441zBSqh3NDp6hfNH+p+FlA71o8waJK2
RM++8eaeu82YOy/7+DxD5emKiTMorfoBXnz2Cb4giJblhVq9WVgMAfi1tNl7ipU/
9NUGSKQYSkVbPJevkZ6+psRVPnlxmcQx4vO4AsNOSx2vyux2+6MI05vLr6Iq8Whz
jZYMfhL9mcCGZLGlgRjhVGre6nJGdXUpvCDTvwEXtpGDT01xCxrwzgikBLIfSuTQ
pmzZn6poiW1xxtPWNq5uDE3U7E50DyrpQLd6uZUIPBpCASfoM3u3GipCn7n0FmNl
3u73qrzdHpMg5vqlbn37zr9Tn6mqE7DvqnH1pw/DV82V4XGXdKc6/8HVzduBpGPq
ReeesZaincCEz96s2cQSwLhs93Lq7cuIZnSLUcWang0HxXdcoaj7pi/R1619Yy5R
L3mMN5q/B+l84DsKU5hhzwkU3ME4YEU7kyPZnPYB8GfSut6mlPksZbYRjfPs4jMF
PuJAQ369YMBtOl1fuws8RBJKDvgX4F+JSqVl8jEQJECnw0T9Y7JgqeVQorvDHOKS
WZu2IsNlX2KJMcl6PTrYqKfet43HNIlFbiuLPwUlh5MgJoO6yNBL1YkHLTO91wyL
lDp0oBnsIVOZxxL3o4U+QGS8sKeSVs2915lLrn+mRPW6w2ANFA668FJRU7Dd0T5w
AQuCN0rwWaMoXP6WjgQD0QdW1ROi6RDHRSbT4qnYnEP5PP2WoT63kLnIM2ph1aJs
lE4icKSAJfPoh1rmXCN6O2VeiGPLvg1gY5g/Qrk0adeSNctdwilBA/ocMH7AKYbv
4NhVIYszhtfrYF3ycdmfjyCVC3zpBLoDTqMyJFDCbbZWerZzh+3QG9Vzfjxy4s7E
oY+kIYh4BOyqVcE4dCbvNOi3ygBZi1QH+sQy3MAYXQ5YZ4RZRNvPhwLMNTSB4DIj
FruDlXE5CvFLa2bDlm62H6NCLd9AqeHJ1n4LaAj2Tgxe5XQQ4jYhshh8RDjBqvoU
uo17k/tMrYVtBlJpBDkAMf9o91NcYAZ+bX11Mex3PSRMpffQtJbqD/CPc2ce+NNW
VRQmAvbjsQhKRnTISd2W0v50G/+KgbmJ3k5F8NRQdLiOAVR5MIPfjry1Ehd0WFhJ
ci9G2GOJ+c2T7yBWqK/JjPZavNBv0i0EgIHI70f49wQBXz0E89mxet7J7rezczlY
lEwcuwskjFbO9xAawf4dPG3T+dCUD64FPR8K6Q2UOBUPUSUhBxaZngLh/v+xPbw4
Q42sBCawVR9W8Vwz1gHlqN3WMOOqPOXNf9fnsz1oBcv/kgktKicRhITjaYfhQ7kB
94EHts9Dt6ZYPzgUaM2UcUNJ6oqCUn6IFAmRyf1VBuGw+2ZjitaYgN/HKLA6REW2
nWFVvQWS2Lb3E8L+6rX7UR26Ri78zPx0wKrhyO8WT4JoujTGg2Q1t8ZC1f1zFoEN
jWP9yuV8bToF/HbroO6VEKvxp3lYXoR0YxKEMjMf5DtI222Lk6WoNmfAP4Ua+0XQ
JHzTs251HTbDAh+RLibolBB3Z8ZH5QALsylZ3euHypbz9cJ5EUbgv6l5i8qX6eG0
FGg6TpY2Iq0YJmbBdZ3wAGqo2nk9TjleC1Pa/L+FqghPgkTbWzKHrXp4lVo8hUO+
PeFr7Ii0IAo5PvIItDBli+HNdodIen46zQF/KjjXsA85VNqqIxb9qq5kxhLrSndA
TVE1oihf3A4PdAgiMYFzjyXuqcnmescBhL1y6RA+9prt22+bW0HBM8Ax5dpliOCI
pnwEBkYUFq1BdMfLMbYKPLillWKHrbRWiI4wy2HcGyk/5CYVs19yL47kAMpQvXKJ
DJgLrnkPk8RzzCD76mZ9OhGQRqi90opsJ+gB+mPRa2648ZV1aUHHSGjr0oMW5XSl
t0FvB8w1egs4pRyqsKmp79oqkeyoFtInb55agGdkwCK3aiYvvvHWvfmg85oYuD8j
n351WMpsiVEwU/fiwARh93apiuCtuUU/hhXSW5QtFUGQL/D0ZpwL5YELbueJ8ucc
cRIY+4orStUPhnerikn2aLuX6x8XEk18i2lFJl6WpfQPAMarq4XOpEZ1QGH1Ou9O
ohgoayQRmEWIVnGGf9yFvwYzROVr2jnOxN+bush4X7tUioUFk24al7RB1BpEtixF
mHBGN6fss+CBJT0VasisQdSuVFknH8PTSx9qhc1x0NQA+/ZsYkQURPO5Fkn1QbYP
6MtuLnIKbfbKtQst2diesBgcduvu+Ij5B/YF9uR/tgnq5SPRDkjl+0glizJzHu61
0JiFsl6rJDEJei2372wyPZSLoPwjdPHbVACiqzMa5mbAjDlf/GHPwTSmE2n14CV/
18EU8RP7GrBM61t8hC91INly+cpBQzTnuEH36RoVmAuxCta0pcbokYtzyO5ecCob
0Gcn2yOf1XVauiCab8rderv6IEOKmLCB2+8Wu5AxUS5xz8zbx6kPR+m7Hx+cEAbS
YTnOfuokY5H1J3YBvOfaRUFPR1YRFQbV5yixsJtVxSts1GZFUo8owpDvLAzkl+H1
aItfc7glnPtUefTfoITZjvo+Bqe+JvaSeNkJ91RdSqALrbQIIdO+SIVmIj2Uk+j4
FGp0YXM/j+eCt/b18GtBcpWvIVTfHQOEdMdgMkkyk2o3KuJsuefQeRPMOHitkKDT
7jA9b6PJ3XvB6Kq9AwK0zxB/Qm9BMAbzFaf0LqzeKQX1015tIGRAbIoEF1ULaVow
dHCGaNfmUdiC0AL3D/HsHvxFiVxhQDHA7HAJVg0+OQuLQFPQl9+Jrpz87aAj6nh1
cc38R5cktOtrwCtFMmLknVq3V24qEXx5Ymvyr3h69kbQE/YdLNGA4A245XfvnY4/
jrtrXJjXu0AFzT+1PP2gMA77W2sizUua2/j0KV3JoSSXjc6fEFruTVy+WuGBM3Bu
LlnZVUj3njrXXOD+cT0iw1/b3n0M/hjit/AtGRoj+uCUIgKpoaJ7Boryyv8ZnKg9
yPP3Qtru7ww5q2Ahf7aBOzAfteS0Pg/pH1jU5++u3X4Gys/MaPCgRC+eBXNFDxfs
eqVdtD3y1cor9B+XMcN1oCmunG7K0xIb1S8XyW3b0uv9XDNyQP0EO98MJSAUsl8O
`pragma protect end_protected
