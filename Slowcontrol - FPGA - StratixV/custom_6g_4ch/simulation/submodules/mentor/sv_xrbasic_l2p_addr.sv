// Copyright (C) 1991-2016 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, the Altera Quartus Prime License Agreement, the Altera
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 16.0
// ALTERA_TIMESTAMP:Thu Apr 28 07:05:36 PDT 2016
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
b02bflU2falxbFC6OjvUUi1TkbvLyLvVApBCPZv9Fqy5jXlNbBcUsewL5dF3Fp56
7bxZiTRFW4NtcGi0dBKeGevwmHMYdqfu6Ejl96ihdmxdUDFG9ZWciLUFfiZss/Vh
w4qw0rY5NhIPkcZnUsS5ClSts8HOwNeBp9FJfyfu2lc=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2192)
lQzKuCp58W0ICJ8cA4HuS3UqAjue78fgBLpwGRBg2/gliQ7GlHAsEAC1et8AkZAu
AyYq4sPUFeszu3b4wQ9LcPL6xW0CTbT/czaj1GX7OAAxTZdJelV5vTNF1sy5gjdn
70vP12+DMD7Z937qONFgHc9RZNio88clL3y0arg+eQdF7djlWIZi7Hv4jnNl6nFv
+Z9tbrWyCNF0+1nxsnnqRQTyAI1IpNcS8dBJ48cFCq4rcwTRGAD5MsJncozk89Cj
bxpybe9GmZ+5k9RU4xIn3Mc2l2dOhfTmf2ZU9nCJrEav6FQX3h0GC5NzQdOQB5nZ
MPC1VIoP+dosRrasy6enDpN6nQ0b3oYU41EC+lrHm1DSeOsj8Y1IY0L1xtKHb/Fq
Cjou4MOtXyEpfwYlu+rqYQIt9a0x00I/6eA9Dx/TpCtFRALxC3bRdTnMrRje2i/3
z8ElcS1+VP92jCvsBZf31ITFMwZ2p+MYBKVk3r8YrGGdI8xYxJSwy4GyVaIAJ6UM
CmOOlWVCQ14tdQekEMytqsKfP9z6CHQkaD/eowPR/tAy72rDzxjZqWtT2Pt8NjtX
RjS7mV/+R1/npu1dJxRHEUsOUmQagPe+J4oRw/Anok9y6WZED3oo6CwhE7CnUMvK
FSFuWsPxPhOpBsIBEtCS+4Nvm8WFybcod0hSIr+3uXT0+uHVPEi8uzPGQqrMD942
NJ/jXna0NeWnPD/N/9ZAef+sLWLe/AmsLx9d2nDAie4EgmyWrJc8vxgHnAbPCEXS
EBvRZZKvne+hT2YPYA9jCaTwE9f1siq0V19Lg10h/29RmTXO60XDJith86Af3LI+
rsslIfZeApyOrYA59Hu2dizT358m1bVyegqBR+RiRcsTuSYoJ2x21dUpV6bk+9jd
BM5zO5aodEIj6nLVTipaUzxzF1fHtIYy7pyjP4tC1xRgZR5y0gX4zShqDLh9cAVm
vqibs1OfUsGXpDqfwZjIcsxnpYsvqvkVvHuVneRgw2MpFNNAHbkENlWw01KTMr4w
OTog9EP7zBpXiXsfc22Yx7OevC4eTrY8JL8CvTWoFF9aFphPCVMevWF+417T1O9x
Fr/IXdYx2MQAMtuH+KFjoTj7ePIb1xjZ5GgENxQ8ePI7SmNaX7opigWOpDurDKpB
eYFWGI+w3RDTvjA8GJos4HAmv+kiJmmr/B5nOdjF/2n1kdgdsu2mVGlfSanHekN3
/aT8e3iGgi+u2JY4m+JjrEDceu1GR/njZ4EGwfOUn05dLTn07S0/PUCPqqV70djp
SEkYjcV7OPW5O7oFbbkuCUAPjabT4ZKZBYosZ24cMeDjCUzBCxLMpIH9jPWOfLIO
0s9FpFXvbbtGw03doslQMj6FKDaxNkG6icBV2o04j8iJPUO1aNxzCA424dnff9Ok
YqWQYObbkb4o2RLCX6S73P+f26nCFn+084kucyXjAc2iMK8MCtgAUhY+Z2sGCedG
Vn1r2sEBexEiWBECWDUjU4quxpBCHpAtWm3KscjxhA2FvoC7SCeNJuhE50n/eW0N
coN5r1+ow04II/BiDCLzl0xusO61sSaDaY4TtcgNB4Y61GqdYh3cCdN3cd7pcXDQ
2Iz1vPlr2NaKWT6n8AjECKib+iQvKv4CVH6HQb5sfgOo9RIMyPa3SCacl1e06XxS
XNXFF3h9gvPIA3Wj4BOElcBNmpbkLWw7AP3YbZiMVDbJoPDdb8SNv6GlRm+5qPVX
wuArq1fGy+MYpciTrfkpbJvy4joboij3l4SzBISxN68kNOKScb2Coee64TxhkNFQ
nGHmSHFCjIMwKgO+LEApVYWkJuJAqplY80rA5yKC8v2A3oDXWCspgcyTYYAlXK4D
LAvYPfNIv4ZNU+Nefz5n86CpumVmFrsiZK68d6PDoNG7DmZL1BdvM+Ps6tcNqHpd
WJx2fmsxK6G9gp9CKI3cLIFM19NHAlb7+8ORrdc/fcUUjSnS3o9zBq+oOfKPusCd
p6xTEATb9Lh+hCTULX/xWTAHt6TSC0H4+GO7ThHe3vzGw/ZKp3ke6PDUas7ABId1
4l0UrUlhpGMUC7XLyDPDqn0xftO7cE64prvPhSYr/MXqWAnRC9aoN/EeEgbCaeKR
NnbwMAxECQxHopg+zOFYRweRVx1SezdF/8U1Du/sWSfPDQhamfJ28uA8kY2xJquz
aQijNEkXEr9LYbvAkkZHRf12afvjyfaaOHZawLKJjLOIO64rF7l+zEP0kmX+pdTU
aOkIknmQy8XeuiU5o7+5kN+8/cYNS8w5LGapglMsa0YovhKYUUdFqh8qeqQHtfFE
t3jlT+ukABpC3I7tm7bxXjIYiw7vW2HKq+f/5RS3eeCpF4KA3lMujS69+BA04e3n
Lp92SQmTz6NS0g+Y0uM4tHTqt3ARzD26/+aHFf8uVQDj1cjS6PIEeneVH/YWX3Zc
b3iMK75J3c1F2LhKv7HJOeHXlhYodKsVnElyf5T6LMnTFbBIJEjbLm8w7bCJ2W2i
bFztw5xbZjWqAgEof8kbHTZ7AgH9OpjTrKqjSKXtQmRhv+/w9Qzzi0xP4PRyiLYo
hbOJdwCVZjuFiW8zKBkfb5Qo0Ha5FVPleBqllBhnC8jQMCbHo2UAqZopFgquM/1j
fYKvhjzX0qwJbhdYDT847odduABhl8QxBlcM0EEuE8PaH1vF7zGqmc9DST37MZ1j
enbLlZX+XCnmP2CxqTeMAQE0TAG2I9LSxYnAGzSlM/SJQKj3LmqhgBbcnUymQI/w
Pn3TcZb8pc1gt0/Xw2UCjgsZprg+qV3iqZz2tgUSaCFxcD3enyxmy8fgaHRsNJum
ojDCsLOi4u1Qlc5G8oTwkh2nxIs1qpktHVm9RiSEgUqwBttvPYldiCR0Id6F5ViM
pTCeAxoUhldX4qloJgwYanaGC+EIZm+9MzHFkfy1AAY=
`pragma protect end_protected
