// Copyright (C) 1991-2016 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, the Altera Quartus Prime License Agreement, the Altera
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 16.0
// ALTERA_TIMESTAMP:Thu Apr 28 07:05:35 PDT 2016
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
QQdU1I6TtIprHftBCZel3BNtx+1R6Wd5EcFA1Pg4us8ZAilBbbOhTPwfn4l4mKEK
hafA2KE/fxtCbV0JYzPDJ/sdo4f7Gc0vN2GPggSW/IPuTvTZp7r7Dht/VHmEpxXE
k1khhvPgtdgV4cY5mPGSRIFWgP2yBZQaB/TBS0TaAN0=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 5792)
2s+vAu/9a1d4pOvM2ZqlQE0HawOW1q/R6ajNflpHn/9tWFYi5h5YMG8SXzXTyFGR
RGg9WvtieiRbLat53SIuDBvKnZxsXC9xasgaXWqSEaa5ULUTSdTOJicjPm6vWiJU
IfUgI7aNixSObGYWdZ0VLuQl5+UBLpVD19jdg1g/F9c5ZeTe/AgkqTHdaplpo9uu
x74YRzfhY+rBcb+jEgSzu4tNFS+uGzjibuPR7gStfqtv7gOhiS3Jhn9Aj69SxDTw
ig+I8SXegJcXl8G6HBf+24IRVvIXYdVvfUJqYH2svFgo1qlP6e5jffXE6bY6/Lth
QwwKYflo9/2qqcX0RcR6nVJDKNX+x8yWOgjhwZVjqZl9uGCI59cNZzcUGCMv9Zvt
MwN967oRj+lbVSAk2otCFZxHLxQVPv3rFBJr5V+4G4pE8b2LrluUje/8EIxq5HxT
DJNyebjJTfpNYrby02KC9AGnyco/Ac8lHXGWckTIbzd+5e3P8k4/k/vVidw8BUmG
zfzpYQAws7japbp6OVJhIRkTRyLP+jGr3TY5WsG0XSXagFrtw6flIaNrsCcQ5tFx
f0oJ3Y7x+OVP4O3Os3vwBclQ2PDHx+Ef+9UIeTVAYPretDn4q06PxCpCFC+R4UeR
Y8bThfc4yb/weCaiAM7IeQAa0oRubO3pXAtEbFhNICvGbNP6epMHuhnv489rZPuI
f5wrySZfHjEpC3ZkCLwJ98HuoA0skOSIjtkcNSymqawa09BcRCKxvN+4Isnsx+Ts
GId8uY1rUD1a3RA5sLw57CkmT+VSXhC5Acyc3/+WLx1yDuogJhHFTNS0Vs422eJT
PHhf1UtmfODMKjnUlhENxFj7h5C9DDIGTlcSaw04S3bK+BMn/Ty262B3QZwMrHI1
NZk0WhEj0QWL1ySo81Ix4x+FxOvkgNdK65WfNiGSPIIRPDi3xqxPHnHYCtWYTlLB
kNk3OA8FZC7DlxyaJUqnrw6k8ofMKUFtCY/GyEQJ4Hjxgrar53RwHpz0e1BUmY+5
iUb2EXWZi2cqdWVlsAp0/6WYMZImBPw8943S7yp22KHdC3HhjwIk04l4lyF8dao8
/SL8q0tdnyws05EuYLpj3rRtcSr14JazBOArcnFttwnnMsxqCrwDUIKTgVizk+Yy
ZmrjbXNF8iX0RA0NG9npiteZr9E0qksVwsV+tqsJ8yNXleOyHxM2HfkTMi/vEpS3
dhdM1dVwzL+Wp0gYiIQXJBpO18PjzAmzBlUKLVCzFvF1mg4vCtcOBMb+WeCekRHW
QdPetcbscAee2eG/ovWn6vYfnfmAHNE2jXPCrYeZ1RIjM9zW+voqiVlWMJuaTtp4
xPSZM+IbQKQ6+gNE9M1Atp7ZI/DnzY+2tRQu4UaL24qYiLSChORVu3GG6VHu5VGh
GddtBngEXsiH+amnVy+j75p6Ixly36uDNJlqSzs043/v3XaSzQjsM/tteeFagOHF
iFM2dH+CF0IUMResvzhmHzAobOYEk7OPWBaMMGcujmfbU1TNa3IGCkszHJc4Cqyi
S087TdOzcgTn7zuyY9DmORVcjEOzv/2y9TrHUZCmwPVPis4A8Fif81MvMG+8mG2T
Q3xnLUe6ZbAdVg9ZKsqKn3LFLNLyiwA8cO2Gpxf5AOyutpgtTUo/nedUn0JmXo+1
p+CaWMLanJnvnzl7Qxf/ywHOR0FDX4CYWONEAHuWEYKFMapnQJ0ctPPnR3CwJstk
OeRtkDTNfrqXh4bVKOz1l670KK/tnpLdEucKaKvNAPbgwpsSec2oRdZwbIBlg/ss
T3SM5djy72YldZMjzRCT6h4eUfqhgMSrrLtX16QE5nBlKC+dTpU2D12jV/N2kiUT
APKlLGQTzxawfIfzsz6PELBejQ6YULLJuw1k4QJKXx+S9w8F8jGelAgOiexGskSz
FEZ3xS2MUjalOeqy8j0Bg8RvXMznwzTcPntXRM96mVMCoLvH9urCbU+4HHImQ0L7
djTR/FMwQ4hUIFGTC7bOWTP/m9uiyowz/f15tki5/HvPSrMA5c5Ih+zq1m9mcw4w
PM8mWAgY2a+c7Hx+Lny3P7Tq7ISh1kVpBca17m7M9btqOfdeJKTv3pW1hDTUecBA
S/R4gEi8b47tFv+lXvxQ4ZB74bo0MWda7M3oLguVrWXCX/BXQqXUcaAhrzykbP4h
FGRNgfzS1D2tEcKBvnXHhVV5E9fck1B04TFngZs+ly2g2SICBEiexXn1Sld5+fQm
PhIeFBxgwQnWxk9f13VWsBJZpR2QOiuXTxp4ODdox+YuXxj9Gm+pC5dI6NC64y1p
3bS5/W4Cm10gU51FPNJPnvnP7fCxwEJus400CBs8/EUzaHdQ5fmRqJm61S7+sWTb
395kzWu2jy7q3QQNAUJoiGJI4r3wfAO+EFyqz9ykf86h/09LVQjS9dPxaPtjb0/Y
gm2PMzf2BKRIjE9D6+gh/iElYhUA7AsGv0zaT/3pipTTnwCDtzsSLQiJpfSgzEFZ
rCsoSAAofs38veVPGzIzsiT119VNo2Q2Cv1yzKrEVRP7TfAQdLGR1NADsEMhzf6M
GMpEJ+gNJqIKJcGFYwYPN0YIicYGhRYwD/ZRE6NzTHKOvgVfALMRUcvAfkfzdoHv
DBEog9+37E+Ift9AhkwpI2h06K0i8NeharmrezwS/JwzJ2CZ1x7roRxRXdSZAT7A
vwCgA2W8cVwECge01XWLGy64Y6fth3FnZ4XTvXTZF97/Fpg80JZx6wZxLrlwAi4K
1/BhU7eqdAa7FzygBcIvaquPdjAY3cMwZbati2Fdcj/VJbP5SWOHwimrOeMQLQK6
9CZ/HVcfg7ZY9Y17PsHa0j46NpCbpO5Kl+Tbvb4xvYy1dDD3JUMHwWPOZL0eI5gr
PItSXkAj6cC4zy/GGFP0LTDk6YmxflHhPzSFImNMJkvPBSq1ucg27zdvDM0besGL
35aAngunVH4N2SdPhiEXhv3hwDXjtW9WD6IR0fbLfz1uwTLe+kBY33ut/YQ7oAMW
E5IDfQLD/46IHA5YQtheBe6px2VKZW1xJxNsaPOAqjwcy3h0y95PWQxpH5Ok2xhV
opXYN6zzUPbf8bo5CIX8VaOFulgOJQfAo9Zm4ZY98vvw84p0YxiVAr2QW/Oemau3
w2mV9xUUS5o8qEXot7MN/gb++dZAjlTMPIYFB5fv3YaUmD1mhKeQQ+f94ngsq1uj
m7QoiRYoF7lG1BsMZjhlPjA0nyl/ewkvCtxIEVHSv1j+BZyM+J1nnDMF7F0OASVj
u43cpDzv/inlj0vhdHES2BYwOhLTt7NdgiY9oUXRtPuW9vb86khyvI28cPV03zKk
MpAoch/4sPe56SMrx18MVFB/NqJvX6KFkoiDpq9ah6zVqzW0ENwIo8RuSufcTJJO
sAtH3urLF5qJYrGjvF6GqFshV+E0othJG6pn03s3gTzH9TZPEQmBi4wDGbK/wyCQ
geKwil7skBEfZu0GXhgLWDsQI89sWZ5GEaENCQjfbZq+6huTuibeJRyL9EaKMnzi
82DPQOpZsvq+4UkCj/Laz8NIG6sdrcJsp+n5nNvuuYXJNMXtXz+mh5lUwxwO3Hsn
nQyrKv0AmdMhT6zQZ9WebFu8StD5pHly5smbX3iI/ZGqKr/hCc1GIaHhje3KojHJ
wCMfJbn2f/g6cQuhLm5VMCyNvGhDm+2G8n+YX5ERFG/M6RrgtagJIG75MDAOkOOC
pFqaGrZcyyhUB+etSI3fOk0iOjEJxjeooi0XtVQuyfMqYQrmGhWIVU/O4a8OvPZw
u9qzzxCnUQ34c8/gNj5zmGGH3krNAWNYnQTOsYqVkufz6dxzK5Io7SKdYCB/gRSK
CZv/vZosX8YNTeD7lnOArX9SMVgKGeAlmsX/9wDypIPncg0lALVGbu4uNQIGCGEZ
9S7CUMbASZm4Wagh+XZWrL+exTGP/KAInx+uj28bFwF8cRl4j4HzPo0u2v2wyVt7
1FZ1iguO3PW7oU/1pmVkCaMqBgSi07a01YzG2Guxv0cKUjsXKNSg7ew8KSOia2wu
A2JtGx03Lt7f1r7w5ObdvEr5fdamNbZK7kM3SbXxm54rRTyOfa6mManY56ECS0Iu
WjbnFOIaqZCHE4tRbsupeNQRncnQ3wUK3aGgniNN+nyKlD/mQRu1KDYWkllTe+A6
8hRNOlBlN45fvEKLSLEvJq7lhJuFfeH1dgzP43ArKhlPn0ZhVEAIJhdCsUGD2lLe
BDvocqnO+/z80VD9DUzBKEudv3hfFsWqRDlrJJoVrrYoM26PhcnGh3W/uZP3zztj
geIbWoy3mVaxiHhHkEI4dfbxsMFtmURlBJJqIEjLZMSy6BDAKb4f8PQaUWQuNk3C
EEnX7+KTxzkFmcj0PuX4Eo68a8kkgtTE9BE/jVbem8aBzqSiUL4GYaQ6SIk+tiar
Y9iQ9Dd/jvgjqnxmwISpbA+1cxtahmpW/cZyoHDPEMm6MdoeofPvbeQaW4lomDIW
HniQnB2qX6+5h5y0MQKMpVlral63GWuFnIDfhrEGSJt4QJ9meOLpVp7DOgN8kBCL
KBvxgcrcvfY6kSVFoWEFLpkurjeyQhFUCi8HIhjxrq65Q1hJxs65Vj07JNjwF7vF
jJRN3tb4L53APfE+pnQwkbLRbIuXQ2ljWkkTSTFkB07bnZKgAdJhYA8PEQwvm2qr
NLozFChEwQKk7uOm/AqHEWc4FbRmaGqtXiyU7aXPdizpbJ2X8eiXgvJ+j5Uwr9xV
V4Y8GPwdk78w6to9PnkCzgANIVIxGwVKoL9WlJ402UN5c4EFatgJw+spco0GXiDq
ZjF0vZt3DTNkEeXaL7w6E6YHIzXN/rNBey+7l3SlGxkUHRk5khhThnvi4MIeMB14
JyV8P7o4pHY6vCWi4dYtM6HlTlXYSPaNR648ShDRYtJhZk0MH28zaeJy4yi2I4eG
QlHoM3YMlFWKXLb7agqTsD8hYSfVIYGdwhpgV/sph+8WG9FLqV0BFrjepZfqN7/g
6rFoYVriqSfsJZWsZZ1eawYFj4PulYCdOYJ8JQP7pbRNPEUgOoL+maf7Le/m9R0c
ZFBEtcGVjNpWAPwBS4lwGS6zt9H6sCxI8couVU9g42bq58+PHxWomx87wZSngfAX
dtDZod79RbOr19wvSVyCqFQjRUMpS+Xx2MO7/xEJ2pdIIAu3JWx51KvFsUtHHylX
OhtCoGHm4Q9SvxD5b+8u7N0/xtcmkg4rtAwWxdAHSXjKtR0fi8jF8zuIjDFAT+Re
Hch2lbSCwQzPsA29jW9LydCtpt/E1AHmNa4FRhFoLZSGmMX/bfLm9x3hljUJu0GE
YQx1clTzJ1+JhJ6S/rMfdgf9GnlKwHbiSi/DYicjH8R4fmXbg6tsc6eosNQW/91F
9cl7kh2MqZAvJvOK0jPSJiFWd0uNNYnb2x/vE1OFFP1COaLckvakMNwayVEL+NPo
wOxsQ9DhVBlHalEvnkU5jDV1zdEoo+MfM/s/xlLeOFhvi3veQzC7QffVpTP41W85
u9Lxv5f1KGbcKl2KhiOKUWs29ry2KU5n+JWbtCk6KKzu/ZcFeuFCaA+2835qpciE
81rptnedAxVMwPZyRGBlf8l4RY81W6mmGuMTFJl50xTQ2xsa4fFPEpXkvhZ4qRk0
OIAZE5L2fq/7VgEhAEZxHyAxLBkCyu+AvI1ss6L/Yi/M0hFScRht1KPBHvTFCLtf
EtYis9fmWLCMmkmtfXYBFm/jOR8pDlZHKE+YmhVstRzVZ7eVm9FaFWM2aa+gS1uO
Kk3RBUtVCZVpMmLHa3y/vD48Pk7XehHh9Dwx3qJmTrqhrnFUcfXqMlKrbjppQ7X+
BUTPrzrPGqunYrZj84b3g47vRcOGF3N90AX3tCaYD73fZ8tlDr3KijgHPbmB341N
ArJGD9L5NBMkZbcDbkoTaMUPR0+lWB4Y/Wspl5U8WbCRVaBia9/bcpElnBRvUsKv
2OrBUUpyJHt40ly2wa93z9mr24TFmAA5CbopNHL2O+IpzUebIhZYWKxNYIB7OelS
gaCdjEhnal3Fm/6LxxdYZemrnk3ZLx7fC6Sc1cMzXsfW0Cx2y7NgX/DCkJ8Ejn+z
sFWlPgU17IZL96FxZtuhJ31pjKcyGmcKOXM0oPQsdKIbr5PgqsyCoeEx9Qqr1/jC
mLNufVJ8hTOiUFzRt1WHlBYkKL+9mEKV5AJGNgwxo+fuhVe0FK/hdA19WDIxazAr
pJUrItyN3beP8ceFFSL8F7gfo1uPYGpiVnZF4IySizC668VnQuswSSE3e4MB/13E
4CB7q8l8N2ZYttYMssowXoYWEbmMwDtmocGQMP/xCMDSqDBtP5lPVPDdD/0WoM2j
4tFk3VuhPG/JM2qpwOPwZZq5oczHUwpDR7hrAVVoC7pWFmrmPjfvMXN+t2pdIaLG
MdTD73ZDqBbgelMb6mpyuqEryYsP4bV+ADL6RxVearZzkpB/XjKDsfFxtZhY920/
a7A0ww0WJf6dPTscJkSPQSViNUt4RM94+odS0hKSUAPo9H4upzwdzoyscov298NY
4hcFiq0hiADoTivp2Cn8Ey/DcSpa1yohiB+UQIDM4x2SUXEO/Fcy10cwvEzMAT9X
3ET9Qixe46gXOvxfKcpwbGIyKNYowESHAJS5rZUfNZVnasZJd3bz3w1AxZX/f0X5
rnKHo8bsc1QQ0j7CMmS6Is/ZhYLyxznWZ8hIOzuA+0GrxeFHm7sf0ZKxJ/GKM0oo
6ibjn+ASUO919Hnrjn2Wl3D60OVUkeQK1F2z4Btl/XZ8rXFun+CYns0vkN60V02p
jcuFF6AeNrYL0MX/2V2ok2uEQOovtK0RLFopKxw1ByJ677jksxhjyqKrEOaDYEXq
9d4ejVOekbu1cHiM8hxbsrFCnW3wzJyuZK5q3rEOdmCzDrDfzfXprU2/hmTYhakj
lRHlKpCqJlyGBLMr22Un1JKa/gAk3J3lwgjMbbdWWmXxxKMRbcqBtWEExNhMVzZF
2PWdJr6nyUODCpNbXvYHeWOJyLf6Km0xENAunc3sotD49RWsai+evLmariPTdcAg
MpyL55k8IZBzYW5Y6Rp+SABSS+/kMNQfGFNlp3BzsmKTt8Od0fZbpnStmsAsyVCc
eHcN1TH9SfH9M5hDCQOpC4mv0mZKquJ852XqUKe4EKL6cFwjT7YcV5s95l9gOkMt
frFDwXkNplZ6cVAL2OYhJgWrCVLJoHqXh8y1zwHqq/qk8OGCDxIxYC290P50k8Bq
W3T9ZP6+gCTtQcGQWlRnQ7rYCgnbWQsuf46gQFtRbpFwT+dMG6SpIXJqHkICNUBm
Hy0JZcFdK3B2Jos30dOu9Cx/3eYO03SPQuy4Rv/1wpl68deYypqRFtQ11FNzokTO
Wkw7id1JmQcNIW6d8oIxUYABcPIXNsS8mkXGPrkKlZ0RmCshNSFnIWsjZcRkLUH2
UaDjYS6uk398ph5mmcDy5Bt70JDMCLHAh41AqxlYZCH7QNutmAleSkMPHepLkOUG
TvbVneXGbYIod82AnM1DmxGiUgUz+y29yRabsn9fpLWqYLy8dwLcpJHzRFPM6eRu
MAsGwh+ZDLaLofJr7sFaU4AKY8tx4KhP8FmckVB34c3NcZi8y9l8/pdOPydNbqld
AnlP0dWiAMa4NVYZTQXdfjGPiJ1PO8Xi2T1T3pOtb1ZmqauHvVHnah45Qit636t5
26PQI6VBCIa9Q/mPKVl6do3pUUWymnUryh+P5m7HQyQ=
`pragma protect end_protected
