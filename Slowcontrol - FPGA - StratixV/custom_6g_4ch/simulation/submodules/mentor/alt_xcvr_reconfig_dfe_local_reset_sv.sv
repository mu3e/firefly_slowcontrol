// Copyright (C) 1991-2016 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, the Altera Quartus Prime License Agreement, the Altera
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 16.0
// ALTERA_TIMESTAMP:Thu Apr 28 07:05:40 PDT 2016
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
sRmmQ7R61M38m3OUSVtFCAoJvHf0v2fpPS8H0ZSBC8PyKE6R7zJ5JtJo9e1q7K1X
6u9xE1h5Ddp5XoMCPfjUl1LyOqqIAKwcTKyLqFNGZsD0yvikiU5iJ592AYa1Zn0P
m7zc+TBsrG2W9JglYBySLGu4vaCiqt6mm/bI47x5BrU=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 1456)
CzDPnQN2wPXSgrZn27OiOzH78I6wOOFHzyNb8vJe6JcdQgm4mLsfmWME/peK8o92
a8wQUYkbwRHS4IbK2Tw/Jl7zst5sxiX/3EHATRA2ewJPPlL1HzFRY8MNCE/Bw41G
B/P+AXPI4HeeI8RN54kGyTW9RO3e03NWCpt+dtRLNKm2/Kkf6uFCiUjolt2nlaFK
tM38uFokbxpUBVHxvLoezOd1magPVLHlbTwlVxcbaqP0a7HW8agKUErpVrtqzUar
Ia0C0FqQK9rk3HVVZkJCZqzLYPzkIZ753dFPcH8ytZL6BrynEgsnvgqz9C5pIMxf
3Y8h3Ia3Ol1fGhgqpI+OXSeiXPlzL4qTzB30183c8lFYRvCx84PScAdCA9w6mAO7
CJsZ/KUTfUk2RG5+UoPaJobwowX54WvqVnQaltrpgplw08vaMbFlK0GRjnxkYix1
GBbDCfjcSkkhxxbSD7ha4QogsFmiPDkuQMLl5f9OVP4xXN9zlIIUcrA8z9q8csqS
I3ONF4AJw+iFjPrtY/fR2sT2DcitCF2+Xpet991ZIxOq2JI5xEFAyazTS9EWsMm8
5hPjkVd9TRByD8obHpYzPjaSftRN7C90tvBrG+kzEpfjhge68DRoGMLZT49AsnrL
3iIODPeek7qJoUi6BbWwFqsuGKBaVZERbNrCbxbtuiKPVo9eAsb/KY0L0Tmr6Ag2
XKmLIqx/Ej0/oiyQWMaZyD9Cxuh8Epuwayl+ret1HAhwMtR8PtSxxGfsUPZshuUf
3fMdoEyI9of+x22KtC1WUkXFF+FWeWndZkXxgpiD9QTWPAPk8ScuIBMXH92hb4d+
b9toavfk46Lfsi30X9QMLq5/ASbLbbsqZvxMD65EBmodROqn9kAJLFLDtyJybUnf
D7VYheIX3Qy8qlw2q7w4E/dcsPJ2l9du769Pnf6FcMJoIZnvR3V9fG2BJuuESVp3
e+/NlMjWXeYwF4a7CV44hIrcT0FXwUB3nWQ4OQX/7M03+lmKgPiuErv2SiHfylnl
ZM40UUuPVPIKAAseUmZwDc5pfhx3jTSrWauW17hGd40MiAwide0kDfe8usSl/Svc
o6I4n5XGFgsp/a52vHtFRR3pjzFum0KH28w0tK9ktgiNWl6sMlk2qw0+OD8ttlNE
mDDr6FExO1jJEUiqdmUHdWoeR9CE2RnYgDCghAdSiMQtGfVpD/q0URtyq45W56ei
bik6w21rnZjA+nl7EccnPRTqbfzyETGnBT7s1Omyqs0pOp4w3+quY359xNX/U/Dh
sunMf5B6CZOcA4YB1zV331oHTlTjEuLC8t8XL8UmSfsOhQfc/RkvkPHrIcwv3vzX
eTg6dtQXBD3SWVwVTgm4q41YVemq1byQfttk/CcK3GH783ux4qIcG2Fwei56CgMz
mNp6vurvc9O0/NxT870Ykzqz5HknNSMXi3lMsRCCswFLrgCALDBikMUWPnIZN4R1
nbmSnJDcs3CBMuB9E2FGuE1i+0MDE414bIjGA9hGGZtvYhGl7lbQewfiPYvqDak9
oipWUSwOSxozl8x4bsWl3Vxn6BY0MYaR9Ul6QxF6sKUAmlmcFpfk73jZHF47g0uI
75F8bIUmQB5uWtrwfA9+NLpd1/FnAqc/AnKpAAcZwCAOPPj27twmS/rXvN4BBLSO
NflApIOuo+6blDO6pI/xLOey/n2qp/NoUaXdGU7fVI16O+I72R0BRCc3jcfsVR+d
QQ4yXM1ORAbjrX88tLTeyrW5JXHHwaNYpNgYd3f30qCgzoXrymXaAFR5woCVjZuk
jwYNCRCGPz9LdsbUlhvbeQ4eI6irMnXhWCl9YAwEXH1+yW6AM3Ub4yq4D8KRERE/
cdnNEogI4htABWBxmjnM+rJ+CGZvhIItRk+LLLefp2yKtPGXMO+TxlGhIVMDKtWF
d6Mf2Y2/MnmFWYzz9KH3wQ==
`pragma protect end_protected
