// Copyright (C) 1991-2016 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, the Altera Quartus Prime License Agreement, the Altera
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 16.0
// ALTERA_TIMESTAMP:Thu Apr 28 07:02:12 PDT 2016
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
ttRt4q8FUa+vyb1YiwSeOm1NvMehPNjBUxSbLZwUatqK1uBCeFjBYmffihfRxbbe
knK7hjhu1ft6Ly//J2BMVRwoTorQT1XBIXMerwXmrX+cgd/I0wVuSwixP9cwFQ4D
m5acknRzge1F4k0ggTGqV7ZL8B5FcJdNOU+QW4Sfs0w=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2352)
K1YDZ2v5bpVdv4sdKhJHWJvZSZxTWIK+7Dbz6p6BRPQJGHTPkObQlb2JKjfgGh5z
9HkFH0t7RcsmZi5FgI2LUtfKoauZpgvhkuEhOqbwPwRIc3MSp27hRvCB+Eod5iF+
oxobB60U0BfBWICahvppwJQUJa6sAbOKNc5M4e+8wLynknmWqzGm+RNa6f8M1gng
DUqXKDvD7E07fYombvYEK7gIipWe44RSvbWcgabhU/19XMAQFSIY1F4u2olCCqKh
Hdsq1Q5n30FE+1SbDvz6EElZvn1DFbndz7sapXw9/ALYtbqv/DtmP5BPsPCewg93
U/LysVgApYyvEmV+tVst00/DS6U46+cJN70wbTP5Ff4FzuMgz+gRgBHgxGRC7hul
IIZPtMZmTM1BlUE5BwywpJjXFQN5jse1WqTn+JVE4Zmy47BuVDeZUs6JXfb3DB2Q
4FAyJuV4l+TCs4Xi3KvXlywDxUc8Deu80d1M0QtVR/buqsxkUukbi+tGStN/jqeu
1G1ZiK+qw1EE18Y/zmkHi6zMr6c2daTNlbhut0IB3vPCmuKPkC+EtBaxT0b1fhqD
OnsLfMUL97iw1djHrsfGWYEwZzz+4D0FV7EZzmTxlPjRmKnxnI1tFyRX5v+mBqlC
mZFGZ5LIvFSGXoa0g0SRceaixoCc7f0xIAPRi+f6t6VfkbCPimJiH9YErhYHZYg8
sug5O9MRw+wBcpNZm+C4Qdq3RejA0RYFLx2pTftCxEZI+DmQFNre9PEq9UU1uIcg
BHSouWtP2TEcLmlo4/yLHRZdov6dITA8kj9TkDfSt8m2JS02tdFw3qTQYhLr0Q8Z
NCTFuDyw92sKQUQ6HEeETAg/99zuqG33LQVE05lpH98lkGxn/iU9Ptmor58SovkI
Km8HiqCE7zU/fuheOazUjdF7G2nFlZ4aHbD1zzY4P026ikDqo1nNUI8/OA/l06pm
UHSIyaU8fDBiYSNzDHXid/y2N03tcUA2lsZw44UllBr5XNWk+oi+eZjBCaHPZByK
Eb9qocl71Gk1PfZaoeMfCfB5p8YTOSpEa+zuQI+nNYlpbWPwSlrpl25iLyC3DN+1
8LYNTpuTdgk3Ll9EiqDkxNyIJhEw5NmB2GSS6frMb7pv1/LE7whCtqpgRs7/7VrW
E1k490lxUwUmDPVCeBIl3iLivAXUxWsmd15uJFCYp8jiPocSGgfOPiEiHRLANDxK
ZvgWZKNoCIfdyDjOswLWm/Wg38rZIBobrsA5C1wtR/QCrwohz4BUONGn/zvmGDvg
4F/knla2D956rPzBjGOU80NH7r8dSR5Q+V+I7Bb5mG0aHtWNhjREIU66iefMKsMv
UXNTNcqP+jo5bphFx95/E9E3X6oS3r/Pt9tK4A61Ga70obRW6igS52Gh9eC8GeeG
Zvlr4N7ewkxgxHZ1c8w3Bm8FcGd2QEJuqaZl8ftMRD2e29wfuGedNwBVIw47kPwR
UKZ4hQltlL4IK3jcR5Y9WFWE+rOf+QabtYAPUkfCdD2AgvG+4xVpON6a8dQlbx+j
MWK2y/GxwO+5+EQjQQZsJsPGZTRGCLbabSSpWtKvv+0cnOM+APxW8wa6wAzADzFk
IVAN1dLkwW3qHXNVCOQMTO6qDmqsNx9jTi5oB0cPSHrAVCbCzOlwXFNVjp2fzJAj
0bNI5FcR9/+2tQ6snImYVGtHls+Sl1lgPnf+KjbMIX2wO5NzaSSVSG5KhkfD6JG0
sNdE0jfIe70u30moxRa01xgAn278xoPevcnuRBRIAvDdD/serlWmsRDuDhYJcR1+
5zR7hHN3OmYXrtzis++GvVYaXiOHEshRKcdATL8luxOHwL3jK9SnHo5+HGx0Aluy
BWJhw7b85sZUUV+KgmzSF8MT0ZNlJXd+Uns3LHU3UBLjJxRxomAHv2hPr4q3dTwk
kg3F1jd9nWFtmPJIKpH6Srj5QrMJ/LKcevszdZG3Hr9lX8fjmtDuQPJnd+dmgR0r
0DGamD4jN1dmFXx3vxREzJaLfUQlkp8YeMq6G8Uhs3UevwG9tQC4slBSATi9FISC
pteTbDU70O5SnD2GJ8s53CmSasXCWGNQz/QzmRz+WzC9YYv8l24LgRMMMk9HpChE
WT6xkLiMXyXWU9x14cPI/4wlCU4+esZxvXERDX3jc815++gveQsvpERGICxxo+7Q
UZFp49aBcoLtWf+EhYgxCoYOpQvCVtGpu+kjiZ/CIZeXvaCP+LgeFUPeGVe5kEkg
KN5pRVgxXMCe5I9xDSB92vQs5gXwGgpf8mVTPtnkg3ZBUd5YVwvCHZHZY7DfC/jA
DnKML1aBKP1CDuWEDdnW9tqi2DEcvTqWA2vPGJ9Z759pBM4fNIckF5ldTkgSZQuZ
xI+DmcUcens8x+tWifrAcO0bd8XSTf/TjryCof7naK8mG3aJe8rIw22PCSWTEtQK
dDxohplP0iLjc3l4CAj/wM2AID77ZV5Eeh7NdlngsahPpSBACO9lrnQ1RG2VMvOO
XjOS94/wilzQ8Eoqm2tCIdQ1nP00x2r/guRBFradTod3d+gqm/DcjYc1q9iqfuXM
VYab+UJaT7C7Vp6kACKwtiJimQoq9aR6QNVxzyJKURKyunXXFifSPyU+US/QfUsp
qbJchp5pQlwSEvQKfHPARmt4aHdGgdDPqCrhwqTLRaBWoz5ONvcLnddl3OXGH1+/
cytdM29hZozJZLSmyMvM6dejl4Fwc/sMW3FlW2j+zPOowv7raLdyxNfiay9FQBAB
6A2aQwCKzgQzoigm0QY72e75zetNd2kS7Xs9pSZtYF+I0/ezgiUhQ2vp+SDyYceP
ydbjVCVjMiiRBIY7OvvIE/YJGvorwCqibl/H0FUYzrTVtj0stGe7CcNphFYcjk3D
zZo80XLRW2pr2SSvC7ST2LkP7MCLSbLQuPdGhFvtZdwxhpdTFBC/AgMKJ65g1AqD
nDt004/WjaawWVMnSckGcec2sSJrOHu6S6pg+oBKtuJwDUsv8wsKTttyMVtJD3kP
I8bTvg91YT+H42rULIfk3RxReKyQV8CnZlYlzfF+PzoqN9YrTedWiyGILJySNAbc
UUAcfdLK7al/UWChEI+/vQUBehUIIlkhbsBvfOutFn/qnyRZnU49nS+feK25b/97
`pragma protect end_protected
