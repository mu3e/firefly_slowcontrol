// Copyright (C) 1991-2016 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, the Altera Quartus Prime License Agreement, the Altera
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 16.0
// ALTERA_TIMESTAMP:Thu Apr 28 07:05:38 PDT 2016
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
GMBrtT8aD8U/WHrhbb2gdOTKgINd9VcmIJ0D8lUi+3ps78IsILSTxKNZtrJQr5Og
M66+Q+dN/XMtSj2fAgUoO/WLnO5tzp1GRdYeJuE7/iMUjKS2y604Sjw8Vc/qJOy6
6e7W2W6Z4ZN5UBjP9X829AHfAD89qFa/iK3ClLwtJJQ=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 8384)
rSUqgAX9PTv0kCrKKGRcEp/YntellmT4sk9cb4RerU2+9MAZFpy1WZndyrGKEhH8
Yj/V4vMPI8CAJjWnW5JtWmqXa18yra5i95HuLbHg3zn3p/8kXIkSrYjPTJVwbeuk
yE9HhNW0Ao4VYifFuUvD/S3nN19sFeuknP4uKasbNPekJFI69dWK4elUlLwloPjl
QN6CFO0ciQ7/5WNKaMVc3RrwtASQgoZBlQbxv3Jqxe7VMncQk0kdmBBgwXGj5brx
bQLQPT86FeeND3DFTMTPj812CsFEbarZWf6SviHtF7fx9kqgeqzD0l9WRgv13hpZ
zqu0ivCbQ+I94pxq3OC+8Qa8TzImeG4VRBOOqMcpKwso1SAhyB9sIAJCl1stzw0T
v+dl4Fu/ldWpHaCQGvGwPLubPSByh9tOzD0amMjwT7C+7RZm2v80FwLptDU0mmNt
Gum7LnymFVsIIt/mSv4t71r+jHsfyMgvNm5IyVBdZ5u1+doMaspYt+A9kspoGKLm
BV+tOKOX7uNs+Zn1giqInIzrfJopfZ4iWgP35jz7O3zZNjgi655qQ3Gb+gTOD90q
6fdUPZHKviXCJhF7/voWoOa+KMFsQsnec2Qcoex7CAjnhyIdVx5ZBX5mGsZhGIXp
NFLDKl50KPQAibzcYnh80bBJr6+3PI5wAIwRLZvB/OWveiHYB1r1HmTu70hRUWuc
GkWfL7HGXLdoXl4osSv0lT1Z5SWT9Fb+ONqTDvRiBkZjv26NULQWP/MHx2PoSJ1j
0eRhr3PYsl1+8RWybzgBXxWZ1AOAEG0WKc5uvsECtANCxotv7ERHtID/v1Rj6c1/
oy2PU1eN+ghpZ9xp65rqxBXDL/72UXtegazDstQpTqQ6Yx9Re81IVvazkeAqqsjB
2c0iGk9y8E8AFe4M8lpNuU+XRnW0plJyT3UijRBsXwk59PlYy1b35XiaYDgXWLyV
X+4yD+uEe+oMs7um96bYrGAWR3AF6NHB4xavKhJ6TzrT/1aGmuzNDx2jSBn+EaQ4
Uv6DMWLanvVCaRtkzlxPphXuM5XrxwDlZ/CbYrIa82UEE7d5W507mWGNtkW0GQjM
KuTR3dBb4kTggmaWxIZE9tdvjutJPZ6nZWXaQRHcMlhhv24MsGHlhI+LyiJmtuRG
Og+UfrLkd/JR5woTM8Z4s6scWF+onY3Z0BXEVUaXxpt4nkStUV8JcYuve6M/fB9O
iDlBpv0/JkukV5WVLlxmd1nQjt/jNnIPW0/BLS5TUJG21MiHalnSd5U2gDLl9Pvd
7RaDAcgNPCDoLPHP4R6Su5xVV2EauvtZIih7Pmk455WExCUREbtQKDKNCtgI9Uc6
R/9vjBs0OBtzulYsjzMkoDYZ/BE1jSKNiqDFBo5KDWUqs14WE6YWTXO9+zuPzg+z
b9AGKtq6yCdJH+zuzQHKzUodUDqg4vSCDVO3R9vXYCv37XRyeNB4CbzSfBr04cq4
0wcXt0zsxXS0R1TN5MtH6BPkD5GBEO7iSHjvvABxSq5PQ4O08KGoB1pCDDuIU12v
fQkgnxT7qjn4nTehbb6UP29cclZ/KZHbOyIrYUg2jY7h6+LC8UCm3/4wV6vbPp0P
o56cr9L1fMXDIKBZy0wwFIlj9QbMeR8iAZeN70xULU19e1wDkf8zFJkvvpZ+a8oa
c4pQv+SRjLLuCESJN2L95Jt9/lD/icexRxd0rvJXFl55htpvJnxkv0OMuDd+z2ev
v09zYLDKgxGeQjKINfeHPtJFBXu9nYwnvNkDhK+T0Io4I/nfWj4O1tlXLtaSzrTs
T5xOZSFfaOyo3Yb4hSQDJ8jFooOLmYzxC80+EfF4aheGFSf1vrdex/rdmHTYFROb
CvZmV7K641pm6s3yEpBxP4NiDoxh+1m7Dvp6u996WtW70SGa6x9+mEPx8Q9LRkJS
bs9YT7zdfHU56AebSq6gMk9DSe4pBBTL/T0N3CfF52cnmrz9eoroNs3S7deGwZ1A
lixjcMdEE3eW7wtLYtKksOmkaLyUXRi5MNLcxMDkh1usF6SAYRCiEff9/dwhBuGl
RK9Kgl8iifKHViBrtl2ecNbZItgPH2ryJ6SoNRi+Pn4UMd5M3UA7g1u/BJEpULHT
0PNWCIHMNezOvXEC71HaFNjTcKjMKhr7MQH/04PW+wYWCzjiV9opBqTivSB4svrZ
Rgk51Xse/ozwFYjk3UALYn8igxcFs0uu3CvTSNt/7czw4ik/kGxJQWm8f+0yqdQZ
IRbiBAAPlqXtjRPFeEjrxJ9RspymWODZFghdckAEn1nqaOdZLo6jNZFDq/H1sNjk
lD88tavUtfts1IOzWngLk7B2d8sWzzrcgAgbJVAnHW1ngYxpbbk8HPOsaJFt6lUV
rHkyN04BtHmrUWaEELKN677XAHB/vIyfHI5Fhp+c5iLx2X1WkP9mTyqfNl3+GcAY
pPHE8n9iA8LmjRZg5+fKNCV/DfzI0CafFlmkQDUNDqkFr8gxut7p9QH8c5j1d1Xk
jghiN6VasuZb2y90hJSpFKUqV3ogX6OhayeFs2mmRQmIE2yizgsMA6ekd69LbcfI
ivJMFItnNsSFfiHUZERkEeHOi6VNKd7eFTiWFGAGt7Ul2LvHnRPwFXRrgHLj434T
/Aibg0TbgyIiUy/CU0bBgNJ6F6aOblo7cJSOVfEM4fuOEzpqMciXFFUEM/NhB8wC
4FBU4j6FuqF53pZs5JhlFh3u3ln4dgcHPspMOeOQrxfla1DyiBcnqLmcZ/Cn28vt
wPXTQlPL77iZ9W2MRIrNHnlHDEunD4dNcRCujfxKkYmC2JHRO7V+Vyw23fVXC8Or
hNk1Ms5OYK3tDywDrLb5mormHQhtKan306OXol+wuBiD33bT87D5elIF87v6laRW
np/JoIVTz0MtUgfzhhJyHTl2IdUM+4FvPdoQ8U04ZsByNmLV2ZIrTTAvkXZZ8loz
4WvIdTyJG7Lh5RF9CMB0hqQuvY5L4GAgakT5v13EpZ/7cGc6Kp/Ox2Zll/mXQp0W
p8I9iv2yciAhYw7gy+NxiG2ByZiWTYWt2OAtcNx6a+rLjfXuaWkqxa5neK+l4VH5
7zANjGPyPQiD0AN7TVrAKFh7JhOGd6IrKurkBJ4J25zt734LaHa3myObCwcu0rNk
52B2bVp6YM3vJkipxOm1NHghZ4dO1sxTXQv1B6rSBdyM0VRiXLj7/cgGRV66xJoR
HoKOOsV59tr5ZlWFm2XMftaQQ1GVqVOkepvAMC8hpSo4GkhBFJzXZ6a6sP2repPB
2vQf4oOHgEWlxw12hmdaSf8caNXm4cBeHcFLjrhm8PiSjOAORG8ZoxGG+yQ/rqg2
EQFKx8/rKg8S7pp1NwlwKNdWZE95rjU6zQmoxkVHbftsvu7D7LN5+zAWArqb7xj9
rf6EKBzVWlFNLggERliM8Xd3DVh8nQpdHs4bPE5n+UjKtPqqtEExXUuUFzdAsDRI
OzVpGZi/kQBgoOU4Hu+Gai/iztxkCyAMioXe5gZ+mlwQMJTzWbsr6XM6OWIUVLQT
Hw86zGxA2vF3h0eZdFEm+oeovEIez+j5IQt5JxOCFPq1pPij4pdKBfIZ4eneYzEu
zKU5q5fSN1149UlF3ufhgvVDdll4MVDIu7eLKd1yBvZQVbQ8ZjNC2BzyQQW+jv8e
fvVZxoNydZDvMQ2TonDQZ1aYjYCzmo0g6XmCGrUdMe0KeEUB+p5XwyQZt1UrYbOZ
dGp0s5rA7c+8cgn3qakHRL452/94s6aOZPmNg2NcQ5R8vud5O+KuESYUa3dh7dhB
VkBS7Ef9ALYpQ2nSuzt+Oz4tiV1nZ/bPdp3tCMUG6ccu7pWdJbL1mynfqhANKrG/
9bIdvbr8AKe+ytSgjmDE3wtPNi94r8A8mg+81Y33+liQM2SarPWMjRyoHVgi7WyG
5mKjLVv0m0sM6qa4g/StTNAKvlYpVOqCoEwj5LtKDRTTVF9ws3KEmNWM/feU6MnN
MZhpo0dWW6qOfIunu96kRUbYZ+bBXg7FsMjuP4s/X6CTaFaWu46nqA0oKRhaTceo
3a9Mym5Q4zlNTxSn3XmqpRjAnIz5A/le0hvqyXao3wqWJAgzCHeiJZitjIXJArXr
x+tr0djv3Cp5VIlujnyteUo7MKTK0DlrM+3inMSFH7Qz0cROk9R/4p5AgtZyjWec
B3avqXh0l7TqeioTU2fizEl2yK9Hd/ZhE+mgeG6+9ZyId1vKE0yJ2MWkdKSGhRTM
ISIlyEs3hXs1bcbyvdL8+1WmurZKjNT+dTvJ8zbUVQl5BxTExaAJiuDH+KvzeEUi
h0c5lpFgemj9XtXOQ1/3mQoSHUENqILAhUxw5x5GAmTSxx61Ws/1AaABs7tGOM7M
rNjueC59AA7dYfk+cuyt2rbpDZ1sLSqWjbe75qBZmvsJ/zVYQNFuJfegvVGpmknq
R1Law70VKJqx5M9VTVD2nWSgaC5tE3A6YVIBWtQ+jVdJ6Cr0ZqZOSP8lPPmR6UQj
y67TWi9J8goOrnApvlp/vmG4u6ZqVccHEymVJ8guToQlT7jrTprXHgtMhXqU0iJ5
81eva+UjBj2UKSiWATCNzclhVylCbo8EvuGL5yaEEaHhZp99kDhhzXlhwr6uMAT2
N0PpCdxtf/kKHZBTJy622iS/Tr0ZQrlynE95FMZXrlmtjDghSOxMBujjlTPnCld/
+o5WxzzeyshMgyoxOkX1oMnktMam/BO7pqbTxsNZdq5onQWw0XyS+dnt0SlGmwY+
VUEgNLtaWZ2Tn9bMjynDbxKW2gGK/ZkgAl7AA2pk/mTYC48LthN1cf//4j3jB8Vy
Jr9QsMs4qVdR5BCtllQMbcdv4dKt2SXBiZ8xZGpAXECqjZDCCV19Eu0ySVHlaaRk
AUXbJxa0FapUiLcj4OZUgiUV8Bi38xOAs6FELB4nNXQ8BKOXLvD3LA2SNu6MEa8H
0052cu+CQEZgprV/hRXdBsSHHDv3mueeZzlpcYrZ8rLvyC8qeBV6OX+TBK2mk5i5
/lqfAm9dtZAccl7CLvZz+630S/v82eN47QdifATcGdEsAOEt65lH77HzLv641jJC
H+yV+BJePWQzfln8eHdRHDNaPpBAWM09PjKrnM6HFGvthG/jfXHgTxkObJWFPIgJ
fsF8AumO8/qrHwaOvqznnAJUHQO/iw3FdAHYzJQacLxYny0ol5ll1pKgvxPyhjLl
XabhdU2NXuj2RLwvqDQGEKQ0DxUgnCazgJMjKwgzrkRssjxxl+PcfbsFOBYv3nF7
rWmo9jbtyht+/62hexNX5RpG6g/mnXXxg3EqEtm67ofNKTD2Q2iDfKTle7bh2rD7
0KwImq/9p29E0tD64r59b4zWeNnEvT1lRRvE92j+oNKCm6Ix9Nd4Xz+8IWI5Lns3
hDkK5QyD7lgYM1B+7udk5wk6g6hxS6BIVJtZIE+CrXA7zUjUBUAy1zsMhQ0GIegI
UiHEJxo5O6KryYun6UNQXWG3qFq/m9n/S3p8jPERaiCt3LINsRcpM5/HSOXwSetf
fhlRIIwl7EdMSdLIyqPSNcaGMhFthjFbTUJWO2iLXxoh/fKOnQaEhvYpepxtw3EJ
3D5Cl/RWWJT80icnsSC5LYEFP4E39XSNRNe6WYib6iX7BmZeCRU+Mt+berK0n4DP
pp4nMMftZ07DM32ruPS1b2QRqfkeo1f1KaFYfXSu5BdeykJUb5uQLimLXPEOjFwU
hBdsHcUchA/OHK+oxhYgk9F5Q52U68DjLjmt0LaX6xsZuFiH4U+fp7/xtBMYh9QL
LFTnxb1qDnmxDz451hvL36eeolhL0bOA9BWDLz5ohYLT21+GbPidXCs1QAkp8vqH
Lcrew9MIFFDR2K65ApM5skj8s2twDg7Y7oO2Pcyg956vcpfikABvtu/Zwtz/GNn2
wxHw4mO5evDvbMo/SG3efhPzktn4nTGpQTvyeuNPEERDaIrxGWkxj5ahwXs4tCz0
CFRI08Vih5msElpXJTcfXQVCLqM4IYHQzYn2KL1ak1QlYqYx15yulTtroLFzyA7k
ItCTLjaeEWObZ6q+lHoNmjyLRi6OVV2Xyhxm85xi1WCBSF5r+XK+WEHauSTlA7XK
Xlj+pCibkxe0xmbScAj56YCZmP+c/fNE4M6z+n+AWdFbM2B9aLFRT5MLczgSMDBw
asYUgdnyw++eFT1QEFDKcRwyPuwMx00rMD6/4xyebrqIh3qf9Dfu6flMAmdQXTSo
0SJA6/o7ubg9wbXt31ss0rYDnjocoWErGcokKentLD4FAh8Bqm5BH2sYhRlCKTSk
llOekBL4jPrAUTSbyiDAr3lrtxmbPQtIBJUwtfe6ey/UAmium6EgldLBUqUU1CST
LNnMR538G67cH4HTx5FGlhLM3rSv4r1XJjoIMOKTrrk8JsvPBcRqtSv4/U7zrezU
q2LSLOb/LMqcF4ROwv1NKkU4qTpF/bGRXI2zRDn576dF193mzcWJQJaEvbL7MtNa
qvtvFQNUXUeZhuJzK5sIwGI7zx8cKvFDOEZL46lqZkiXUMegSAPIJ2i+RhcD+RUE
hMq2uqDn23wRlet8jw8IPaGT0jFiEbjCmqOGMe997zFE2Urs8fm740gEd6dnSER6
OxlxUWmO0uYO67Whu47kLajiIrG3NEFkuHVeo2yyiCPYOxWA3sxchNM0MDYw+Vuz
xTUXeNpfokGbSf+YFAijjyYBUJSdiY8MlHvruQQDhaTuXDPSUrGYBCf9M6msshOV
Zjylb1psuDmuTXlvWpKBc2SXwSY1NlIJn5M8gLdzpCGEle84efmnq5pqoKOJclW+
IjpHmLhiWU37iXaAaQifE/SgNLjd8C03/QpDm5TAEuYy+1sKDYNDvTxKVyF22D4X
JhfF8ZZ9oLyNglGnNiZrRqIV8kLxILBKpsWu+UjGiAJbvZg1JeytbbGWNiW3btE6
A/slZG3qHUnzOtX+bGfQ7WCGzV78cPb6DwJYsQX3zPmYc/xLapVEG6FfkFWFRiFK
jjibjmHvmk7PeDZ8cO84gigWtnZNGL3LsW1kCbaClyEJWvojKFJ8Kg2wC66/rwd0
2ah2QLVYDth5ZjK5SFCa9//Kcgi5SgCYmwjFci7fgOOJAX32juEDeDdX/cGLqfZP
zbSwlLvWmhw+enozb1w6l5wO9QFbUyjBVldqMiLqAcTkaJENGsSqq0FioeIBIFbK
THlxKoiiDteiUqgk3+5uKtATAHX9H/FD+aHeiDmGwdiyO7FcLGRasm5ro+rzvZBj
NlpO+xLWQTchcvUkMVqVooo31d8gKLUKvfqcJtN09JMvBAWTbSZ5W1l08Pe/pLeF
xXpUQtICOppusB99lxHkTbywS+9L5ocTGgGrpdUWxWJ0zWBafxeftX97fQ3oVyJZ
f5j6VsFMQ2o/iWj5s3Hiek9T9eofIfaycqS66VNNdSiKxwVEJA9jdNVxAbTUKKwI
L1lZlwBBusgpnZw3RKPhDs15iaP0BXyH+SC/rDYrFfuhXhHMqzV+ZcZT0Pp/8bBO
0RCtZelTHrwR5qOzA2V+0sDapqUJ7mXL9ZedHJnO+Li5BoDzXxhTKKC537X0jEmA
/TSC++zW79QoJ1ksrlXXminIr6BFBkqwMr1MnTRyA6/XPraTtbqO6VwfJjbTrgPH
X6tjhn5aXAn8RtsplEx3GymMlinL9vgkHuI59vp9nLhNGeFo3aFg01uOGjdqTxaO
V1f/Znbr/5EMMnVYlSRhYNSX3l7MXJPnmn30upMJbzfOX/QfzIUVms5oSIJi3+0G
oZb3A8WDbR991WHPgq0swC5V4GBOcljKJdsYeV/M1HbgG9StJWsvzOJooMy+5xwW
4oKvaE7Y+3wTzD4XNAchAWvYaSgGqjaSH/d6Ya/ww+Sn3sk7FEVv2hOO56xQYtXW
aHu9uMzHhYGZm/lnGX+ZJZUkEB6YwCuI5IAKiJN4ebSDWDdOhCT6zKdWfiNXvDte
oNtmP2pDEMAFWY/4+C/mqrbRsWWqxcabSYV4d0ye1AcSsEK1lt3Cp4n6AJYfoWAN
AfTTvxU44XOfIsRvKOCFeqVG1ktIXjopwtWGRzSVDWD9+DzLEcCRFzvn9fkvg0OD
GvGSQrxF9i7hrV2jvM3TYlc38bns8/L5dnEa619dpdix+C5US7isf/lZ28qM1kbT
IbMa1FW68Nj+5G3qMYlTYCHon3YHMiO9g4tN/pYOdbJWybwc0NIzUlgoi/oQGLBG
cJ7gMwgPzE2fW6I6+MDL3tnYKgghuTakXyl6imssnArCsYRl8n1zyr7fYw3xe2U1
wnRnPI7OQFGN0oRu+o/lyhce6tlIXDOyR5bvSxBmYrFjVijHKmoYBu0N2zTbXlij
2KebwrTW1CZVFhEQyjXkLNY1r9upGXBXnjrjIRtmRchVsoVZfjUztC2fa/xct3vw
6mityvtEiEnTGyOFolh0aqKj0laEaw2IRJQvd3p+crNqPH/nKHV2NtI92CRb5rpz
Eqw/CkosjnSiAjgXqwyLYz9phm1u0k0WKRKCpCJClctk7LM4hmQleTQqOantES6C
uxYxZkoo69urgxs8U03b1KHXPrv/jEb6/Gt1/Xex0UQv7D4vd/9yukxIGdCaPyf9
B88YMcvOJhNSHZytf5dfIR3La1GoO7oF1eJRXTyOCc5umh3TktqJuNVPH0o1ox/V
UuNPgUCUPL6qHhPCt9b5SN3GN1rC2/Esa9KgFGEOX2aaGc5vNLs/mheo+oC+Q8iN
rr1BJwiNSBfoenqkdugqI6wA2Xmdohd5u+fjk5ujZrMqhd7CJUSIjHnlwZr9kTLb
jnF6dKJ9faExk6T/CaBOe93K77MhRuQnMuNXLnaVxwc9u6VPQ9M2qHXC+Q9zYBom
nLek5qDZo80mN5ru95d5xeXHvoP1zaSPTxGe7jPXopMWDANaKzlEdp9Kx3f8QXZ7
UcQQiQuF/JLnoL7cJJlyy0D0F+ow+uer/6hbfOQKRW9tEhkGDHRJl0fSw5jPpZA4
3z9vYlOeCHluN/dnVaUc4MRvO/kxn3rClj3YqZMLS1lrc2fo+F7p6XcLfmgfpczr
ktm7E7Qk30NQFlyxg5Nkq4pY24Vfc4gOkSQ97OWIbKJWNoTqyPMKw94lmo13AuGb
tjhaIhESiTv4lWgdAqUzNNb0TMhHmF/cw7K74vEmxaOVl+L0Q196VU/14OCepz7M
jZmVjQO7FAGjmG4E07rYcGwhDEvJMPYNXFWlWyaRds5lPL8H5tSXFvnE/6k3nvWZ
OGhUKauG7VAY5vPZ1FYVCkDo9+40ym00+NpahvUX+Sy65sZ4SxGT3mJq+sUbmeoN
Yzx9MKPvF5MWkgOU4YPMHX2/r5tPlF8eKuX+QRkATKjwHmsBvKlsYp9lgWoMndZA
b3U+FogOb7f8lwG5TOliNFSR498kM90hNlUs4sdvkELtAe1pseo8wjegQAyjVozy
AeYInBhvKRi1X0kTJJQW9VbXxb2NR9tqdmcddolmmeJrYClAuPeU1kXSEKph+xhg
2T8GqIPC25Q2n3QgJRtJesK8TnXNKjLBW9iFkRVoskx5SUzCSYju9OLgB+dIS+Va
908NW00uw0n5aD7gKwqtdlJExI8HRgx59dHCIbDn6Gi/eNQPSIooRyy71JaocsE3
K500Pg+jJzB9If0Qagd/arAGTa60KQtJK2yMxMjFgKcXbU+a7VLGczdRRJrrpNN0
C++XatZ3Qqdwv6FTiVyIqEy3ZBEVh243rHBifp5G0PMXIDTO3YSMlf5tkmf13cfg
aT5CxGPC86NL4THNjRmDmhz9ruXbu9an6alU15IK0r0R0fPUAE50Vdo2afO/ySx4
KhRsm6vVIGy8iRGrF4pkVraNpR+RSeOtPPG27HrWjZWFaybNHol3WQJDrLVeKxXw
QYSPJ2ppKOLVZ34CwZAHNZI9Ruq1B6ItbtlAoW/NzyDBDQF6raUtllX5dV3Pvh8G
RQ2/lSf5ZojQ61XCnSxg7SN8zM13LX4tzjMXt/k81myVXq9TVgwliKohOVInXwEH
gdRKeNkLte/9JHVqOwEw3Y2o7CMwyImUfiAAQVTg46I03W5UuNi8kq3eBEtO4a0M
DzVXPssrFQOLHOtqtncIN8K7f2d0jMtQ3S6Df0UNdPa8qQm3+Sow6HAkLgM73Kmw
jBc0II93AjE5tlV9WqdDZ/7Hpydp+jFO+huPMenhnoAG/RL4NyH8j8q3OfQ9xYzq
3qv4AqtAiL9qjoG+kNXuWO0UReuO9y65uGJ41D/4QWe2lx7SlMlBF+DjrkrOkcuu
hMdvbi/Rrtx8aWpoSK+uSChY7BN4enbcVvcQPQjAhDOmjEvB1eRCj/8r1cwkq5dh
+dt85FVgUlFUBszmabRho4TRltkAaBtSvd0/kwAj60wjgZUkt/5QlCb91DS6fuMv
K57QtVlJdh0txjMCWpbkNVUAPgh4jgGi650teaZ9K79iqi/S3F9LNbyktN0H/YUf
u7UUQWw//7CZA1XK8z+3k/xvq4qiWx+/FrVACKZJUCtzEIx9/5tjrYxgb+gtnIz9
Cznlb07qJa9sBRescgcqc8sQaGIM+CHbc19ZKpPV8eg4IRvV8ZCGp5RdklwKc2mZ
HX2ACeIcrT2ohyDMWqbdHZVIOvoXJR8n3gjk+TddANPwEO08TyWR++O6CpmTOklR
2i0NfLbPPj38NVOqaAkh2LU1sd7uXwOfjHPL0HbMV8iTsGwhW/yhhGRRXN02eH7C
ZthbWXOR422/P6bkM0xDM2ZOdK4Ks3eUJlAo/xh6JYo8Dvc2mEfTZNwhIcpKhZj2
G991JzAMVwtsLeFdkJsABFX/Ol53yfjWSIYXKYNxhftuKHry4xhNYvAS3VCkunpP
phqSh2De7pBvDICjQCtjPz2tdLDT1r1tRN6z+CGx3XShcGGKKkpgi9y+WpAt8cMO
/RK12U9Rm4Rm0HNwCbuU8Ud73L9iFNLqiYjDjodiKtp1K/kp5pWJbG8IcBnPR+Gt
zNYr78ESwJ1oLCEDHpzcPQTlJykDh+A0H/n0bsLCzIrhUVdrDy/uiFmMMUtovOyW
06jClqQoLumKUGmN2rRb79BxioVSVRiMQiu87S1AFnOjnK9FRJkFa85Xd5YV7eAZ
CJ1GZuSPBCcnW0o+DzQruQRIYECW2UxlLsyE3yuVaw4mutq8kV3pfkWgImenDFqf
hBPbItxXQZTrjzT4WzisfDp0tbbfliTrDe4FYnYaZMw=
`pragma protect end_protected
