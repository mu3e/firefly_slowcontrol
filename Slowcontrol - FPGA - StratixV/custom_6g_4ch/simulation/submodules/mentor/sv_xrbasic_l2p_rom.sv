// Copyright (C) 1991-2016 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, the Altera Quartus Prime License Agreement, the Altera
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 16.0
// ALTERA_TIMESTAMP:Thu Apr 28 07:05:36 PDT 2016
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
K2nSvZGao9uSBdtd8Ctr7I3WdyEB0+5/GmO+jZJXjZ8tv3ML3eLLEKW/gLHHGDmO
IKvmOMU8E9dssOfvMT/0kZpGqes0WeKjylYJMTuhQIhCy2DkPRGkdRB4MoCEVa6O
MSzvPZZqBnk9vgmtaeWEHiFooHUtucFQ4R8UPA42Nrs=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 7104)
w7X9m4YRU1UShWomx0zilYGFOqtnizZLfolX/NZvE0SOPRIuf8gY9tLRpu022cLH
bDDhlvY7sWImQjPdKSFmNyd0nio+OCHqOeCaAqsLFrWP06zFmAYOKagp2sG/SNI8
FsEkprfOlEm/Pe1L1/bIgTSWx/OHGbvIirCPzoEo6+AnNIVxqHEq/y6y5ANBWDw/
tTw9r2VcL1hrGnAcBNNeCelSJPWyH4PsceR1PplfvLKJAnApNOSt9uY2S5gPkrWi
XR+QJP1yaWoTKO7e90ZECqDK8L57dIubHSy8LAwTs1vPOH31Mu7KZERVxfknpZVS
Zq9/7Mq3sesZo9Expm8OMh7pKfwI5U/J7GCJX7Bltd8eMGcEN05yGZsr8Xcq46wr
Q+lOx2NdEBmNcwIGLoV7PDiXRDaiK4hvrNcBKq+LcNMA07Z32DCFemOKghX2Pg5O
JDey22g472wERhFxHfMdYsoN7ebWiYDsvkLQPF0hIrjlFK6bUXKauugAFt/dUwfW
E5UEzcYcr7F0WJOwjlcuafgF/pN8IuCRe9n2JQBrPb+R54OdoN7a2LBMbdtxT/OB
PMn+T8SrjePWtnPb5msJFXTQ0RvexKurUVvBiAJHeHhl//GBIDKi1ENclLiHkjsI
Fuc+iVb3aGtnMKQ88xcWH+Zfdq6aok6bJWRLkAS/Rs2rT2MBCmaq1/ZtAA5ZAbT7
MUKdM3rQGor0EaOrAXlMBPVhFzEkUmPmK3dtZryoClonrbMCJr7s2d8pQoDXwUjy
U8IzotV0WF4zOGFSpOdhfFwfOLO6xtu+/y4gy3PBx6BGwUnhLQZIIhzjGLTlHiNA
vc7pinlN6Czx/6iIbe+8A/DyvxoThNJhUyhNoIsVh9CXwFYsJi/VYpK1y1UUOVnq
kq3syyIqvLa/2a6kh7oAfOI2+y6c7gKRBZM2WbEwMJFis5Yk7p4TR5u+Rz/qmPpe
Vaev4Ms/eaSTeyRcvTjLmhi9DI59bSWB0JgfT/g1sfpeR/C8FdtzQmwiSB4G5G+a
s4JBijZlhwb5N7z2REcJOUq0KocavLa7SDT8z9SCRjTcoGzsF2ra10BA1n2kWwpt
ft00+69XvTK3xZkBfEyHQP6lQTsRGmhWTvoGMKQdD1ZSr1MxTZzr+/0iSWXoM4Wn
NBECoXXql1pujExOntQk8EZmQ1LXXHYHFi5SbsbfEVx0pK9oj5x/tQbqRPZKOLyV
LAw5Sazo0my7TVwgPf+9boTsNvrDSbr/UmievS9NfUdVJezuLhEm+AzE0I8c3tj4
HYvHpWH5/22snHxHlUukbNXbTiQJDhkVafeoXtjKYOzbvsfKE9IHDHdrqa5i7myg
Y9iXrMTEcIh6QXlfcl50maAFYQXtenXB3ytGUR7ZXbxi5y7CDDMkibBVSe7bUlxw
9dVAgZZ0OPS1WeMZYNC9LExJQBQutn4Z/iPdB0hYrhnSqAqsUaLhsyIrFrtFl5fJ
6Of8bu0QtVquGkUIxCDJ6Djf3Y5N+Sb4Oj0BZoxWyHwIYZ8CegZGyj1XOzULkJy1
7uQG1i3m5IO950ubbz4Nc2VAbSXmIqiduoV7CLAZKXisIYWVYE9gfRw6IQdBcw8q
IXdUHUopPodDs1J1U6RyVY2RoDKtU0sCQWJ1SMhogg+xFZeQuvpAP1Y9vXZLDBGp
g81SRA3ESMJRR38kROKKwiCrs2dCHm+IIjiKCFQI/8LWFIVgl3O1y7QlSfE/+gon
u5MHJlI97J0aqGLprTAvCMFvTSd01Iq0OU/tyAZGQsNufRf5WTJv8AVhPRTr+rWT
mC8IqMQEb/MZtzPxYQMVJbofEckX4sD8Z4XFCebmckjWnMaRmK7hyRvROOKL2kGA
Ct6CtzM6Z3GHU9AeLMrfIMivjlLMbWa94/TrbODnsr45DFmfO99ZHczvxYzX4Djk
Jv5oM4D9MBaPUlOllXJ65PqpJF0b2ai4ISCkh8bBwMHIeG5kToYtru6fL7UKBsuT
Nbzjd/ptKIde9O00kMyDCjtJZQnZEw6PP/0CATjSGtgn8ypO3nnW/pi1wDe2OC1R
TbdbM1/i9ZI/UwKD8GCZ6O0OwAgtFNZGFyfrJ4UX7CovvdaIf9d5K0pjJR8DduBG
BRw/QSweDM4wArLX6MZ4NRJbG5mwapCQXhEuNzRxaHa/AsLNzJdvGUOOIMnQc4dS
4Fqqp98Zo5aaOQLIsK9ZoFHsPP8ZCESBjm8ZFB+b+kYjkD+uNYB05rQpfpW0mMMs
PKmkuTAP++hxnh/05xuwNU1mFn7QZE8Hhfm6fvGeQtbuznpx9FbSTWkXQvVY1MD0
daRZdBCu6Ee6mqYdlTr8D0rb2Pz5b13FdJZfmeI5zt74hOtXev8ajVz4G7dpX6hE
LwZFWfrNOFV9ixsSTZZ9nQXif2tuqkq4sslyA2CGyJkMkKxDz1Y9iaFv5dRgKdiZ
+QRWwBe56OIf6ygjxC1m2W1dNEUOTUkq6Dbg6JxFZEkKNdo1wIMhfm6ewimKj5UF
YWSOjwv7rflyxp7qZMPbMcnraWNw/uy7psObIIIhpLBR0jvT+N7BgkabgsjbPNjZ
Nbl9T8psqfS6LyZteBf0rw0izfQZa4o/3TE/DzbDAptMMqNb4CkPnr8JI3r89/f1
lbk1s1wVySXfIzMl34SI6tc4qIAC0IYA9ogwUb8BSX6XnFxdf/iEZEjAFI070BQC
0N+MtCVOJM5DAsNfSf8SE3csfu6oQ/cL3c6QkkyRejeAWwL884VYK0pndXt6er/N
YXb22lgwREFtmPIv3i3TgoxdyXqiBUPp8l7iLcIOeFRkOVFmgNDf3s02PFhuk0eR
/Vgeppg5LDEcrfQ9MEoQEUfkFb/xubHj4ob0fcOjnZIhfu7JbjUgttqCiWHVClsR
6rvxdphRExKLiHMu4d/iwAeK+jOGLiU10tVixdLQoM1++39+tQw6GyxG3flLAJqC
X+9K+09UBduvf3uEy+sgFZTedfuA0zhQCbonVVEZXZPKhk6zOHy/RBYPvd9h86MX
OJ26bVp+TaQbOsX7YUhNeGJAMOSf87aouFcrK8CXaEIxJ1WJWKwsngenUWavrMTV
af8volnxxDCBkWbaQpWTHzLIw4WQgtRT+EKOilUcVNtg2V875gDWIDjQby3VF57M
q7u5VDdcMkhfqPH+Bhd8k7+OHbE/Hzq+1AYM6EFESkfDnpvHuEOtY/TlFOE968GM
eG9j0PMupzzv+US+igXp1tfSnHoUjSzOntQ6tePh5IlVLFbHB97nRtCahHZrbj+u
fvB3gcuus1NJVkQswOkzKmlTUPGFilB7Jo3z0IHSLl2EnHtqJZn6i0yqu55ND82k
fA+ZBltWzq5KGWOW/nTmh6RNEY+FvCKw5GVPV3z4xkcmlaeEkRbdWZykamm6bEZT
H9n+BZR+OYsV4fxj5n1rNv6neEWXxUF1plEzJd+Gil/8IHxMsPjY/aiVZcuhOdTr
yMJWPvix4XdVmDScRNvgCU4mFjtE3sg8KjvANlLgsuwMpByhrWjgIi0aNWoVdjwY
t5fIpe60inZfpPhod1Q+nYKZhFD8toMNmWLT19v3yAkfG1703g+kBXhKt3brEFIR
PXlHQO610mphkHsdYwmfrXe/xX4fN71VfdYqmdJbgH24DHusmuQQZIgWmwBt9Gd+
ITOfCbgVboOGMMHRjPquYNMJ/zuXDu1rD7ly4EJw6+kvkX96sSTmPzItUDkv0h1X
KAbysYAHe2AJjpwExJl3d34A22skyQY3ybEXCYG+zztm8TxpYBFtYQyFFtxCaJ0V
s1DHEYSqyjv9+A3zxasf2/z7upW65gzK0WbEk9Tlbq8Ln/KZS5i56sG63Y+DJLaO
b09jrl13ij/oqQzRX2MS0exffDc9G5Ah58EDj6aBdFahjonI6E5FrhOp2OGWyK8V
9/SLH4UwYtBB14VnhaZOR88r23t7Gh7u+kEVM/KgzsegKzVilJyPVY2eDjjv4NPP
f5KAZNyS+nZn239Fc52P/cyzW+fJDgBmS5iydb/ZEldupFH+/4S0nmoXu/eN2moY
UNKvRdi35qELMus9LlQnqkIBsRdVTf1GXroyLqXdlB3m0Hawdf1d70guPSPsDxUb
CKQkzxgpizGvltSCVMN4iBzJAdORmO2OJr9+BYCpY55Kx7zYLGgFQ0SeKUnIdFR8
9qtgO+/InJtq0vJusJdXHy35WTaqcEQm1lCw/7R2oB7Lvo6gd+yCf/vcuDMmBN64
BKBZVmTaFPNtbHT0+LkW4CSr0SEmzE9nymH98NhNluEOd9bReWwUMNb+nSQKBCi2
gADyszPXB6ocawaJUl5TKlF/Ktb719grNhBTrj/kPUqtNuViyps5NxTaoTjOfAz7
XkuHR8fcdU/C0OkLRuAa/FQTXuoodBAPyAYFK8azeyctKC777AfPMuIfQ5Euw/vC
6GnYrTRhZ5jw+JprxckaPpSyrGSM2liNF0AqkEsbYxkiTpxYO4c4X2Qqfx3gAMeo
k60geyjlaodUlSTkEw/DVwchfwIweXDvSmMxU5ZxTStALUZAPMT5YRiJCrtDchdC
9fxLY0ED/bpygoIRdMw9KeD+wDeZ9gVfYDMWoHqUpCPJRzUfA2MVRTwufN+IeWJD
IVTfaos+C/SyUYFq/OqxeQeFt7nKiE9z0OL3avBqeNQbfpGfr8gy1wELwElvXm8U
eT919JWd9FoCT7Qa7cN+4eEape37MLj8OfxSX2VwVesprbkKpAlUf0VqUtAznNIV
N0HBRK2XsMEkGBbd15gRkENwmDp/J1ogXXdMW2Y5VvG9oj6REE/aoksOhWil9VIi
g6nFNOH34Z+ugnIF7E/eC4x7/u3K1aI6BGNBkSI9QfP+ylpSaRUElSV/c61Z93oz
w0fIjehBOLnOk2qyffiU009xzXKT8B/UINkBSqMd77IAgOo3lKESviT1OyEtFymm
dW7rYG0Yiufd1hzA8q3sc+XWXdINfy/YZdWZF5W4wys50XYTi4xoTKqPTbB6XUQ9
Umhptfi+dgy1nsz+8kdJt/BQAPxECQEbnMDZsw2z6FNl/7sGs++HEd8V7P56EhhQ
mCpBUUw6rsmwMdLsMrDzFkE0du4gvepC/TpIiC/83XlnhxUzIRzX3wZPbwoJGd3b
1A8xOF7xmordW3sWa/bTEDMYEZa2y7NvPvdrq9cDwiliw6nuRsw1br0BmVksFfES
lrlqZo/RwHTczUwYFPGqXC+YBcPVKFBVFBcTArFhkuLuEqJD7I5coYdI4kPumijj
SA7fqzGR0a7W74IteluazTcPGcWbmZxtdTKy+0yEe7jwxuDQF2YYjnfnqiOg42HR
Hybt08pFVRM9rSf62ZfQri+I9mR8EFY/Bckn6sAVbkCZ0n7kHxztwfpNbB7LeSdb
CJe73pjr/yQjYxHd8shdEaHVFBXyjQOFA5SN5cN10hBQYKcu8SY7ySzQgFaq2Yl5
xStuFgMSzvy338C3576TRt6Kcfa95XDbCWg7iI7IGZG+lIIUK8AIS1Ip5w4miyiD
m9ZuBn9BQiaHhFi+Dt9BgOiiOhbmTEo5AExEl4qy504m7yFwWEn7jr7lTaSYIYXq
zyOLDIXpBePbP/97yFkqBRkSMsnLSky300Ot1VLm2jZgz6MSFh2BB3Wggz+NlH1l
wASQuPzZNFYwON78WLx4F9BbECxuGTfuSbAKwXkDHNRULHfMfdDZ9tEEm2HG8Uso
+Zj9y2/YC6CK/u+/1Z29CPo+WqeSYFafmGsSweyQ7F53psKT6sK6qKV41ZYnkYak
GKUk6A6XeLCtD/DyZmMFxd+oAvh6sJvciRvI8hvCtsKOFyFxKDrv+0N0PNsWSIRB
nmniYkfsWPhw5WGTC++SlbOX3g2d5OeULHzPd0nVXSg9FfQQfW24BL0wLc39Thle
zvB5f+/9NpCUFRzrKqMTQIxWfhjLbFKXFva1248VIqgvJikB322YQt7JkXzw+JG4
e46tYcGvtqz42KUNiddApaz8h/d3hlRGxoyVsF2zyOVVZW2UmztmGvIhfBw8U+cR
HcB7Ocp03nV4J8GTmZhV5XxvmVSjoYkAR0PVQ5JS0xHsc827c+YPyFavPyhQSWx5
/vNJXFjAuwEm03re2cJ9BLebZ+61AOCUAIQp80yoXPPJ/cjPyo2XO+f7bzv3TDEo
Mp33PP+KePhtQPBJl+MeQ2ehb458MmYFbca8yxEFD9KgwnqmvGmZsAMgqfDozErT
4mNbAwZjwfsZMwmxvPsu9ZYaLPcKPRN/JNHnv4NOrdKGVdrtg/E0aJhQMtT6suBN
WOG9nk4Lyo+ZdGAOPaAHq/QgbVih65J3KJB6W4y7SH+If9IVkAtan2VwG5e+qCzc
6OkT0tqakIr/+jz3zMVuYvXC7Wce9RMA2mBxJPPmyQ56tkjG9N5AtIf49UJnpQJf
VcFCnuZSSbmeckmAVq74VA1L+6KMJZ9tQDFjcchrJhIi7e8/1nhyc/pADOSRBUJM
QXqa4rfii54jwh+yf+ccI7RvehtKSoyNpo9tOd4Gm2GrZBCoNGF9wmegSb3tqAQk
yL4qRmy+f2iH1lCHcAkwjqYg3v1JPYhyGdpDjfhLGQCCdCkqnN/hlpUJhDUifXF+
UgdH60eY5VZN1SK10aV7tqjY9eKp3VjK9zkLCin6jwL5KpJJ7bmdxwrp673OFdcV
wuQiRCv418+s12CokauLOPhyiezsron7IUmRS2bB0vO09+M21K0h8d0HlI9sJGvu
60iMKHC8gBoaOyrk/IeCayeZ46EuRJhxDIGrhHuMPxhfIh0il7EmsU+e4xaOEnJx
JV8BGBsOv+rlaazt7197BclAM0Ve1Q1SFSuj3mEHcI7Ph9HqkUi35YZpLypgA67W
QFXztL2lyggZYyir/XKxhmpC+fVEHrOdvIZswoFkbDeyHlUtkreMjXS9wNp+7vLq
kTAeSU+9/LH3MlLKLbXbZmYm+yyNwvlOfVBSfhZcNxyEteUmfIKHb6c8vbRjDiri
O7g2uNk7HAVBbXexqpOrtYlUgJfeEaWPIwBQHzux9ybwXivOdIZW3lHC2ZZOSNRr
6j1Ci6aacoDZEfpa+I3U6m01L8EFReH1uCMuXA1jkgL3PpKWUdHpr1USRMT/qcpg
V+iLjFjlo8RYhBUpb27Og+Nhgqu7VPy0KYuq5G80Y/8QGQMHuGT4MbR0bqPUqUl8
h6bsyWS3OO/IlcrNTizJm5OwuxmSejty+y26NGTorhnogt/z/lw/d3X86A3XNYAb
9gEXQMXleyZ/pwuZHYDHSpG0S9vcYaWOGkLL7Ji9s6PbKscKDEgdu+0uN1DTolhK
JVMZcApwbBl2f+598LSY4vcjDsBQuRPzNWVVtjaP3VL3iX70EFsWT2rRgB/Lhfev
ld4ifCrQj5S9fZZgo9qBlLKEw1WtD7Fvkij3jj6cAroLwti5AX4ICwbznSjs0yJZ
FP9jB4DxzLelksOK1JfoNcT124cLVMqBXXAbSHEffEr+2uoNGkPHZFe8vOKONEDq
+CJJNFCEKbzhSSSZxOTFZ194huikeGbBsq7izIZFadoL09kJP53GvPChQvgQAi7e
kOJQG/NKfX8Xx/TOWckjxaQOPyECVhsdrDSOQ2mET6UwA8y5dMOdcoHkJkkV+zD3
7KftpyDGgxBTWF7GKJ+bJ7hbb4btC1s+0R9x3CtMy7mTn3enJ6IbS5qt1R8IGtqh
PVIokS0OoR2OfDiRNqydhl6de7DW733nV4fyAZpHFwbR9lk9OakV3+nrO3Oq9tVx
Bq+XDVtXnkMMCpRlPSaDBLE6jMvBDnNj3AN1Pm+UEGJOvfQeTq7nH4AC21/+uKde
iHyz8qqF04NFHC0sDa8ntugNmXBINb9Z+0WQUl3Q+HF4TyKL44Uw7fHftfRX61wc
ZZviPSGk44W3nKw7ju0Qt1yyNSG0ShZzSwi+LJhwWuQRcmOiaLksd9eNl5xe+OQE
kELLHFe4f4rZRH6mSsnoheZ00zcnEISO7fP6y0M+O269vG4m6zZzMf2GI5WilmJS
L7PrBSFblhysuhw7iWpdQ0qTSXvDniyNHSjKg0aMlhNU8vVNIdigEpaTKQx1ZOKf
oJC5Z6JSkyzxME7Az0kORuPOd03JeS/eprcWtlEfy1Xcdddan2Fg91oz0eYrxG9s
M74X3k5wYD/gdIKjvWqgbm66HL62cJ54YDlAu+YokgBpQjUsAsDfrfOxHq03xBpv
bZIorgoszgospF5p/ru2Z6DBwkUgFgnX6rlEsxxcGV7aEAUq3lj5HEK3E4HWzGsS
vg+kpiKeRx4umH54rYm09sUwFqXB71dPfIGIthJFhbCQwfpTilH9eXdvnK0Rwve4
xZO4bVvzmmqx6Wl5SYjZGRHEhriMc5hydVftLnH4ozfyb1FWlJ49S+ok//CsCb36
VTZ/Weg4Sbl5cLh7YmuD8Ew44RjbBAiJ9GkgKB8aNP4jZAdcGKs3kPItfjF0mn/T
drsehEokd4RTwrYEo0qdxkmIZ7RMpSuXZBATgrKkGnZeVZg6X0lee/lo+KgsnXkC
3MjiSbZXOafo9zcosOlp0miZAFBPOl/SKaE+YNpAPBbRNEU1eobUWZu7kD0GclIw
SNmyCq8u2qricDBN6xm9XtQu/CEfMbnes39wBeO3U28CChx3GIhgC7Ifz/MKu5Hs
nMqrfurzD5WH02r3mypq8z2jdAtn0Ovc4uV6AgRi0GT1mA2dqq1SbhCuANONP21X
Si9uCknvSROBBIf8WIwd9Nx+yHuEJm0niXna8ssshJPQYdVtFBAy+8fLAUxilHDA
BikppzX+/dY2Su6ttittdUeXIu/StU5OjX2emqhthYRV9/vKikFmZbIBq0m5CmYp
4BgXr7R8/I8L0zqIYckM6KR/yQBAiV3e/4fPDH8oi6fi5HV44DknPLZEPQudaAsi
a7+RdmK5rGhjmWG9WnVO8PYWfrGtWu1IBicEER0K/D0r0+ch1O8772hX4//Avw80
kbX3CITZsjii92Ygek/irxZGOfoIoGoee5Ir6w7FWhG0sOECNQEV0sqxw+JDX7A+
bvgOCmhjbCWmtcAzXpMPin204zgHo8XD6Ng13BJDmysR80Bkr2U7Cv8I0cgMuTYv
27vZj9WA9G18NjEvPZ/aJ+B8wls+xnxrCvTYyf98S3MHGJ+Wi1uArdWpOyfKuM10
JCCg1j4iZwQ53dyRk5BkNGnUsNXXJg0dznMCY1K63kpeNlPRcWniXFhvpqCTLmPK
qOiRzPXknejJ+j93LSD1gYw7rR/HrY3k+ymTav8sdxv5XYOoBlECRAHpKwqPjFzT
MD4jjEXiSwIF5Rj2esabQlYRsLnNLLP33RcJsvvmBhOoGWP5Plv7XSuGanjxrcFs
TJYX7BgGFuZ3zbG5l0WVKsus8jhHrMwWxw6bOIM5OJ9wJbKXYkIrRQoRtEleg1rH
gtD+LWmml2Qulm4kU1WKKXsaDzoTjk/mgo1yFe4sgdmj5FWffWnIJYdW/VoXCroO
`pragma protect end_protected
