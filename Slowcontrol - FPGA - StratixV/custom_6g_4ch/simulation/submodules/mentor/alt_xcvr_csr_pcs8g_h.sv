// Copyright (C) 1991-2016 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, the Altera Quartus Prime License Agreement, the Altera
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 16.0
// ALTERA_TIMESTAMP:Thu Apr 28 07:02:12 PDT 2016
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
V2vdtd6AVb5OIta4dDWaNnQZqYxBGT4h+f//Zf3hxzhVeqJqXMANFS6StcEUDYuK
89hyAobwnFF+ExifKnNAQCPGbHMcj3yFk3tr5/4/ALLNuOf/WYX54aK6NpBmNepu
x6h5Rk0DCeMXMQKdFdgYbB9DQfbekEBI93Sk1NHEEiw=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 1760)
PSCp3GInlKLoabN68GQCfc6MmfsJ401inUiFKEr3Q95wrlPBZbweUDH4nKJzjYGA
TYn7AiT73WhQRENeAt40KGlanlnqsKCnlw9GLVCHgZIKWiSUmAcCt2Y0G/2bvUSf
ZlpzsZvkkg3wRiHh8FwWMHvifhBXRzQ0kDsJktSaZSyi+5/mXxuHEJIF/lTDBhki
mHgGvaPuJYK1uPj4e9u4odRYQV4zSDTvQjY1iYec9PFi93h+ghlyZY60Fx4t0ovp
dCFvN6M7tc6R8YBwftpNFhNw903SlUBxVTGdgnCwcI+tTrCiyHoIs0ytw7E15U7x
7QAqN+9/4A+6TY3gLGYIxC3I4WfpQTqyIQ3N27PfkUBW6dyObAU6S3kCmlTY8KoB
1YEoZ5LB5NPByyOeordfeVZVmiuoFQzjcJboKxcCM0FC1atSyzuf8BoNAWuAKemX
qvdYhfb1YmAGNfi94VnB4P+cYWYUar0sYGZMBVgM0KA+uPVGTaU7bWYc6TvPBTgV
3Q7bWWGcs+cgcSeKyO9RGrUWCl16BLWFP3Sb9O+rbwCCwffEX9RGmgVobBN7ZZtt
CTblzw0aWq0iq/L6JsnR9uRuJT2wsRkywRNiYj03avX+u9JfC4nmfS/q2xTUxHZq
BntJmKjRIpaqaFDCVSjSYUc70kPnlL2EjW6pP0AQh0bx4MEEMOPuxgksbgvalzL3
P22XAEvHSJZjY7wIIupbmYayyfQ6wEbjI3jcfpezHDeq9sXea0vYUQ75kMOn61UX
FHirhVfVkz8i5Nxy3Au/FsoDux1BnOUHF+irDsboT2kcV/ENbvYY0B6Okg/M+KdU
yZ7LAsDM5IF4vEbr+6cJtIEDk+jq2V24ucl0H2b3FIixhSyOWL8mC3mRJ4KMJ4j5
EP5w6NYG03nkYKstw+yGUCmh3fyFL/SXlWydQp3OqqJSGLw1qdcmi0v2h4JD6nG2
YJ7hPzmKzzcs+mP/4Yh5TB16+fqwPEbCeTI6nyFUhJHEJ3kzciG6dvraxuQNhsBF
NqRRKDjH/A5eqAWeMvpxznLEz+dsZW+yaenAUOKzrG7Wkx0Aysw+8EPr57AORFg/
WqDkFK6urQyvD5Toa9YU2KGNnSffYZc2VyZndFFUqx+xKV/b9XpZUcxGl8LglBRV
Cw+PI2wNvUNsnOM5Zq/Gk5bDtvXowDuQ/XIZvbqduATrgSkNUWM5Kn0Gnf0UErvB
1RyN/d7B4d49AMycPkAAvgZWWbAhzbE7gQd6+g9uveq/f7eWELTMa3UutUgXg8mv
8M0R6IoyJ8/73yIEK2dwNni6zF5+BlCFZpO19hBkW02FuzAfRarJ3dreHwoHaa9T
oWwK3w1KcxGE1WamT4KqCTS6oqMbFYrnn3ZQKhZQ7Z3tYoDv+TMxpAtz9hB5pr/C
NMocqCpnUrqUvHIYsGcqZ0Gjs66Z1h1PcobcGQC0Z+vGwbAd8qu85fHMUq7KoAjp
xV/kWKoQ4StaJX2AcG/G4tOvXxj1S69GfWWaiZ7zHzLsxlrbkF5WZOpqXRZRnS8G
SI2Ovo5yLwT9SDC5u5wRfXeVF2w3NykGeWs8YrnFV65ajH0dqdBcQNvQcWG/4WH5
2oh5M7pByNWR8nOwsKjy25iIcdMxTy4dUp0FS1AaQW/GwBaUxPLw97zOmfKL21Ou
1AO+XshWcRPrq7Fopl7WJrEhB9NLBOyME+lgO5XPqWRRC++ksbRe96r2anluXU7o
CbN3TBJp+oWQPeSyP03GlMLba5LzypwUk8VSthlQ3e+N/47+ljKCvEf1tXstXS7d
uEycHOpvlUypX1cCO9wjpdvix51AX+snVoEvkia9cxCJtobp7bG1T+pmdIjk3tRq
hSVyiBG/Jr9ZmtRSp9moC9GG7xFD7UMBtDiM11z7gHdVFUhqTc316/xjXmhLBOFe
41L1SBzaNlQqGx5hqQ0gLu46BZe0c6maeA5mh9E2ntURQVaIxfy9rHiykWTVbJm8
runFYr57vqP5ROqZoz2AOXLO44FlNwXGtEVZIDh5CkcPThlfJihyIF2fTlipLEMA
mG7DxSn2mD8TISfij/yA0aeUJiqKkdgOyPKIus+GAqDkst4VVKjr5ioUY23x0DC3
50vhGcROxpw1T2LXUGv3QGcrx02snhSJiu78vuLRGymyKUBc+gwxJKFqR7hjY/G2
1PwS636rfVrv1gC+sN59RDtSZ+Jv1c+mT8wOGUfjRCfHM9KKQgHAZopDwQrO6lp8
e+1VZtWBBaHs5fIkrocNlX0p2fu6PBQCyvpkzfspPftlE2Pc9JydcxtZq2/zmnZM
NlwbwkFokYZ+wi7FtdRkEmBgRdQduvHa6kAxHszO0cg=
`pragma protect end_protected
