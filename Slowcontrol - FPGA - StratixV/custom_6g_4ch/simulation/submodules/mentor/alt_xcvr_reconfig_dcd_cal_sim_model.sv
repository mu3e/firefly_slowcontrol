// Copyright (C) 1991-2016 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, the Altera Quartus Prime License Agreement, the Altera
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 16.0
// ALTERA_TIMESTAMP:Thu Apr 28 07:05:38 PDT 2016
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
Qu4lR/iGdcZmvym0U30g6xaqnlyBd1iq1kRV0APZ4U9tRkPfrsc1pm1mzdaBGQFA
IEO0X1fZUqh/7apGt/yNeyTyFItsQnxNIUxjZwdFg0oiSMrrni4pp2FnOwRNn/1+
gJ+Wl8hMm8v+XYc0NNHYLD0N2+hhYGlzHDm+kZhBkUo=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2768)
9u3KtmqWJF/kDJ7XxDq7wXGzFcAucBgdlnhN/WZZhsXEbTaksQ+WbQeoDTuNuiVp
Ele1yaRmuap+i69/M0mHBe2elnXPiSYDAJWIXZqpsAO00e4hrNZcgH8ttf5sKX7o
KlT6saegAPfgZpSPGm1FLKQAiQWWn+fI7Olv9vnKrFtOVEg36IAIm8xLO8jLICqt
20y6Z4fjAIfn05j9jwTx5af36dxZ3xeO7aOHi1LcCpmWyiFTWYa8c6EC/HkyZwrL
KL5+HWMh3JZu7ocXdOZEMY+9oUzBD3llu81btSORhvUvl6J6yKbbPrhnTuMvczv7
xOwjxo9QRh3xov2U32TGFZcvzoX72YYmQQ5vxEWTSASc+bNfPskRIawQLI8LRXVm
/yBUg+Ezd41MDT6g9zQ8mUxV83QNsb4sUIsdcvFlzyvnLitYUIQ8JojOUHbhtaxh
KsiZmzrOF4ExaHXxb7ok9JCRjj8kBHJQvjnwpwdvRc4nQT2xwIf1YvYYInhCPfS9
96d6tm+AiFlCkzaucttwo1JnBmknLCekqR0739QVD6LQUVe5Mfk38wC36Hu60H2u
okQ288onIAe1OPvBZTS5h5ovu4WhD1GShrZCKwBbSNjVboOe7dtUsiUF35wwdSdV
XRCTuDbuyxbYRo4yXPxH+8SOkLZl/Fg+OzWf/F/HKR+p4hWIugjeUPawPK1LBmM1
IoM5NmsUTZVgksMu2j7N7andPlqmSonqZuA6R1j7pKnH7Bf4aphBelLIox5PnFk+
GyEOm84WhPmjvlQVpedctlEnnlukFGkKzwKjLKuUoRqWzWx0Swa5mnoMjIw1SBqq
SXDLT8z2twvh6gbDatWHUE0XHRnR3tuZwTzLGhBmivU1Z/aYe++KlDTEHjvzJIvC
53RDUth/apIg9R42miAEOyO7H5iwWTwgdOEWjYvLYPLhXghx8PHBDjm4stFe73U7
8O0BytlmJCi/vF0IhnUMWS1NuEJQkIXK7yFtUWa28hHTED+7ZhKvVo6qAxdM5wNK
GTsaxAvJYfoJEWKqyydZZ7t3uFV/23hw0h1yM9Fi6nt6MgJoDDNkVLZP78FyNYDh
prVezpOz8gHzjKH3LX4jbdbumgtrBq+AOWVvUWpLsyRdDit4585gciyW9maCRgTP
ZBTBf9WD6QvRfYPrxNgFm2zeE6c2tBhLU0hVwgizj8nocB0tONxmpwvqQvxnXKp/
qKfpgd1g/8CoYKFjFTczQYgm31tt+c0R8umclHZWoeYspCdz0VsQpahqeLYItGdR
+G6sxbge+5O8hbhsXQjvYRu9Gx9GHbVtPA8adhDZYqoHeryl0m8XZTK2cvMRBS7v
b9Sorz42DQT5r/yElKWgP2iid9jDvVmuZeaX27JqvVwO9RBTIWhP4VHJOoYt2vRi
HTlYOgnDs4bJ2HoqqNiJmmxTP92n5234i7HO56qlg8mhB4Uv96XU4xzJ8o4KRYEL
F7XhxrlrQiN/uvRrpx1GB0pAnfHGDRKEConFZri4aK4GjCILGht4ZThp2kV/TwL4
aKHyYw4N2l94CQQgunZQoXmjaO0X3E+uaOJ/S0R2/pO8j+pTtuhyLN0kjgMVAJBi
SLyJdXweLR0/nkn+fr3lhJcwX/Hu3tF0bVZNp5Nq9jkM1WynzVSnVtt2W8qdtqQr
XZx4NtsHYP/5v4nc/HAqDlBRTE1RK5yJrLByWfBFLkDGQnwNyLcmKtQFgP+e7nTO
Oz5ie3ykR7rYoB0cr6EJrO9n490RJ1tgxlNe1dI7PQ2IBUDYnTS3XuoIE5HHe3Ul
8Tt5JOk/yTPObfUanf+rhLRqApnCA9s1EtbPfPln/mRAYLkGov0ZBAODKn+l35dU
H06Jfbx8/oViW9eiw0BGEYZG7vwj69QvnCEusrKmKWhDfA6913+Mo3OlUvLM4htc
a/uD0NstGHhLQZuQILnR0N22F14eYcOesqMT/8CYzLaqB2xvNlSaa205THdl7/lp
hBc99dUVVLTkt29Hja+m8ljeuSYTilD2d3vmSSII7c59IAMgPu9gtWMHw0DyohgI
B2oQR2KXkpigbEQ76mACJNkkGrB4bf2f/AlfLEv0p2k43JGgjFma/YrLLyTwV7Rw
17OI6ED2d/8MsOYdcPkqfE0dXKxiAfsvGAZk56RlFEKqKtKntjEBV8JyBPQujIlU
xO3yW8SmyiavYLujtCpQjsJlZf18bXnvSX5+jDCuqTRtP76eg/wCx+VE+NXNRACd
uNxVFSILB9A27BAWqXh5DIHTSO6QsPFgGU6uFi5oM+CmzYy+WKa2QO0sl40ca0Ix
FaU1X6wNOoSNPzzg7sLOks0OlcNZPdVMhgQhEm4yIh1IyxdI+EOq6eocYcGPYxT6
QRftCmnz+6ZF/ugc0h0s8m4V1hyOfUVjd2fgR7/gerznCvWtLh1C/euaV3kyqGtr
d1Lah3jdh1WnuQacOoDzi4y+BkWb9Z0U2jmmWl2Y20R+60utR4PFeK3zxFBsjKTS
z2JakJ8YUY2kd5QmFRMUHBgchB3GN42CFzvV1SYUaZsccYTelI2HSSdllAms2rVx
yGgnWBdk6hN1YC1y1RCSkoa1nSd54XbE0arFonhYMzt+HNNuqVXhvi+OP9EYgUSx
FbfrL6dCKa1zzVANPUrbClU+I4P6pRwAMIXBH3I44UZ4Q2TeKN6q+lS7yJjUvCnF
NLq++3lQedec243uPYKgSZSZil+cbuSrBhRZpai/R6pmD+xmId4Czg5nuTIdSWRN
fNUkzGPQXWVIhJyqmtsmrgfPSsfqBLXbD9IX4Z4Q3B5PgImawJ4v0yZE4TEpF/Bf
jq0LaBkPFwZrz0nuNcxATXChOkhLEZURddoABL6QUrhnT6ZBA7lzDkBMYhMWvf1S
UJA9Kw64aKyvdg64+b5TrPZXLIsmJ48WB/RtE5B/liJBrSgq1xeepr/fj5Eg82co
riLfOmSubBZu916A/eD31fP2R6KSpGsXphseHHvL5O+AjmXL7bbPhquN1f6UVPpd
jxv5ewWCHWFfQGKi9bp1bu17MFiJ9ltGch91bnIKgEAUKd0Fhqev2tcR2AekOu9P
zsADVtrHR1UF9DiQsL6iVOdreolGozloeRxnlrU8tuFECcIcKzxuOFS1PQf2trjP
V3SybUVIrHIhhGS9YO6Xj4qwwseMAEyC9FjcW1SnQFyYSSFl4Lkhj+PHH9Lp49qK
oqYA2YT+TNQy1XKM4sOViHB9gPeq2DF9YDyZcCDtXpeDQKRihP1XFv5ssIRlTd/h
LkPqTTAJd/LkPx+5sme94iKM2YvdsJ65+KuHnuWBWHsLWuhYbH8/j/+W4HPYzhqt
E8NXmni3jb3/ZYP5xfz6aMEJsguLipe2m8xpEfXb2ayxUOpY5spprwkuEdPPLM4C
p25l6F2SG3zZnqA8h0up+XXX3Su0Gu/OAs01melc4wdx50q7vff6X3U2/WZXWonT
5gk1zcFVBxDbae5DsxgVHnoCs4a5w2TqUnGXp+T0awdHeOkZiTI+ISSzB1k67gEr
k0nTiSyzOUQ7EFe+dfkl56NJ486dadyGRVTLNqJTqJdGUmdTxNgyJ12Onws9CTFm
5JuOMbOTiIcer6YlQ9cOHRudN6vxQ4g5MJ8ejmXebOmhoAUhv9ubCrsIuPEa3OZj
SqoW06eqt42YlT/2+r11DPv5My6QjzJECFS3i01dtFk=
`pragma protect end_protected
