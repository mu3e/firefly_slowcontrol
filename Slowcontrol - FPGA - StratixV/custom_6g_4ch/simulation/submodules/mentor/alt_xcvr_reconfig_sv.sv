// Copyright (C) 1991-2016 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, the Altera Quartus Prime License Agreement, the Altera
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 16.0
// ALTERA_TIMESTAMP:Thu Apr 28 07:05:33 PDT 2016
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
JdkD7MhN5xfkvjCFdpogOL1v2xv8VlBjgYOvjGKHxKxbaJq6od7X0t0qJuVilsfp
oUE+Zn+jprOvrcWQhn/rn7UQG83HWD1CKig8N+u1QGnMFIQfFHnochIfm6jiuzNW
fjbLwAtfPw0a1SQgo49qbDhTN4PYdX/fdqNl9YXI6MM=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 3168)
GVALg2OBsMA5UaOQemO0Cj6e4Ln1Dv+b7Lp3DWvf6MhfD0uW7i6gs5Is0OqO+Hs4
+y5fDJHF8sd9GwzjFT95+bE6M7IHMEEgfswAiKfonDsQtw2ULZPGZSmPoan/qZ9y
kgA3i4Idf+tSQU155CVgBdmTj6ZJtADSkaYJQvp070FRiMz+25X7NqUFDWq1rtSc
DcBHkLP4zBWP385YCVeW8vW6ezd6hvPTromEtLXYJzLmbimRhRmC4GFD2ecXP3Fk
zhGlCpiILkJuqTk3Jw5z22ZhjoUQm9crpzappMOij1EGK9muYysC3b8Q2CHfOZjU
jWCPtNUf88bDMg5lncZH0rb2tnMG3IcYhEYe9pKBUwuKDILojFvGPqLUB/Cm8A/l
Eg4wNO17COTHaotF3vH9rjA0G7Ugj4sIAQ/P0Fi5f6PU8aF5G0bvx7qJYPRHREP1
GI1Zjq0Kf0w1tPtJaUF6YU/OlcdHItbuxlzD87vOtfrYQrah6y4uXTx4/JObcOAv
mLH0DHLGpc2gG7y+B54GTD5dyc5GW6GmZxbbpd6P0vdVsa56/VbQFXpsk5yidgY8
52qD3jN2zBQ0SJV+6bp0DMKr+mvJc6FJaEg//J+CyW6Rs0Ld5Goh2yr/j3qMUqTc
RK2QSf02DJZ+IyBKEuFeS8Jvz1tFvzNvgslH5A04XjdCipcyC7RDeiSXUabCNCZH
Xk3TNUS0uWRh0JXl/8JTIgAXoQyeE4ujqIcKBepV/kscxjSUB0oIan6+NOyDaTvF
SJDwH2/ot2L26ZTNucOJLmXM0n6QjDduG9KZERQVE3VQdmlhKwljn7Hgx222Astj
BjRRC9kPeeLG51kIfVAuVjDakvHsbhbaCpF5TJ9uM3J1cAdONAPl9OOFZDNfaCwt
ux7mlUBBYV3HGN88sVgRLoYro2PYD+lIsM8RQDi1nbuE8rNGQxZ3CW07Hkuci/Po
SpBeM96ilg1ax+hSXcmg2/FLIZ1MINOtHYCgP6lPfY2ksdqNBvd6BryVl5TcT/2E
31zQkSksx8k4IQho8/iHLWzBQ9GhgCuoW3M/wQ/omMv4y3XRlyZ1kKUn0cK3ORkC
pxk0T+u65VRQUprRsHVvTNB+YZfgLr7/iVKXktj8dvJVmID5GCTlqsvABV88COsj
iSyhjzsXXJ5nHfW9GQUKSYgG2zmMaF9XmIrZOEKsa7IipU9S8qdtKZHqhGwKj3A5
ni7Ts3beLPEfB79v/GWoM1X4xMVUSwliWWQpYYiSwshhxgtlUupDfywl6fFBAUUs
3Q4kOlAiA3BrhpOI+sFEpwGi5JS4f92F33ZvJy7uu+HtvOY/Uf/7WlwOXRhB6yOt
l+b1FgkqLD4OI5T+1av4sFDe3Dn5GohEvzzd5N5f36aeFjRMrlhK5x1xNOg5qr/n
jsz4OxC4Hax3e6abIQXQtejDOf2lXktlSGeK/KbIa8TtSML2rNVCROIpc0LKSJlD
8xqnbFF1AyJtu6yuygkL41Qg6FGaIDy8aYYS0Mj7gnXuhsBe+hEBnHZuqmvPGxKY
uY1hpRo/A47Obpwr1+n3ET+Db18C1qqpSyDrcGxZYtKylNBDPr+q2k+SX+gIvCG0
7eS6VF+haSpIt549xBY9vj1GCOxOFm9WZOuwYTZjeBJQdDey0MRUT5i32o3RHlon
qH7qxNjqnbadXWFpgOBL4OFuJUGxfAnIWNXtgYZmcIGVLiiJgwvLeZ55ikFXZ5r7
0ZBKDGwVa/BVH5gckFFp6t9Omz7BRJr44kqsKcMh/LdSykzofOQEYlAV7eG4Iub5
CVdA50nPZImS6CUbkC9WBlLa08s+EMvI4NmJGDX1b2zwCiupwhDLRO7qR6mhycUF
+JdL2Bfxn6EKysDJHElt1067x8hXpHWTPQo3hJ+QnWPo0Ja0QDhfkfp/Vj7Z7AwX
TvfMHP8omSMXBS/VsN2wheJjstHzLKRJ4/OkO6gGHocWtIOUPuZ8UgA9YqnZkh3C
TbrrhmlAfTQ/D4bskp9/EWDhHNzci0djX3fp1MeEj0V3qGnj+eBAlacbBJCvNNll
puBTkDLE4wbI5oNDn9VvrlkeGqQoKVwl+OVuWpbOvqNdymSZIBPvA7RtiBAd8rET
oAB593Qm47nMBbAc2hxavLIAgOB4dZctQ6irL9Ylb/MP238HNDyp5x/k2GdPpctr
B9P9WW5kP8dyrKas5QJj6FSFuXyxvC+ozJCIwlOlRFyQC3RtrL3mJIHpzqFGFxQ2
zMe2GfJb5PckGIZ3A5Xt8GIDQ6CWNaXDpAeUYxpjG7I+wQJCVDVUvJOxuud3roHw
fKQooeSj9YPI1rKUjqJrqDeMb7MQ11bmEc2xsx3gIRuoxB0YwhicwV2JpRGKW15T
AJ4PCl75DCMOstWcOIuIYqLgoHS1gH4vipr9usxvBwPZIM8I2bChArqQGVgl7TJs
rv2Ty3FAjKuC+uKzC9JhHSdVWH4c9GJ3FcxNcDRjBaXhtVZ6TscrhdowNCmpckBj
RUBxWYWzZj/eAwh8cHenbjmLglH13p1EXEFfqRmBE4zLqXX1995XbSK+aNVSqtF9
bIPz9nbu+CPEip2PbCVBwP1nCEK2voDfAfBMwohQ9kWsRyuIlHFR38XeWmKFajBY
68ePEjuVNTVs3jiz0NtU2I3hcKHJ4W4nkCnIAmajzK8xmIDxKdWLEY9E7qD/bftw
m3Z49VlPtP2tndau7mXrhOuJH3t4j/2ZeaG9LfpSnxl26tWYdnxuEQC8GVxph27G
Bby//qYm5qefWUhoh+Mud+pQtX3eqHF90BpZWaEM3ISs4Ep152Q1qNPxJHWeu52o
QHd37WX/r+byX29z2SCLM/ZzSLZ3BQ0vylZuK3QrUjRAI0hAUmar7A7BOfypl90e
N+OAcjgJumflUPcHYbph7d7HOO28FAX++rKeuRimpZxvFFn8Ol3+OCTlTNhTOw1b
gqAd++JXRR/Gs5bEEITpKyvMEUNci2Vy1G+xZBKln9GW7XoFtSCmwQ2ab+1kkf/c
1ahMNLI4V+PH31Woma8SFm64duHdaPqrSDxnFMz0WEaBhDHaiJmbiaBCS83IoQy3
Wfx5aPdu+uCem2/cXHNFul8Wh+vvebqTP2EYhTvk+zWNcXaYMNGz7PwvFDx/LJxs
wMZauZXgkKJnfz4OCboqbvZqDg5lW+RvRIb/WUtC683PjxuHIo91eJD4PTwnK0iB
j2x5LLz4yRA4m1/7e6tY08cbb5I7ayn2Bh/7q+NAJrFfncc4K3X7x+gyPRm8owW4
9PDRUuZhikMfzzbETSkCp9srMYxxHj275E0lPFKPm5JJ38LWjxMG3PAQ8iHYnl8G
R3hi56I5KnJ8B7Is1JaM2n3frcl788WaAP+4mwof4Ke46ipqhbwU3qOmkjHU9max
HVy9JNBrgcKPDaJONT43ts+waimzJkGhnTNowBAYdi5IDpePRDZyndA92BLWKw/y
4FhG+dV7M2qIAYlhaNPm4Ddq6u9Yu2ii0JwqfH30UJUTTdRUP3TZtx5uOCTj8MdI
cexhpLSPLlBSJTWEub/1vVEAy9WB2HyA96/5zwf6E9xq0wgyZQAV+ovOLcPT5eu8
CtXfhzqzwTAbOy+W0sh8q/RyszPYjv8EPzLfYvlgn0fv8U5wz3LU2r7vs6i+2CEQ
MREsxzMM0tfU1Lx/HUsB2xWPgEd4vYpfgldZZe+6dZcjRwamGtq5Mu074lEqyuuM
P6grI3ZK1dtYnZqxdr8Df90DcOs1fiA8WOdhaq5QUzrd5O+Pt39pGaPgNTLBb1zP
dldUOOPNrX/fGCKhkoB1qWofwms+5753d+3SMuWKC8CNZW+Y71+fiP7Anujcqmuq
cted44f/GvsfRaknocy0Z+PJ0i8mrnhLFR9yu2zLCcs/aFKxUJXQoI+ENVFHLG24
nyNWNTg4ppRObLXFoNnWZAubvRe3IPE0lLXrHHwpA/l2GFo3FYiASJBFw+1fE5eg
4xcorOh5hGT/DzXvf3SriBz/w37xj9uRwGMDGm0Is4fnDhYuTwPnTvAl4f7MgQZA
pBfOi0PzkHoiY7Jy0W6lQ2R5CaCOh101UV75oPBIYz/CMPnDOhAya6BnQjSzNgqj
Hgf0Kkbvpk1pCeaDlVS/qxPDLrOFA7Stqjr8gaqceg5TUFVOWs9N0vp0s43ykTun
gnS+g4spe6VVZqCXcN5XLRbEJpy0MXfSoV8igQfTPQ3BguSrVWs9FchFs8ST5vB/
`pragma protect end_protected
