
module custom_6g_4ch (
	clk_50_clk,
	clk_50_reset_reset_n,
	i2c_master_avalon_slave_0_chipselect,
	i2c_master_avalon_slave_0_write_n,
	i2c_master_avalon_slave_0_read_n,
	i2c_master_avalon_slave_0_readdata,
	i2c_master_avalon_slave_0_writedata,
	i2c_master_avalon_slave_0_address,
	i2c_master_avalon_slave_0_waitrequest_n,
	i2c_master_interrupt_sender_0_irq,
	i2c_master_reset_sink_0_reset_n,
	pio_out_export);	

	input		clk_50_clk;
	input		clk_50_reset_reset_n;
	output		i2c_master_avalon_slave_0_chipselect;
	output		i2c_master_avalon_slave_0_write_n;
	output		i2c_master_avalon_slave_0_read_n;
	input	[7:0]	i2c_master_avalon_slave_0_readdata;
	output	[7:0]	i2c_master_avalon_slave_0_writedata;
	output	[2:0]	i2c_master_avalon_slave_0_address;
	input		i2c_master_avalon_slave_0_waitrequest_n;
	input		i2c_master_interrupt_sender_0_irq;
	output		i2c_master_reset_sink_0_reset_n;
	output	[31:0]	pio_out_export;
endmodule
