	custom_6g_4ch u0 (
		.clk_50_clk                              (<connected-to-clk_50_clk>),                              //                        clk_50.clk
		.clk_50_reset_reset_n                    (<connected-to-clk_50_reset_reset_n>),                    //                  clk_50_reset.reset_n
		.i2c_master_avalon_slave_0_chipselect    (<connected-to-i2c_master_avalon_slave_0_chipselect>),    //     i2c_master_avalon_slave_0.chipselect
		.i2c_master_avalon_slave_0_write_n       (<connected-to-i2c_master_avalon_slave_0_write_n>),       //                              .write_n
		.i2c_master_avalon_slave_0_read_n        (<connected-to-i2c_master_avalon_slave_0_read_n>),        //                              .read_n
		.i2c_master_avalon_slave_0_readdata      (<connected-to-i2c_master_avalon_slave_0_readdata>),      //                              .readdata
		.i2c_master_avalon_slave_0_writedata     (<connected-to-i2c_master_avalon_slave_0_writedata>),     //                              .writedata
		.i2c_master_avalon_slave_0_address       (<connected-to-i2c_master_avalon_slave_0_address>),       //                              .address
		.i2c_master_avalon_slave_0_waitrequest_n (<connected-to-i2c_master_avalon_slave_0_waitrequest_n>), //                              .waitrequest_n
		.i2c_master_interrupt_sender_0_irq       (<connected-to-i2c_master_interrupt_sender_0_irq>),       // i2c_master_interrupt_sender_0.irq
		.i2c_master_reset_sink_0_reset_n         (<connected-to-i2c_master_reset_sink_0_reset_n>),         //       i2c_master_reset_sink_0.reset_n
		.pio_out_export                          (<connected-to-pio_out_export>)                           //                       pio_out.export
	);

