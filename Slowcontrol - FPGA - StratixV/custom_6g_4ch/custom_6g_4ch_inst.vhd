	component custom_6g_4ch is
		port (
			clk_50_clk                              : in  std_logic                     := 'X';             -- clk
			clk_50_reset_reset_n                    : in  std_logic                     := 'X';             -- reset_n
			i2c_master_avalon_slave_0_chipselect    : out std_logic;                                        -- chipselect
			i2c_master_avalon_slave_0_write_n       : out std_logic;                                        -- write_n
			i2c_master_avalon_slave_0_read_n        : out std_logic;                                        -- read_n
			i2c_master_avalon_slave_0_readdata      : in  std_logic_vector(7 downto 0)  := (others => 'X'); -- readdata
			i2c_master_avalon_slave_0_writedata     : out std_logic_vector(7 downto 0);                     -- writedata
			i2c_master_avalon_slave_0_address       : out std_logic_vector(2 downto 0);                     -- address
			i2c_master_avalon_slave_0_waitrequest_n : in  std_logic                     := 'X';             -- waitrequest_n
			i2c_master_interrupt_sender_0_irq       : in  std_logic                     := 'X';             -- irq
			i2c_master_reset_sink_0_reset_n         : out std_logic;                                        -- reset_n
			pio_out_export                          : out std_logic_vector(31 downto 0)                     -- export
		);
	end component custom_6g_4ch;

	u0 : component custom_6g_4ch
		port map (
			clk_50_clk                              => CONNECTED_TO_clk_50_clk,                              --                        clk_50.clk
			clk_50_reset_reset_n                    => CONNECTED_TO_clk_50_reset_reset_n,                    --                  clk_50_reset.reset_n
			i2c_master_avalon_slave_0_chipselect    => CONNECTED_TO_i2c_master_avalon_slave_0_chipselect,    --     i2c_master_avalon_slave_0.chipselect
			i2c_master_avalon_slave_0_write_n       => CONNECTED_TO_i2c_master_avalon_slave_0_write_n,       --                              .write_n
			i2c_master_avalon_slave_0_read_n        => CONNECTED_TO_i2c_master_avalon_slave_0_read_n,        --                              .read_n
			i2c_master_avalon_slave_0_readdata      => CONNECTED_TO_i2c_master_avalon_slave_0_readdata,      --                              .readdata
			i2c_master_avalon_slave_0_writedata     => CONNECTED_TO_i2c_master_avalon_slave_0_writedata,     --                              .writedata
			i2c_master_avalon_slave_0_address       => CONNECTED_TO_i2c_master_avalon_slave_0_address,       --                              .address
			i2c_master_avalon_slave_0_waitrequest_n => CONNECTED_TO_i2c_master_avalon_slave_0_waitrequest_n, --                              .waitrequest_n
			i2c_master_interrupt_sender_0_irq       => CONNECTED_TO_i2c_master_interrupt_sender_0_irq,       -- i2c_master_interrupt_sender_0.irq
			i2c_master_reset_sink_0_reset_n         => CONNECTED_TO_i2c_master_reset_sink_0_reset_n,         --       i2c_master_reset_sink_0.reset_n
			pio_out_export                          => CONNECTED_TO_pio_out_export                           --                       pio_out.export
		);

