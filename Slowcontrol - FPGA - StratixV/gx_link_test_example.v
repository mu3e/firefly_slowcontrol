//-----------------------------------------------------------------------------
// Title         : <Stratix V Transceiver Toolkit Example Design>
// Project       : <Demo Example for Stratix V SI Board>
//-----------------------------------------------------------------------------
// File          : {gx_link_test_example.v, custom_6g.qsys, design.sdc, Tcl scripts}
// Author        : thteoh
// Created       : 11.1
//-----------------------------------------------------------------------------
// Description :
// Data Rate   - 6.4453125Gbps
// Data Width  - 32 bits
// Ref. Clock  - 644.53125 MHz
// No. Channel	- 4
// 
//-----------------------------------------------------------------------------
// Copyright (c) 2011 by Altera This model is the confidential and
// proprietary property of Altera and the possession or use of this
// file requires a written license from Altera.
//------------------------------------------------------------------------------

module gx_link_test_example (

	input wire reset,
//	input	wire		C64453mhz,
	input 	wire		C100mhz,
	input	 wire push_button0,
	output reg [7:0] LED_n,	
//	input 	wire 		C125mhz,

	
	output wire ModSel_n,		//Module select: active low, when host wants to communicate (I2C) with module
	output wire Rst_n,			//Module reset: active low, complete reset of module. Module indicates reset done by "low" interrupt_n (data_not_ready is negated).
	inout  wire Scl,			//I2C Clock
	inout  wire Sda,			//I2C Data
	input  wire Int_n,			//QSFP Interrupt: when low: operational fault or status critical. after reset: goes high, and data_not_ready is read with '0' (byte 2 bit 0) and flag field is read
	input  wire ModPrs_n		//Module present: Pulled to ground if module is present
);


//wire	reconfig_busy;
//	
//wire [7:0] pll_locked;                 //         xcvr_reset_control_0_pll_locked.pll_locked
//wire [1:0] pll_powerdown;           //      xcvr_reset_control_0_pll_powerdown.pll_powerdown
//reg  [0:0] pll_select;                 //         xcvr_reset_control_0_pll_select.pll_select


wire 		  i2c_chipselect;
wire 		  i2c_write_n;
wire		  i2c_read_n;
wire [7:0] i2c_readdata;
wire [7:0] i2c_writedata;
wire [2:0] i2c_address;
wire		  i2c_waitrequest_n;
wire		  i2c_irq;
wire		  i2c_reset_n;

wire		  scl_pad_i;
wire       scl_pad_o;
wire	 	  scl_padoen_o;
wire		  sda_pad_i;
wire		  sda_pad_o;
wire		  sda_padoen_o;
wire [31:0] nios_qsfp;

wire [7:0] test_sig;

//wire clk_pll;
//wire clk_pll1;
//wire clk_pll_locked;
//	
//   wire [7:0]		   system_reset_cnt;
//   reg 				   system_reset=1'b0;
//	wire 					system_reset_n;
//	
//	reg [19:0]			sync_reset_cnt;
//	reg  					sync_reset;   
//
//	reg db_state;
//	reg [11:0]  		db_counter;
	
//    // Reset Counter to give active high reset to enable offset cancellation
//	counter_128 cnt_128_isnt (
//		.clock	(C100mhz),
//		.q			(system_reset_cnt)
//	);
//
//
//    always @ (posedge C100mhz)
//    begin
//       if (system_reset_cnt >= 8'd128)
//           begin
//              system_reset <= 1'b1;
//           end      
//    end	
//	 
//// switch between reset and clock output	 
//	assign CLK_OUT_0 = (db_state == 0) ? clk_pll : sync_reset;
//	assign CLK_OUT_1 = (db_state == 0) ? clk_pll : sync_reset;
//	assign CLK_OUT_2 = (db_state == 0) ? clk_pll : sync_reset;
//	
//// display which state we are in: 0 (flash = clock) or 1 (off = reset)	
//	assign LED_0	= db_state;	 
//	
//    always @ (posedge C100mhz)
//    begin
//       if (system_reset == 1'b0)
//			begin
//				db_state		<= 1'b0;
//				db_counter  <= 12'h000;
//         end   
//		  else
//		  begin
//				db_counter <= {db_counter[10:0] , push_button0};
//				if(db_counter == 12'h800)begin
//					db_state 	<= !db_state;
//				end
//		  end
//    end	 	
//	  
//// sync reset with a rate of a couple 100 Hz	
//    always @ (posedge clk_pll)
//    begin
//       if (system_reset == 1'b0)
//			begin
//				sync_reset		<= 1'b0;
//				sync_reset_cnt <= 20'h00000;
//         end   
//		  else
//		  begin
//				sync_reset_cnt <= sync_reset_cnt + 1'b1;		
//				if(sync_reset_cnt == 20'hFFFFF)
//					begin
//					sync_reset 	<= 1'b1;
//					end
//				else
//					begin
//					sync_reset 	<= 1'b0;				
//					end
//		  end
//    end	 	 
//	 
//
//	 assign system_reset_n	= !system_reset;
//	 
//pll pll_96(
//		.refclk	 (C100mhz),   //  refclk.clk
//		.rst		 (system_reset_n),      //   reset.reset
//		.outclk_0 (clk_pll), // outclk0.clk
//		.outclk_1 (clk_pll1), // outclk0.clk		
//		.locked   (clk_pll_locked) //  locked.export
//	);
//	 
//	 

custom_6g_4ch DUT (	 
		
		.clk_50_clk										 		 (C100mhz),
		.clk_50_reset_reset_n 								 (reset),
		
		.i2c_master_avalon_slave_0_chipselect			 (i2c_chipselect),
		.i2c_master_avalon_slave_0_write_n 				 (i2c_write_n),
		.i2c_master_avalon_slave_0_read_n				 (i2c_read_n),
		.i2c_master_avalon_slave_0_readdata				 (i2c_readdata),
		.i2c_master_avalon_slave_0_writedata			 (i2c_writedata),
		.i2c_master_avalon_slave_0_address				 (i2c_address),
		.i2c_master_avalon_slave_0_waitrequest_n		 (i2c_waitrequest_n),
		.i2c_master_interrupt_sender_0_irq 				 (i2c_irq),
		.i2c_master_reset_sink_0_reset_n              (i2c_reset_n),
		.pio_out_export									 	 (nios_qsfp)
		
			
);


	
firmware_test test (
		.clk		(C100mhz),
		.reset_n	(reset),
		.LED_n		(LED_n),
		.test_sig (test_sig)
);
i2c_master_top qsfp (
	//wishbone signals
		.wb_Clk_i	(C100mhz), 
		.wb_rst_i	('b0),
		.arst_i 	 	(i2c_reset_n),
		.wb_adr_i	(i2c_address),
		.wb_dat_i	(i2c_writedata),
		.wb_dat_o	(i2c_readdata),
		.wb_we_i		(wb_we_i),
		.wb_stb_i 	(i2c_chipselect),
		.wb_cyc_i 	(wb_cyc_i),
		.wb_ack_o	(i2c_waitrequest_n),
		.wb_inta_o	(i2c_irq),
	// i2c lines
		.scl_pad_i	(scl_pad_i),
		.scl_pad_o	(scl_pad_o),
		.scl_padoen_o	(scl_padoen_o),
		.sda_pad_i   (sda_pad_i),	                 
		.sda_pad_o   (sda_pad_o),                  
		.sda_padoen_o  (sda_padoen_o)
		
	
	);
	assign test_sig[1] = i2c_reset_n;

	assign test_sig[5] = nios_qsfp[4];
	assign test_sig[6] = nios_qsfp[0];
	assign test_sig[7] = Int_n;
	
	assign wb_we_i = (~i2c_write_n) & (i2c_read_n);
	assign wb_cyc_i = (~i2c_write_n) | (~i2c_read_n);
	
	assign scl_pad_i = Scl;
	assign Scl = scl_padoen_o? 'bz: scl_pad_o;
	assign sda_pad_i = Sda;
	assign Sda = sda_padoen_o? 'bz: sda_pad_o;
	
	assign ModSel_n = nios_qsfp[4];
	assign Rst_n = nios_qsfp[0];
endmodule