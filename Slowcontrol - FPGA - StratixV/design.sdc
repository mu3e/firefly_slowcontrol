#**************************************************************
# Time Information
#**************************************************************

set_time_format -unit ns -decimal_places 3

#**************************************************************
# Create Clock
#**************************************************************

create_clock -period 100MHz [get_ports {C100mhz}]
create_clock -period 125MHz [get_ports {C125mhz}]
#create_clock -period 644.53MHz [get_ports {C64453mhz}]
#create_clock -period 100MHz [get_ports {C64453mhz}]
create_clock -name {altera_reserved_tck} -period 33 [get_ports {altera_reserved_tck}]
create_clock -period 100MHz [get_keepers {custom_6g_4ch:DUT|alt_xcvr_reconfig:alt_xcvr_reconfig_0|alt_xcvr_reconfig_dfe:dfe.sc_dfe|alt_xcvr_reconfig_dfe_sv:dfe_sv|alt_xcvr_reconfig_dfe_cal_sv:inst_xreconfig_cal|dfe_calibrator_sv:inst_calibrator*|alt_cal_edge_detect:inst_edge_detect|alt_edge_det_ff1}]
derive_pll_clocks -create_base_clocks


#**************************************************************
# Create Generated Clock
#**************************************************************


#**************************************************************
# Set Clock Latency
#**************************************************************



#**************************************************************
# Set Clock Uncertainty
#**************************************************************
derive_clock_uncertainty


#**************************************************************
# Set Input Delay
#**************************************************************
set_input_delay -clock {altera_reserved_tck} 8 [get_ports altera_reserved_tdi]
set_input_delay -clock {altera_reserved_tck} 8 [get_ports altera_reserved_tms]
set_input_delay -clock {altera_reserved_tck} 8 [get_ports altera_reserved_ntrst]


#**************************************************************
# Set Output Delay
#**************************************************************
set_output_delay -clock {altera_reserved_tck} 8 [get_ports altera_reserved_tdo]


#**************************************************************
# Set Clock Groups
#**************************************************************
set_clock_groups -asynchronous \
-group {C100mhz} \
-group {C64453mhz} \
-group {altera_reserved_tck} \
-group {sv_reconfig_pma_testbus_clk} \
-group [get_clocks {DUT|xcvr_custom_phy_0|S5|transceiver_core|gen.sv_xcvr_native_insts[0].gen_bonded_group.sv_xcvr_native_inst|inst_sv_pcs|ch[0].inst_sv_pcs_ch|inst_stratixv_hssi_8g_rx_pcs|wys|clocktopld}] \
-group [get_clocks {DUT|xcvr_custom_phy_0|S5|transceiver_core|gen.sv_xcvr_native_insts[1].gen_bonded_group.sv_xcvr_native_inst|inst_sv_pcs|ch[0].inst_sv_pcs_ch|inst_stratixv_hssi_8g_rx_pcs|wys|clocktopld}] \
-group [get_clocks {DUT|xcvr_custom_phy_0|S5|transceiver_core|gen.sv_xcvr_native_insts[2].gen_bonded_group.sv_xcvr_native_inst|inst_sv_pcs|ch[0].inst_sv_pcs_ch|inst_stratixv_hssi_8g_rx_pcs|wys|clocktopld}] \
-group [get_clocks {DUT|xcvr_custom_phy_0|S5|transceiver_core|gen.sv_xcvr_native_insts[3].gen_bonded_group.sv_xcvr_native_inst|inst_sv_pcs|ch[0].inst_sv_pcs_ch|inst_stratixv_hssi_8g_rx_pcs|wys|clocktopld}] \
-group [get_clocks {DUT|xcvr_custom_phy_0|S5|transceiver_core|gen.sv_xcvr_native_insts[0].gen_bonded_group.sv_xcvr_native_inst|inst_sv_pcs|ch[0].inst_sv_pcs_ch|inst_stratixv_hssi_8g_tx_pcs|wys|clkout}] \
-group [get_clocks {DUT|xcvr_custom_phy_0|S5|transceiver_core|gen.sv_xcvr_native_insts[1].gen_bonded_group.sv_xcvr_native_inst|inst_sv_pcs|ch[0].inst_sv_pcs_ch|inst_stratixv_hssi_8g_tx_pcs|wys|clkout}] \
-group [get_clocks {DUT|xcvr_custom_phy_0|S5|transceiver_core|gen.sv_xcvr_native_insts[2].gen_bonded_group.sv_xcvr_native_inst|inst_sv_pcs|ch[0].inst_sv_pcs_ch|inst_stratixv_hssi_8g_tx_pcs|wys|clkout}] \
-group [get_clocks {DUT|xcvr_custom_phy_0|S5|transceiver_core|gen.sv_xcvr_native_insts[3].gen_bonded_group.sv_xcvr_native_inst|inst_sv_pcs|ch[0].inst_sv_pcs_ch|inst_stratixv_hssi_8g_tx_pcs|wys|clkout}] \
-group [get_clocks {DUT|xcvr_custom_phy_0|S5|transceiver_core|gen.sv_xcvr_native_insts[0].gen_bonded_group.sv_xcvr_native_inst|inst_sv_pcs|ch[0].inst_sv_pcs_ch|inst_stratixv_hssi_8g_rx_pcs|wys|observablebyteserdesclock}] \
-group [get_clocks {DUT|xcvr_custom_phy_0|S5|transceiver_core|gen.sv_xcvr_native_insts[1].gen_bonded_group.sv_xcvr_native_inst|inst_sv_pcs|ch[0].inst_sv_pcs_ch|inst_stratixv_hssi_8g_rx_pcs|wys|observablebyteserdesclock}] \
-group [get_clocks {DUT|xcvr_custom_phy_0|S5|transceiver_core|gen.sv_xcvr_native_insts[2].gen_bonded_group.sv_xcvr_native_inst|inst_sv_pcs|ch[0].inst_sv_pcs_ch|inst_stratixv_hssi_8g_rx_pcs|wys|observablebyteserdesclock}] \
-group [get_clocks {DUT|xcvr_custom_phy_0|S5|transceiver_core|gen.sv_xcvr_native_insts[3].gen_bonded_group.sv_xcvr_native_inst|inst_sv_pcs|ch[0].inst_sv_pcs_ch|inst_stratixv_hssi_8g_rx_pcs|wys|observablebyteserdesclock}] \
-group [get_clocks {DUT|xcvr_custom_phy_0|S5|transceiver_core|gen.sv_xcvr_native_insts[0].gen_bonded_group.sv_xcvr_native_inst|inst_sv_pcs|ch[0].inst_sv_pcs_ch|inst_stratixv_hssi_8g_tx_pcs|wys|observablebyteserdesclock}] \
-group [get_clocks {DUT|xcvr_custom_phy_0|S5|transceiver_core|gen.sv_xcvr_native_insts[1].gen_bonded_group.sv_xcvr_native_inst|inst_sv_pcs|ch[0].inst_sv_pcs_ch|inst_stratixv_hssi_8g_tx_pcs|wys|observablebyteserdesclock}] \
-group [get_clocks {DUT|xcvr_custom_phy_0|S5|transceiver_core|gen.sv_xcvr_native_insts[2].gen_bonded_group.sv_xcvr_native_inst|inst_sv_pcs|ch[0].inst_sv_pcs_ch|inst_stratixv_hssi_8g_tx_pcs|wys|observablebyteserdesclock}] \
-group [get_clocks {DUT|xcvr_custom_phy_0|S5|transceiver_core|gen.sv_xcvr_native_insts[3].gen_bonded_group.sv_xcvr_native_inst|inst_sv_pcs|ch[0].inst_sv_pcs_ch|inst_stratixv_hssi_8g_tx_pcs|wys|observablebyteserdesclock}]

#**************************************************************
# Set False Path
#**************************************************************

set_false_path -from *altera_reset_synchronizer_int_chain_out


#**************************************************************
# Set Multicycle Path
#**************************************************************



#**************************************************************
# Set Maximum Delay
#**************************************************************


#**************************************************************
# Set Minimum Delay
#**************************************************************



#**************************************************************
# Set Input Transition
#**************************************************************

