/*
 * slowcontrol_firefly.h
 *
 *  Created on: 09.06.2017
 *      Author: weinlaeder
 */

#ifndef SLOWCONTROL_FIREFLY_H_
#define SLOWCONTROL_FIREFLY_H_

#include <stddef.h>
#include <sys/alt_stdio.h>
#include <sys/alt_irq.h>
#include <stdlib.h>
#include "altera_avalon_pio_regs.h"
#include "sys/alt_alarm.h"
#include "alt_types.h"
#include "system.h"
#include "io.h"
///////////////////////////////////////////////////////
// constants
// I2C MASTER REGISTERS
#define REG_I2C_PRERlo 	0x0
#define REG_I2C_PRERhi 	0x1
#define REG_I2C_CTR		0x2
#define REG_I2C_TXR		0x3
#define REG_I2C_RXR		0x3
#define REG_I2C_CR		0x4
#define REG_I2C_SR		0x4



#endif /* SLOWCONTROL_FIREFLY_H_ */
