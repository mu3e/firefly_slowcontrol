/* 
 * "Small Hello World" example. 
 * 
 * This example prints 'Hello from Nios II' to the STDOUT stream. It runs on
 * the Nios II 'standard', 'full_featured', 'fast', and 'low_cost' example 
 * designs. It requires a STDOUT  device in your system's hardware. 
 *
 * The purpose of this example is to demonstrate the smallest possible Hello 
 * World application, using the Nios II HAL library.  The memory footprint
 * of this hosted application is ~332 bytes by default using the standard 
 * reference design.  For a more fully featured Hello World application
 * example, see the example titled "Hello World".
 *
 * The memory footprint of this example has been reduced by making the
 * following changes to the normal "Hello World" example.
 * Check in the Nios II Software Developers Manual for a more complete 
 * description.
 * 
 * In the SW Application project (small_hello_world):
 *
 *  - In the C/C++ Build page
 * 
 *    - Set the Optimization Level to -Os
 * 
 * In System Library project (small_hello_world_syslib):
 *  - In the C/C++ Build page
 * 
 *    - Set the Optimization Level to -Os
 * 
 *    - Define the preprocessor option ALT_NO_INSTRUCTION_EMULATION 
 *      This removes software exception handling, which means that you cannot 
 *      run code compiled for Nios II cpu with a hardware multiplier on a core 
 *      without a the multiply unit. Check the Nios II Software Developers 
 *      Manual for more details.
 *
 *  - In the System Library page:
 *    - Set Periodic system timer and Timestamp timer to none
 *      This prevents the automatic inclusion of the timer driver.
 *
 *    - Set Max file descriptors to 4
 *      This reduces the size of the file handle pool.
 *
 *    - Check Main function does not exit
 *    - Uncheck Clean exit (flush buffers)
 *      This removes the unneeded call to exit when main returns, since it
 *      won't.
 *
 *    - Check Don't use C++
 *      This builds without the C++ support code.
 *
 *    - Check Small C library
 *      This uses a reduced functionality C library, which lacks  
 *      support for buffering, file IO, floating point and getch(), etc. 
 *      Check the Nios II Software Developers Manual for a complete list.
 *
 *    - Check Reduced device drivers
 *      This uses reduced functionality drivers if they're available. For the
 *      standard design this means you get polled UART and JTAG UART drivers,
 *      no support for the LCD driver and you lose the ability to program 
 *      CFI compliant flash devices.
 *
 *    - Check Access device drivers directly
 *      This bypasses the device file system to access device drivers directly.
 *      This eliminates the space required for the device file system services.
 *      It also provides a HAL version of libc services that access the drivers
 *      directly, further reducing space. Only a limited number of libc
 *      functions are available in this configuration.
 *
 *    - Use ALT versions of stdio routines:
 *
 *           Function                  Description
 *        ===============  =====================================
 *        alt_printf       Only supports %s, %x, and %c ( < 1 Kbyte)
 *        alt_putstr       Smaller overhead than puts with direct drivers
 *                         Note this function doesn't add a newline.
 *        alt_putchar      Smaller overhead than putchar with direct drivers
 *        alt_getchar      Smaller overhead than getchar with direct drivers
 *
 */
#include "slowcontrol_firefly.h"
#include "sys/alt_stdio.h"
#include <math.h>
#include <unistd.h>

//-----------------------------------I2C----------------------------------------------------//


int I2C_READ(int base, int address){
	//alt_printf("base: %x read from: %x\n", base, (address & 0x7));
	return IORD_8DIRECT(base, (address & 0x7));//I2C_MASTER_TEMP_BASE
}

void I2C_WRITE(int base, int address, int data){
	//alt_printf("base: %x writing address: %x data: %x\n",base,(address & 0x7), (data & 0xFF));
	IOWR_8DIRECT(base, (address & 0x7), (data & 0xFF));

}

void i2c_check(int base){
	alt_printf("read I2C master registers\n");
	alt_printf("register 0x0 (Prescaler LSB): %x\n",	I2C_READ(base, 0));	// REG_I2C_PRERlo
	alt_printf("register 0x1 (Prescaler MSB): %x\n",	I2C_READ(base, 1));	// REG_I2C_PRERhi
	alt_printf("register 0x2 (Control register): %x\n",	I2C_READ(base, 2));	// REG_I2C_CTR
	alt_printf("register 0x3 (Rx last word): %x\n",		I2C_READ(base, 3));	// REG_I2C_RXR
	alt_printf("register 0x4 (Status): %x\n",			I2C_READ(base, 4));	// REG_I2C_SR
}

void i2c_startup(int base){

	I2C_WRITE(base, REG_I2C_CTR, 0x00);
	I2C_WRITE(base, REG_I2C_CR, 0x01);
	if(I2C_READ(base, REG_I2C_CTR) == 0)
	{
		alt_printf("ok, we can start, disabeld i2c master and cleared interrupts\n");
	}
	I2C_WRITE(base, REG_I2C_CTR, 0x00);
	I2C_WRITE(base, REG_I2C_PRERlo, 0xFA);	// prescaler for 80 MHz to 400 kHz: LSB=0x27, for 100 kHz: LSB = 9F // LSB = (f_in/(5*f_i2c)) - 1; for 100MHz to 400kHz: LSB = 0x31 for 100MHz to 100kHz: LSB = 0xC7
	I2C_WRITE(base, REG_I2C_PRERhi, 0x00);	// prescaler: MSB = 0x00
	I2C_WRITE(base, REG_I2C_CTR, 0xC0);    // enable I2C core and enables Interrupts; dis: 0x80)
	I2C_WRITE(base, REG_I2C_TXR, 0x00);	// clear TX register
	I2C_WRITE(base, REG_I2C_CR,  0x00);	// clear Command register
	alt_printf("ok, we enabled the i2c master and set the prescalers\n");
	i2c_check(base);
}


void i2c_master_ctrl(int base, int reg, int data){
	alt_printf("input: %x %x\n", reg, data);
	alt_printf("before: %x\n", I2C_READ(base, reg & 0xFF));
	I2C_WRITE(base, reg, data);
	alt_printf("afterwards: %x\n", I2C_READ(base, reg));
}

int i2c_wait(int base){
	// status register for read: 0x4 -> check if transfer is still in progress
	int result = 0;
	int temp = I2C_READ(base, REG_I2C_SR);
	//alt_printf("current status: %x\n",temp);
	while((temp & 0x2) != 0){
		temp = I2C_READ(base, REG_I2C_SR);
		//alt_printf("loop current status: %x\n",temp);
	}
	//alt_printf("ack?: %x\n",temp);
	// check for acknowledge
	if((temp & 0x80) != 0){
		temp = I2C_READ(base, REG_I2C_SR);
		//alt_printf("ack 2nd try?: %x\n",temp);
			if((temp & 0x80) != 0){
				temp = I2C_READ(base, REG_I2C_SR);
				//alt_printf("ack 3rd try?: %x\n",temp);
				if((temp & 0x80) != 0){
					temp = I2C_READ(base, REG_I2C_SR);
					//alt_printf("ack 4th try?: %x\n",temp);
					if((temp & 0x80) != 0){
						//alt_printf("no acknolewdge received!\n");
						result = -1;
					}
				}
			}
	}
	//alt_printf("irq?: %x\n",temp);
	if((temp & 0x01) != 0){
		//alt_printf("clearing the interrupt...\n");
		I2C_WRITE(base, REG_I2C_CR,0x01);
//	}else{
//		alt_printf("must have been cleared already...\n");
	}
	return result;
}

void i2c_write(int base, int slave, int reg, int data){
/*	Write 1 byte of data to a slave.
	Slave address = 0x51 (b�1010001�)
	Data to write = 0xAC
	I2C Sequence:
	1) generate start command
	2) write slave address + write bit
	3) receive acknowledge from slave
	4) write data
	5) receive acknowledge from slave
	6) generate stop command
	Commands:
	1) write 0xA2 (address + write bit) to Transmit Register, set STA bit, set WR bit.
	-- wait for interrupt or TIP flag to negate --
	2) read RxACK bit from Status Register, should be �0�.
	write 0xAC to Transmit register, set STO bit, set WR bit.
	-- wait for interrupt or TIP flag to negate --
	3) read RxACK bit from Status Register, should be �0�.
*/
	// transmit register for write: 0x3 -> write slave address

//	alt_printf("initiating write process!\n");
	int temp;

	I2C_WRITE(base, REG_I2C_TXR, slave);
	// set sta bit, set wr bit to command register
	temp = 0x90;
	I2C_WRITE(base, REG_I2C_CR,temp);
	if(i2c_wait(base) == -1)
	{
	//	alt_printf("problems! \n");
	}
	else
	{
	//	alt_printf("slave is responsive! \n");
	}

	// send the register address to transmit register
	I2C_WRITE(base, REG_I2C_TXR, reg);
	// set wr bit to command register
	temp = 0x10;
	I2C_WRITE(base, REG_I2C_CR,temp);

	if(i2c_wait(base) == -1)
	{
		alt_printf("problems! \n");
	}
	else
	{
//		alt_printf("register address sent correctly! \n");
	}


	// send the data to transmit register
	I2C_WRITE(base, REG_I2C_TXR, data);
	// set wr bit and stop bit to command register
	temp = 0x50;
	I2C_WRITE(base, REG_I2C_CR,temp);

	if(i2c_wait(base) == -1)
	{
		alt_printf("problems! \n");
	}
	else
	{
//		alt_printf("data written correctly! \n");
	}

}

int i2c_read(int base, int slave, int reg){
/*	Read a byte of data from an I2C memory device.
	Slave address = 0x4E
	Memory location to read from = 0x20
  	I2C sequence:
	1) generate start signal
	2) write slave address + write bit
	3) receive acknowledge from slave
	4) write memory location
	5) receive acknowledge from slave
	6) generate repeated start signal
	7) write slave address + read bit
	8) receive acknowledge from slave
	9) read byte from slave
	10) write no acknowledge (NACK) to slave, indicating end of transfer
	11) generate stop signal
	Commands:
	1) write 0x9C (address + write bit) to Transmit Register, set STA bit, set WR bit.
	-- wait for interrupt or TIP flag to negate --
	2) read RxACK bit from Status Register, should be �0�.
	write 0x20 to Transmit register, set WR bit.
	-- wait for interrupt or TIP flag to negate --
	3) read RxACK bit from Status Register, should be �0�.
	write 0x9D (address + read bit) to Transmit Register, set STA bit, set WR bit.
	-- wait for interrupt or TIP flag to negate --
	4) set RD bit, set ACK to �1� (NACK), set STO bit
*/
	int data;
//	alt_printf("initiating read process!\n");
	int temp;

	I2C_WRITE(base, REG_I2C_TXR, slave);

	// set sta bit, set wr bit to command register
	temp = 0x90;
	I2C_WRITE(base, REG_I2C_CR,temp);

	if(i2c_wait(base) == -1)
	{
		alt_printf("problems! \n");
	}
	else
	{
//		alt_printf("slave is responsive! \n");
	}

	// send the register address to transmit register
	I2C_WRITE(base, REG_I2C_TXR, reg);
	// set wr bit to command register
	temp = 0x10;
	I2C_WRITE(base, REG_I2C_CR,temp);

	if(i2c_wait(base) == -1)
	{
		alt_printf("problems! \n");
	}
	else
	{
//		alt_printf("register address sent correctly! \n");
	}

	// we set the read bit!
	temp = (slave + 0x1);
	I2C_WRITE(base, REG_I2C_TXR, temp);

	// set sta bit, set wr bit to command register
	temp = 0x90;
	I2C_WRITE(base, REG_I2C_CR,temp);

	if(i2c_wait(base) == -1)
	{
		alt_printf("problems! \n");
	}
	else
	{
//		alt_printf("receive command sent!\n");
	}
	// set sto, rd, and ack bits

	temp = 0x68;
	I2C_WRITE(base, REG_I2C_CR,temp);

	if(i2c_wait(base) == -1)
	{
		//alt_printf("problems! but no problem!\n");
	}
	else
	{
		//alt_printf("reading done!!\n");
	}

	// data can be received from the register
	data = I2C_READ(base, REG_I2C_RXR);
	return data;
}

void i2c_read_test () {
	alt_putstr("start reading\n");
	int slave = 0xA0; // write A0, read A1
	int reg = 0;
	int result;
	int result2;
	result = i2c_read(I2C_MASTER_BASE, slave, reg);
	alt_printf("\nIdentifier in reg %x is: %x, should be 0x81\n\n",reg, result);

	reg = 3;
//	result = i2c_read(I2C_MASTER_BASE, slave, reg);
//	alt_printf("\nLoss of signal in reg %x is: %x, 1 = fault condition\n\n",reg, result);
//
//	reg = 4;
//	result = i2c_read(I2C_MASTER_BASE, slave, reg);
//	alt_printf("\nTx fault indicator in reg %x is: %x, 1 = fault condition\n\n",reg, result);
//
//	reg = 6;
//	result = i2c_read(I2C_MASTER_BASE, slave, reg);
//	alt_printf("\nTemperatur warning in reg %x is: %x, 1 = warning\n\n",reg, result);
//
//	reg = 7;
//	result = i2c_read(I2C_MASTER_BASE, slave, reg);
//	alt_printf("\nVCC warning in reg %x is: %x, 1 = warning\n\n",reg, result);
//
//	reg = 9;
//	result = i2c_read(I2C_MASTER_BASE, slave, reg);
//	reg = 10;
//	result2 = i2c_read(I2C_MASTER_BASE, slave, reg);
//	alt_printf("\nRx power warning in reg %x is: %x%x, 1 = warning\n\n",reg, result, result2);
//
//	reg = 19;
//	result = i2c_read(I2C_MASTER_BASE, slave, reg);
//	reg = 20;
//	result2 = i2c_read(I2C_MASTER_BASE, slave, reg);
//	alt_printf("\nOperating time in reg %x is: %x%x\n\n",reg, result, result2);
//
//	reg = 22;
//	result = i2c_read(I2C_MASTER_BASE, slave, reg);
//	alt_printf("\ninternal Temperature in reg %x is: %x\n\n",reg, result);
//
//	reg = 26;
//	result = i2c_read(I2C_MASTER_BASE, slave, reg);
//	reg = 27;
//	result2 = i2c_read(I2C_MASTER_BASE, slave, reg);
//	alt_printf("\nVCC in reg %x is: %x%x\n\n",reg, result, result2);
}

void i2c_check_warning(){
	int slave = 0xA0; // write A0, read A1
	int reg = 3;
	int result;
	alt_printf("start checking for Warnings\n");
	while (reg < 11) {
		result = i2c_read(I2C_MASTER_BASE, slave, reg);
		if (reg == 3 || reg == 4) {
			for (int i = 0; i < 8; i = i + 1) {
				int j = pow(2, i);
				if ((result & j) != 0) {
					alt_printf("Loss of Signal Warning in reg %x bit %x: %x\n", reg, i, result);
				}
			}
			if (reg == 4) {
				reg = reg + 1;
			}
		}
		else if (reg == 6 || reg == 7) {
			for (int i = 0; i < 8; i = i + 1) {
				int j = pow(2, i);
				if ((result & j) != 0) {
					alt_printf("Temperatur Warning in reg %x bit %x: %x\n", reg, i, result);
				}
			}
			if (reg == 7) {
				reg = reg + 1;
			}

		}
		else if (reg == 9 || reg == 10) {
			for (int i = 0; i < 8; i = i + 1) {
				int j = pow(2, i);
				if ((result & j) != 0) {
					alt_printf("Power Warning in reg %x bit %x: %x\n", reg, i, result);
				}
			}
		}
		reg = reg + 1;
	}
	alt_printf("end of checking\n");
}

void i2c_write_test (int data) {
	int slave = 0xA0; // write A0, read A1
	int reg = 127;
	i2c_write(I2C_MASTER_BASE, slave, reg, data);
	alt_printf("check data in reg %x after writing: %x", reg, i2c_read(I2C_MASTER_BASE, slave, reg));
}

void i2c_read_reg () {
	int i = 0;
	int slave = 0xA0; // write A0, read A1
	int reg;
	while (i < 10) {
		//reg = 22;
		//alt_printf("check temperature in reg %x: %x\n", reg, i2c_read(I2C_MASTER_BASE, slave, reg));
		reg = 34;
		alt_printf("check optical power in reg %x: %x", reg, i2c_read(I2C_MASTER_BASE, slave, reg));
		reg = 35;
		alt_printf("%x\n", i2c_read(I2C_MASTER_BASE, slave, reg));

		usleep(1000000); // wait for 1sec
		i = i + 1;
	}
	alt_printf("\n");
	i = 0;
	while (i < 10) {
		//reg = 22;
		//alt_printf("check temperature in reg %x: %x\n", reg, i2c_read(I2C_MASTER_BASE, slave, reg));
		reg = 36;
		alt_printf("check optical power in reg %x: %x", reg, i2c_read(I2C_MASTER_BASE, slave, reg));
		reg = 37;
		alt_printf("%x\n", i2c_read(I2C_MASTER_BASE, slave, reg));

		usleep(1000000); // wait for 1sec
		i = i + 1;
	}
	alt_printf("\n");
	i = 0;
	while (i < 10) {
		//reg = 22;
		//alt_printf("check temperature in reg %x: %x\n", reg, i2c_read(I2C_MASTER_BASE, slave, reg));
		reg = 38;
		alt_printf("check optical power in reg %x: %x", reg, i2c_read(I2C_MASTER_BASE, slave, reg));
		reg = 39;
		alt_printf("%x\n", i2c_read(I2C_MASTER_BASE, slave, reg));

		usleep(1000000); // wait for 1sec
		i = i + 1;
	}
	alt_printf("\n");
	i = 0;
	while (i < 10) {
		//reg = 22;
		//alt_printf("check temperature in reg %x: %x\n", reg, i2c_read(I2C_MASTER_BASE, slave, reg));
		reg = 40;
		alt_printf("check optical power in reg %x: %x", reg, i2c_read(I2C_MASTER_BASE, slave, reg));
		reg = 41;
		alt_printf("%x\n", i2c_read(I2C_MASTER_BASE, slave, reg));

		usleep(1000000); // wait for 1sec
		i = i + 1;
	}
	alt_printf("\n");
	i = 0;
	while (i < 10) {
		//reg = 22;
		//alt_printf("check temperature in reg %x: %x\n", reg, i2c_read(I2C_MASTER_BASE, slave, reg));
		reg = 26;
		alt_printf("VCC in reg %x: %x", reg, i2c_read(I2C_MASTER_BASE, slave, reg));
		reg = 27;
		alt_printf("%x\n", i2c_read(I2C_MASTER_BASE, slave, reg));

		usleep(1000000); // wait for 1sec
		i = i + 1;
	}
}

void power_management (int power, int emphasis) { //power = 0000 means low, = 0x10 means med low, = 0x20 means med high, = 0x30 means high
	int slave = 0xA0;
	int reg = 127;
	int page = 0x3;
	i2c_write(I2C_MASTER_BASE, slave, reg, page); //go into upper page 0x03
	//alt_printf("check page select %x\n", i2c_read(I2C_MASTER_BASE, slave, reg));
	reg = 238; //Power management register in upper page 0x03
	alt_printf("old power option %x\n", i2c_read(I2C_MASTER_BASE, slave, reg));
	i2c_write(I2C_MASTER_BASE, slave, reg, power); //set new power option
	alt_printf("new power option %x, what it should be: %x\n", i2c_read(I2C_MASTER_BASE, slave, reg), power);

	reg = 236; //De-emphasis register in upper page 0x03
	i2c_write(I2C_MASTER_BASE, slave, reg, emphasis);
	reg = 237; //De-emphasis register in upper page 0x03
	i2c_write(I2C_MASTER_BASE, slave, reg, emphasis);

	reg = 127;
	page = 0x0;
	i2c_write(I2C_MASTER_BASE, slave, reg, page); //go back into upper page 0x00
}

int main()
{ 
  alt_putstr("Hello from Nios II!\n");
  IOWR_ALTERA_AVALON_PIO_DATA(PIO_I2C_BASE, 0x00000001);
  //i2c_check(I2C_MASTER_BASE);
  i2c_startup(I2C_MASTER_BASE);
//  i2c_check_warning();
  //i2c_read_test ();
//  i2c_check(I2C_MASTER_BASE);
// 	i2c_write_test(0);
//  i2c_read_reg();
  power_management(0x20,0x00);
  //i2c_read_reg();
  alt_putstr("end function");


  return 0;
}
